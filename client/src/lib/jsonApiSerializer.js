import jsonApiSerializer from 'jsonapi-serializer';

const Serializer = jsonApiSerializer.Serializer;
const Deserializer = new jsonApiSerializer.Deserializer({
  keyForAttribute: 'camelCase',
});

export {
  Deserializer,
  Serializer,
};
