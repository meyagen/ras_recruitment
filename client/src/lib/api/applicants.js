import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const ApplicantSerializer = new Serializer('applicant', {
  attributes: [
    'firstName',
    'lastName',
    'landlineNumber',
    'mobileNumber',
    'phoneNumber',
    'otherNumber',
    'email',
    'countryId',
    'professionId',
    'companyId',
    'skillIds',
    'status',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

const ApplicantSkillSerializer = new Serializer('applicantSkill', {
  attributes: [],
  skill: {
    ref: 'id',
    included: true,
    attributes: ['name'],
  },
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const getApplicants = (query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants${queryString}`, options);
};

export const getApplicant = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/applicants/${id}`, options);
};

export const getApplicantsByCompany = (companyId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/company/${companyId}${queryString}`, options);
};

export const getApplicantsByTeam = (teamId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/team/${teamId}${queryString}`, options);
};

export const getApplicantsByTeamLead = (teamLeadId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/team_lead/${teamLeadId}${queryString}`, options);
};

export const getApplicantsByUser = (userId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/user/${userId}${queryString}`, options);
};

export const getApplicantsByCompanyAndUser = (companyId, userId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/company/${companyId}/user/${userId}${queryString}`, options);
};

export const getApplicantsByCompanyAndTeam = (companyId, teamId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/company/${companyId}/team/${teamId}${queryString}`, options);
};

export const createApplicant = (applicant) => {
  const newApplicant = ApplicantSerializer.serialize(applicant);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newApplicant),
  };

  return fetchApi('/v1/applicants', options);
};

export const createApplicantSkill = (applicantId, skill) => {
  const newApplicantSkill = ApplicantSkillSerializer.serialize(skill);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newApplicantSkill),
  };

  return fetchApi(`/v1/applicants/${applicantId}/skills`, options);
};

export const updateApplicant = (id, applicant) => {
  const updatedApplicant = ApplicantSerializer.serialize(applicant);
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
    body: JSON.stringify(updatedApplicant),
  };

  return fetchApi(`/v1/applicants/${id}`, options);
};

export const removeApplicant = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/applicants/${id}`, options);
};
