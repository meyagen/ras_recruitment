import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const ApplicantMailSerializer = new Serializer('email', {
  attributes: [
    'subject',
    'content',
    'applicantId',
  ],

  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

const ClientMailSerializer = new Serializer('email', {
  attributes: [
    'subject',
    'content',
    'clientId',
  ],

  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const createApplicantEmail = (email) => {
  const newEmail = ApplicantMailSerializer.serialize(email);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newEmail),
  };

  return fetchApi(`/v1/applicants/${email.applicantId}/mails`, options);
};

export const getApplicantEmails = (applicantId, query) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/${applicantId}/mails${queryString}`, options);
};

export const createClientEmail = (email) => {
  const newEmail = ClientMailSerializer.serialize(email);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newEmail),
  };

  return fetchApi(`/v1/clients/${email.clientId}/mails`, options);
};

export const getClientEmails = (clientId, query) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/${clientId}/mails${queryString}`, options);
};

