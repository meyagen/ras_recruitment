import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const ClientSerializer = new Serializer('client', {
  attributes: [
    'firstName',
    'lastName',
    'position',
    'mobileNumber',
    'primaryNumber',
    'secondaryNumber',
    'tertiaryNumber',
    'email',
    'countryId',
    'companyId',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const getClients = (query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients${queryString}`, options);
};

export const getClientsByCompany = (companyId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/company/${companyId}${queryString}`, options);
};

export const getClientsByTeam = (teamId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/team/${teamId}${queryString}`, options);
};

export const getClientsByTeamLeadId = (teamLeadId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/team_lead/${teamLeadId}${queryString}`, options);
};

export const getClientsByUser = (userId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/user/${userId}${queryString}`, options);
};

export const getClientsByCompanyAndUser = (companyId, userId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/company/${companyId}/user/${userId}${queryString}`, options);
};

export const getClientsByCompanyAndTeam = (companyId, teamId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/company/${companyId}/team/${teamId}${queryString}`, options);
};

export const getClient = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/clients/${id}`, options);
};

export const createClient = (client) => {
  const newClient = ClientSerializer.serialize(client);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newClient),
  };

  return fetchApi('/v1/clients', options);
};

export const updateClient = (id, client) => {
  const updatedClient = ClientSerializer.serialize(client);
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
    body: JSON.stringify(updatedClient),
  };

  return fetchApi(`/v1/clients/${id}`, options);
};

export const removeClient = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/clients/${id}`, options);
};
