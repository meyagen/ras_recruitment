import lscache from 'lscache';
import config from '@/../config';

export const apiBaseUrl = process.env.NODE_ENV === 'production'
  ? config.build.apiBaseUrl
  : config.dev.apiBaseUrl;

export const getHeaders = () => {
  const headers = new Headers();

  headers.append('Content-Type', 'application/vnd.api+json');
  headers.append('Authorization', `Bearer ${lscache.get('ras-token')}`);
  return headers;
};

// wrapper for communicating with the API
// this automatically returns the json received
export const fetchApi = (path, options) => (
  fetch(`${apiBaseUrl}${path}`, options)
  .then(response => response.json())
);

export const getQueryString = (query) => {
  if (!query || Object.keys(query).length === 0) {
    return '';
  }

  const queryValues = Object.keys(query).map(key => (`${key}=${query[key]}`));
  return `?${queryValues.join('&')}`;
};
