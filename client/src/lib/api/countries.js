import { getHeaders, fetchApi } from '@/lib/api/helpers';

export default {
  getCountries() {
    const options = {
      headers: getHeaders(),
      method: 'GET',
    };

    return fetchApi('/v1/countries', options);
  },
};
