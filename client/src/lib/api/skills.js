import { getHeaders, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const SkillSerializer = new Serializer('skill', {
  attributes: ['name'],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});


export const createSkill = (skill) => {
  const newSkill = SkillSerializer.serialize(skill);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newSkill),
  };

  return fetchApi('/v1/skills', options);
};

export const getSkills = () => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi('/v1/skills', options);
};

