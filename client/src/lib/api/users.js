import { getHeaders, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const UserSerializer = new Serializer('user', {
  attributes: [
    'firstName',
    'lastName',
    'position',
    'email',
    'username',
    'password',
    'role',
    'status',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

const UpdateUserSerializer = new Serializer('users', {
  attributes: [
    'firstName',
    'lastName',
    'position',
    'email',
    'password',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const activateUser = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
  };

  return fetchApi(`/v1/users/${id}/activate`, options);
};

export const getUser = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/users/${id}`, options);
};

export const getUserByUsername = (username) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/users/username/${username}`, options);
};

export const getUsers = () => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi('/v1/users', options);
};

export const createUser = (user) => {
  const newUser = UserSerializer.serialize(user);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newUser),
  };

  return fetchApi('/v1/users', options);
};

export const updateUser = (id, user) => {
  const updatedUser = UpdateUserSerializer.serialize(user);
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
    body: JSON.stringify(updatedUser),
  };

  return fetchApi(`/v1/users/${id}`, options);
};

export const deactivateUser = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/users/${id}`, options);
};
