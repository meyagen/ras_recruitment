import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const CompanySerializer = new Serializer('company', {
  attributes: [
    'name',
    'email',
    'phoneNumber',
    'country',
  ],

  country: {
    ref: 'id',
    included: true,
    attributes: ['name'],
  },

  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const createCompany = (company) => {
  const newCompany = CompanySerializer.serialize(company);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newCompany),
  };

  return fetchApi('/v1/companies', options);
};

export const getCompanies = (query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/companies${queryString}`, options);
};

export const getCompany = (id) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/companies/${id}`, options);
};

export const updateCompany = (id, company) => {
  const updatedCompany = CompanySerializer.serialize(company);
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
    body: JSON.stringify(updatedCompany),
  };

  return fetchApi(`/v1/companies/${id}`, options);
};
