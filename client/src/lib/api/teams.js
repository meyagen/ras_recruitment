import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const TeamSerializer = new Serializer('team', {
  attributes: [
    'name',
    'active?',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

const UpdateTeamSerializer = new Serializer('team', {
  attributes: [
    'name',
  ],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

const MembershipSerializer = new Serializer('memberships', {
  attributes: [
    'userId',
  ],
  keyForAttribute: 'kebab-case',
});

export const getTeams = (query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/teams${queryString}`, options);
};

export const getTeamsByUser = (userId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/teams/user/${userId}${queryString}`, options);
};

export const getTeam = (teamId) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/teams/${teamId}`, options);
};

export const getMembership = (teamId, userId) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi(`/v1/teams/${teamId}/memberships/user/${userId}`, options);
};

export const createTeam = (team) => {
  const newTeam = TeamSerializer.serialize(team);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newTeam),
  };

  return fetchApi('/v1/teams', options);
};

export const createMembership = (teamId, userId) => {
  const newMembership = MembershipSerializer.serialize({ userId });
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newMembership),
  };

  return fetchApi(`/v1/teams/${teamId}/memberships`, options);
};

export const createTeamLead = (teamId, userId) => {
  const newMembership = MembershipSerializer.serialize({ userId });
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newMembership),
  };

  return fetchApi(`/v1/teams/${teamId}/memberships/team_lead`, options);
};

export const updateTeam = (id, team) => {
  const updatedTeam = UpdateTeamSerializer.serialize(team);
  const options = {
    headers: getHeaders(),
    method: 'PATCH',
    body: JSON.stringify(updatedTeam),
  };

  return fetchApi(`/v1/teams/${id}`, options);
};

export const removeMembership = (teamId, userId) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/teams/${teamId}/memberships/user/${userId}`, options);
};

