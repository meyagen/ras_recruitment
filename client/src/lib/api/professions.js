import { getHeaders, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const ProfessionSerializer = new Serializer('profession', {
  attributes: ['name'],
  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});

export const createProfession = (profession) => {
  const newProfession = ProfessionSerializer.serialize(profession);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newProfession),
  };

  return fetchApi('/v1/professions', options);
};

export const getProfessions = () => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  return fetchApi('/v1/professions', options);
};

