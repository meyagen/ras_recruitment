import { getHeaders, getQueryString, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const NoteSerializer = new Serializer('note', {
  attributes: [
    'content',
  ],

  keyForAttribute: 'kebab-case',
  pluralizeType: false,
});


export const createApplicantNote = (applicantId, note) => {
  const newNote = NoteSerializer.serialize(note);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newNote),
  };

  return fetchApi(`/v1/applicants/${applicantId}/notes`, options);
};

export const createClientNote = (clientId, note) => {
  const newNote = NoteSerializer.serialize(note);
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(newNote),
  };

  return fetchApi(`/v1/clients/${clientId}/notes`, options);
};

export const getApplicantNotes = (applicantId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/applicants/${applicantId}/notes${queryString}`, options);
};

export const getClientNotes = (clientId, query = {}) => {
  const options = {
    headers: getHeaders(),
    method: 'GET',
  };

  const queryString = getQueryString(query);
  return fetchApi(`/v1/clients/${clientId}/notes${queryString}`, options);
};

export const deleteApplicantNote = (applicantId, noteId) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/applicants/${applicantId}/notes/${noteId}`, options);
};

export const deleteClientNote = (clientId, noteId) => {
  const options = {
    headers: getHeaders(),
    method: 'DELETE',
  };

  return fetchApi(`/v1/clients/${clientId}/notes/${noteId}`, options);
};
