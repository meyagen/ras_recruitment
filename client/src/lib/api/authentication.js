import { getHeaders, fetchApi } from '@/lib/api/helpers';
import { Serializer } from '@/lib/jsonApiSerializer';

const TokenSerializer = new Serializer('token', {
  attributes: [
    'username',
    'password',
  ],
  keyForAttribute: 'kebab-case',
});

const fetchToken = ({ username, password }) => {
  const credentials = TokenSerializer.serialize({ username, password });
  const options = {
    headers: getHeaders(),
    method: 'POST',
    body: JSON.stringify(credentials),
  };

  return fetchApi('/v1/tokens', options);
};

export default fetchToken;
