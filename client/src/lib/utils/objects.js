export default {
  sortResource: (objects, field, desc = false) => {
    if (field === 'first_name') {
      field = 'firstName';
    }

    if (field === 'last_name') {
      field = 'lastName';
    }

    if (field === 'last_note') {
      field = 'lastNoteAt';
    }

    const resources = Object.values(objects);
    const sorted = resources.sort((a, b) => {
      let fieldA = a[field];
      let fieldB = b[field];

      if (field === 'lastNoteAt' || field === 'last_note_at') {
        return new Date(a.lastNoteAt) - new Date(b.lastNoteAt);
      }

      if (typeof a[field] === 'string') {
        fieldA = fieldA.toLowerCase();
        fieldB = fieldB.toLowerCase();
      }

      if (fieldA < fieldB) {
        return -1;
      }

      if (fieldA > fieldB) {
        return 1;
      }

      return 0;
    });

    if (desc) {
      return sorted.reverse();
    }

    return sorted;
  },
};

