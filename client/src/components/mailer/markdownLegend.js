/* eslint-disable no-multi-str */

export default '# Title\n\
\n\
## Header 1\n\
\n\
### Header 2\n\
\n\
#### Header 3\n\
\n\
##### Small text\n\
\n\
###### Even smaller text\n\
\n\
**bold**\n\
\n\
_italics_\n\
\n\
> blockquote/indented paragraph\n\
\n\
an inline `monospace` word\n\
\n\
\n\
```\n\
monospace can be multiline\n\
when using three ticks\n\
```\n\
\n\
\n\
- create an unordered list\n\
- using dashes\n\
- for bullet points\n\
\n\
\n\
1. create an ordered list\n\
2. using numbers (appended with a period)\n\
\n\
\n\
1. you can also created nested lists\n\
    - by indenting the nested list by four spaces\n\
2. unindent to continue the first list\n\
    - indent again\n\
    - to create a new nested list\n\
\n\
\n\
Embed a [link](https://google.com) like so.\n\
';
