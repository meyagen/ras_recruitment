/* eslint-disable no-multi-str */

export default 'Hi @{targetFirstName},\n\
\n\
**Greetings!**\n\
\n\
We are hiring!\n\
\n\
##### **@{userFullName}**\n\
##### @{userPosition}\n\
##### Rod & Staff Recruitment (Group) - Hong Kong\n\
##### Level 21, The Center, 99 Queen\'s Rod Central, Central, Hong Kong\n\
';
