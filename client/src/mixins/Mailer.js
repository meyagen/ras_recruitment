import { mapState } from 'vuex';
import marked from 'marked';
import emailPlaceholder from '@/components/mailer/emailPlaceholder';
import markdownLegend from '@/components/mailer/markdownLegend';

export default {
  data() {
    return {
      email: {
        subject: "We're hiring!",
        content: emailPlaceholder,
      },

      markdownLegend,
      showLegend: false,
      isSendingMail: false,
      tags: [
        'userFullName',
        'userPosition',
        'targetFirstName',
        'targetLastName',
        'targetFullName',
      ],

      defaultError: {
        title: 'Oops',
        detail: 'Unable to send email. Please check that the email address is valid and try again.',
      },
    };
  },

  computed: {
    ...mapState({
      currentUserId: state => state.users.currentUserId,
      currentUser(state) {
        return state.users.users[this.currentUserId];
      },
    }),

    compiledLegend() {
      return marked(this.markdownLegend, { sanitize: true });
    },
  },

  methods: {
    compiledEmailContent(recipient) {
      return marked(this.emailContentWithoutTags(recipient), {
        sanitize: true,
        breaks: true,
        gfm: true,
      });
    },

    emailContentWithoutTags(recipient) {
      return this.email.content.replace(this.tagRegex(), (match) => {
        const tagName = match.replace(/@{|}/g, '');
        return this.getTagValue(tagName, recipient);
      });
    },

    getTagValue(tag, recipient) {
      switch (tag) {
        case 'userFullName':
          return `${this.currentUser.firstName} ${this.currentUser.lastName}`;
        case 'userPosition':
          return this.currentUser.position;
        case 'targetFirstName':
          return recipient.firstName;
        case 'targetLastName':
          return recipient.lastName;
        case 'targetFullName':
          return `${recipient.firstName} ${recipient.lastName}`;
        default:
          return '';
      }
    },

    getDispatchAction(recipientType) {
      if (recipientType === 'client') {
        return 'CREATE_CLIENT_EMAIL';
      }

      return 'CREATE_APPLICANT_EMAIL';
    },

    setRecipientId(recipientType, recipient, email) {
      if (recipientType === 'client') {
        return { ...email, clientId: recipient.id };
      }

      return { ...email, applicantId: recipient.id };
    },

    tagRegex() {
      const tagSyntax = this.tags.map(tag => `@{${tag}}`);
      return new RegExp(tagSyntax.join('|'), 'g');
    },

    toggleLegend() {
      this.showLegend = !this.showLegend;
    },

    validateEmail() {
      return this.email.subject !== '' && this.email.content !== '';
    },
  },
};
