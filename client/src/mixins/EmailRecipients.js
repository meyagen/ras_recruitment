export default {
  data() {
    return {
      emailRecipientIds: [],
    };
  },

  watch: {
    emailRecipientIds: 'sendEmailRecipientIds',
  },

  computed: {
    canComposeEmail() {
      return [
        'applicants-user',
        'applicants-team',
        'clients-user',
        'clients-team',
      ].includes(this.$route.name);
    },
  },

  methods: {
    sendEmailRecipientIds() {
      this.$emit('sendEmailRecipientIds', this.emailRecipientIds);
    },
  },
};
