import CreateCompany from '@/components/companies/CreateCompany';
import CreateProfession from '@/components/professions/CreateProfession';
import CreateSkill from '@/components/skills/CreateSkill';
import Modal from '@/components/Modal';

export default {
  components: {
    CreateCompany,
    CreateProfession,
    CreateSkill,
    Modal,
  },

  data: () => ({
    company: '',
    profession: '',
    skillsChosen: [],

    createCompanyModal: {
      active: false,
      title: 'Create Company',
      component: 'CreateCompany',
    },

    createProfessionModal: {
      active: false,
      title: 'Create Position',
      component: 'CreateProfession',
    },

    createSkillModal: {
      active: false,
      title: 'Create Skill',
      component: 'CreateSkill',
    },
  }),

  methods: {
    onCreateCompany(company) {
      this.company = company;
    },

    onCreateProfession(profession) {
      this.profession = profession;
    },

    onCreateSkill(skill) {
      this.skillsChosen.push(skill);
    },

    openModal(modal) {
      modal.active = true;
    },

    closeModal(modal) {
      modal.active = false;
    },
  },
};
