/* eslint no-unused-vars: 0 */

export default {
  data() {
    return {
      defaultPaginationQuery: {
        sort_by: 'first_name',
        sort: 'asc',
        count: 100,
        page: 1,
      },

      // used for pagination
      currentQuery: {},
    };
  },

  computed: {
    currentPage() {
      if (this.$route.query.page) {
        return parseInt(this.$route.query.page, 10);
      }

      return this.defaultPaginationQuery.page;
    },

    mode() {
      return this.$route.name.split('-')[1];
    },

    modeHasTable() {
      return [
        'applicants',
        'applicants-user',
        'applicants-team',
        'clients',
        'clients-user',
        'clients-team',
      ].includes(this.mode);
    },

    recordsCount() {
      return this.getRecordsCount(this.mode);
    },

    resourceName() {
      return this.$route.name.split('-')[0];
    },
  },

  methods: {
    buildOptionalQueryParameters(queryParams = null) {
      if (!queryParams) {
        return this.defaultPaginationQuery;
      }

      const query = {};

      if (queryParams.firstName) {
        query.first_name = queryParams.firstName;
      }

      if (queryParams.lastName) {
        query.last_name = queryParams.lastName;
      }

      if (queryParams.country && queryParams.country.id !== 0) {
        query.country_id = queryParams.country.id;
      }

      if (queryParams.profession && queryParams.profession.id !== 0) {
        query.profession_id = queryParams.profession.id;
      }

      if (queryParams.company && queryParams.company.id !== 0) {
        query.company_id = queryParams.company.id;
      }

      if (queryParams.skill && queryParams.skill.id !== 0) {
        query.skill_id = queryParams.skill.id;
      }

      if (queryParams.creator && queryParams.creator.id !== 0) {
        query.created_by = queryParams.creator.id;
      }

      if (queryParams.sortBy) {
        query.sort_by = queryParams.sortBy;
      }

      return query;
    },

    getPage(pageNumber) {
      this.setRoute(this.mode, { ...this.$route.query, page: pageNumber });
    },

    getRecordsCount(mode) {
      const resources = ['applicants', 'clients', 'companies', 'notes', 'emails'];
      if (resources.includes(mode)) {
        return this.getResourceLength(this[mode]);
      }

      const resourceFilteredByUser = ['user', 'team'];
      if (resourceFilteredByUser.includes(mode)) {
        return this.getResourceLength(this[this.resourceName]);
      }

      return 0;
    },

    getResourceLength(resources) {
      return Object.keys(resources).length;
    },

    pageLimit() {
      if (this.$route.query.count) {
        return parseInt(this.$route.query.count, 10);
      }

      return this.defaultPaginationQuery.count;
    },

    resetOptionalQueryParameters(optionalQuery) {
      const paginationFields = [
        'sort',
        'page',
        'count',
      ];

      const route = this.$route.query;
      const paginationQuery = Object.keys(route)
        .filter(key => paginationFields.includes(key))
        .reduce((obj, key) => {
          if (route[key] && !optionalQuery[key]) {
            obj[key] = route[key];
          }

          return obj;
        }, {});

      if (route.sort_by && !optionalQuery.sortBy) {
        optionalQuery.sort_by = route.sort_by;
      }

      this.setRoute(this.mode, { ...paginationQuery, ...optionalQuery });
    },

    setValidQueryParams(currentQuery, queryParams) {
      const validParams = [
        'first_name',
        'last_name',
        'country_id',
        'profession_id',
        'position',
        'company_id',
        'skill_id',
        'created_by',
        'sort',
        'sort_by',
        'page',
        'count',
      ];

      const filtered = Object.keys(queryParams)
        .filter(key => validParams.includes(key))
        .reduce((obj, key) => {
          if (queryParams[key] !== null) {
            obj[key] = queryParams[key];
          }

          return obj;
        }, {});

      return { ...currentQuery, ...filtered };
    },

    setDefaultPaginationQuery(currentQuery, mode = null) {
      const selectedMode = mode || this.mode;
      const query = { ...currentQuery, ...this.defaultPaginationQuery };

      switch (selectedMode) {
        case 'applicants':
          break;
        case 'clients':
          break;
        case 'companies':
          query.sort_by = 'name';
          break;
        case 'notes':
          query.sort_by = 'inserted_at';
          query.sort = 'desc';
          query.count = 20;
          break;
        case 'emails':
          query.sort_by = 'inserted_at';
          query.sort = 'desc';
          query.count = 20;
          break;
        case 'user':
          break;
        case 'team':
          break;
        default:
          break;
      }

      if (currentQuery.page) {
        query.page = currentQuery.page;
      }

      if (currentQuery.count) {
        query.count = currentQuery.count;
      }

      if (currentQuery.sort) {
        query.sort = currentQuery.sort;
      }

      if (currentQuery.sort_by) {
        query.sort_by = currentQuery.sort_by;
      }

      return query;
    },

    setMode(newMode) {
      this.setRoute(newMode);
    },

    setCount(count) {
      this.setRoute(this.mode, { ...this.$route.query, count, page: 1 });
    },

    setRoute(newMode, optionalQuery = {}) {
      const id = this.id;
      const name = `${this.resourceName}-${newMode}`;

      let query = {};
      if (Object.values(optionalQuery).length) {
        query = this.buildOptionalQueryParameters(optionalQuery);
        query = this.setValidQueryParams(query, optionalQuery);
      }

      query = this.setDefaultPaginationQuery(query, newMode);

      this.$router.push({
        name, query, params: { id },
      });
    },
  },
};
