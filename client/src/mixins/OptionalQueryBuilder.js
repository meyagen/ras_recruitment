import debounce from 'debounce';

export default {
  data() {
    return {
      queryParams: {},
    };
  },

  watch: {
    queryParams: {
      deep: true,
      handler(newQueryParams) {
        this.$emit('resetOptionalQueryParameters', newQueryParams);
      },
    },
  },

  methods: {
    debounceFirstName: debounce(function (e) {
      this.queryParams.firstName = e.target.value;
    }, 500),

    debounceLastName: debounce(function (e) {
      this.queryParams.lastName = e.target.value;
    }, 500),

    debouncePosition: debounce(function (e) {
      this.queryParams.position = e.target.value;
    }, 500),

    getOptionValues(resources) {
      const resourceArray = Object.values(resources);
      const allOption = { ...resourceArray[0] };

      if (allOption.name) {
        allOption.name = 'All';
      }

      if (allOption.firstName) {
        allOption.firstName = 'All';
        allOption.lastName = '';
      }

      allOption.id = 0;
      return [allOption].concat(resourceArray);
    },

    setOptionalQueryParamsFromUrl() {
      if (Object.values(this.$route.query).length > 0) {
        const query = this.$route.query;
        const recruitment = this.$store.state.recruitment;
        const users = this.$store.state.users;

        if (query.first_name) {
          this.queryParams.firstName = query.first_name;
          this.firstName = query.first_name;
        }

        if (query.last_name) {
          this.queryParams.lastName = query.last_name;
          this.lastName = query.last_name;
        }

        if (query.country_id) {
          this.queryParams.country = recruitment.countries[query.country_id];
        }

        if (query.profession_id) {
          this.queryParams.profession = recruitment.professions[query.profession_id];
        }

        if (query.position) {
          this.queryParams.position = query.position;
          this.position = query.position;
        }

        if (query.company_id) {
          this.queryParams.company = recruitment.companies[query.company_id];
        }

        if (query.skill_id) {
          this.queryParams.skill = recruitment.skills[query.skill_id];
        }

        if (query.created_by) {
          this.queryParams.creator = users.users[query.created_by];
        }

        if (query.sort_by) {
          this.queryParams.sortBy = query.sort_by;
        }

        if (query.sort) {
          this.queryParams.sort = query.sort;
        }
      }
    },

    setSortBy(toSort) {
      const changeToDesc = this.queryParams.sort === 'asc' && this.queryParams.sortBy === toSort;
      const order = changeToDesc ? 'desc' : 'asc';

      this.queryParams = {
        ...this.queryParams,
        sortBy: toSort,
        sort: order,
      };
    },
  },
};
