export default {
  data() {
    return {
      user: {
        password: '',
      },

      confirmPassword: '',
      errors: [],
    };
  },

  methods: {
    validateUser() {
      if (this.user.password === '') {
        this.errors.push({
          title: 'Blank Password',
          detail: 'Password cannot be blank',
        });

        return false;
      }

      if (this.confirmPassword !== this.user.password) {
        this.errors.push({
          title: 'Password Mismatch',
          detail: 'Re-type your password and make sure they match',
        });

        return false;
      }

      return true;
    },
  },
};
