import ObjectsHelper from '@/lib/utils/objects';

export default {
  methods: {
    formatDate(dateString) {
      const date = new Date(dateString);
      return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
    },

    fullUserName({ firstName, lastName }) {
      return `${firstName} ${lastName}`;
    },

    positionString(position) {
      if (!position) {
        return 'None';
      }

      return position.toLowerCase();
    },

    skillString(applicant) {
      if (applicant.skills.length === 0) {
        return 'none';
      }

      const sortedSkills = ObjectsHelper.sortResource(applicant.skills, 'name');
      const skills = sortedSkills.map(skill => skill.name);
      return skills.join(', ');
    },

    uppercaseOptions({ name }) {
      return name.toUpperCase();
    },
  },
};
