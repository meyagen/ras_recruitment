import Vue from 'vue';
import * as api from '@/lib/api/users';
import { Deserializer } from '@/lib/jsonApiSerializer';

const USER_ROLE = {
  user: 'user',
  admin: 'admin',
  teamLead: 'team_lead',
};

const USER_STATUS = {
  active: 'active',
  inactive: 'banned',
};

const initialState = {
  currentUserId: 0,
  users: {/* [id: string]: User */},
};

const userGetters = {
  currentUser: state => (state.users[state.currentUserId]),

  getAlphabeticalUsers: (state) => {
    const users = Object.values(state.users);
    return users.sort((a, b) => {
      if (a.firstName < b.firstName) {
        return -1;
      }

      if (a.firstName > b.firstName) {
        return 1;
      }

      return 0;
    });
  },

  getActiveUsers: (state) => {
    const users = Object.values(state.users);
    return users.filter(user => (user.status === USER_STATUS.active));
  },

  getUser: state => id => (state.users[id]),
  getUserFullName: state => (id) => {
    const user = state.users[id];

    if (!user) {
      return '';
    }

    return `${user.firstName} ${user.lastName}`;
  },

  isCurrentUser: state => id => (parseInt(id, 10) === parseInt(state.currentUserId, 10)),
  isCurrentUserAdmin: (state) => {
    const currentUser = state.users[state.currentUserId];
    if (currentUser) {
      return currentUser.role === USER_ROLE.admin;
    }

    return false;
  },
};

const mutations = {
  ACTIVATE_USER: (state, { id }) => {
    const user = state.users[id];
    state.users[id] = {
      ...user,
      new: false,
      status: USER_STATUS.active,
    };
  },

  DEACTIVATE_USER: (state, { id }) => {
    const user = state.users[id];
    state.users[id] = {
      ...user,
      new: false,
      status: USER_STATUS.inactive,
    };
  },

  SET_USER: (state, { user }) => {
    Vue.set(state.users, user.id, user);
  },

  SET_CURRENT_USER: (state, { id }) => {
    Vue.set(state, 'currentUserId', id);
  },

  SET_USER_AS_NEW: (state, { user }) => {
    user.new = true;
    Vue.set(state.users, user.id, user);
  },

  SET_USERS: (state, { users }) => {
    users.forEach((user) => {
      Vue.set(state.users, user.id, user);
    });
  },
};

const actions = {
  ACTIVATE_USER: ({ commit }, { id }) => (
    api.activateUser(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedUser) => {
        commit('ACTIVATE_USER', { id });
        return Promise.resolve(updatedUser);
      });
    })
  ),

  FETCH_USER: ({ commit }, { id }) => (
    api.getUser(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, user) => {
        commit('SET_USER', { user });
        return Promise.resolve(user);
      });
    })
  ),

  FETCH_USER_BY_USERNAME: ({ commit }, { username }) => (
    api.getUserByUsername(username).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, user) => {
        commit('SET_USER', { user });
        return Promise.resolve(user);
      });
    })
  ),

  FETCH_USERS: ({ commit }) => (
    api.getUsers().then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, users) => {
        commit('SET_USERS', { users });
        return Promise.resolve(users);
      });
    })
  ),

  CREATE_USER: ({ commit }, { user }) => (
    api.createUser(user).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newUser) => {
        commit('SET_USER_AS_NEW', { user: newUser });
        return Promise.resolve(newUser);
      });
    })
  ),

  UPDATE_USER: ({ commit }, { id, user }) => (
    api.updateUser(id, user).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedUser) => {
        commit('SET_USER', { user: updatedUser });
        return Promise.resolve(updatedUser);
      });
    })
  ),

  DEACTIVATE_USER: ({ commit }, { id }) => (
    api.deactivateUser(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedUser) => {
        commit('DEACTIVATE_USER', { id });
        return Promise.resolve(updatedUser);
      });
    })
  ),
};

export default {
  state: initialState,
  getters: userGetters,
  mutations,
  actions,
};

export { USER_ROLE, USER_STATUS };
