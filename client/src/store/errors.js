const initialState = {
  type: null,
  message: '',
};

const getters = {
  showApiError: state => state.type === 'apiError',
  isUnauthorized: state => state.type === 'unauthorized',
  hasNoError: state => state.type === null,
};

const mutations = {
  SET_ERROR_BY_STATUS: (state, { status, message }) => {
    if (status === 403) {
      state.type = 'unauthorized';
    } else {
      state.type = 'apiError';
      state.message = message;
    }
  },

  CLEAR_ERRORS: (state) => {
    state.type = null;
  },
};

const actions = {
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
