import * as companiesApi from '@/lib/api/companies';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const initialState = {
  companies: {/* [id: string]: Company */},
};

const getters = {
  getAlphabeticalCompanies: state => (
    ObjectsHelper.sortResource(state.companies, 'name')
  ),

  getMostRecentCompanies: state => (
    ObjectsHelper.sortResource(state.companies, 'updatedAt', true)
  ),
};

const mutations = {
  CLEAR_COMPANIES: (state) => {
    state.companies = {};
  },

  SET_COMPANIES: (state, { companies }) => {
    companies.forEach((company) => {
      Vue.set(state.companies, company.id, company);
    });
  },

  SET_COMPANY: (state, { company }) => {
    Vue.set(state.companies, company.id, company);
  },

  SET_COMPANY_AS_NEW: (state, { company }) => {
    company.new = true;
    Vue.set(state.companies, company.id, company);
  },
};

const actions = {
  CREATE_COMPANY: ({ commit }, { company }) => (
    companiesApi.createCompany(company).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newCompany) => {
        commit('SET_COMPANY_AS_NEW', { company: newCompany });
        return Promise.resolve(newCompany);
      });
    })
  ),

  FETCH_COMPANIES: ({ commit }, { query }) => {
    if (query === undefined) {
      query = {};
    }

    return companiesApi.getCompanies(query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, companies) => {
        commit('SET_COMPANIES', { companies });
        return Promise.resolve(companies);
      });
    });
  },

  FETCH_COMPANY: ({ commit }, { id }) => (
    companiesApi.getCompany(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, company) => {
        commit('SET_COMPANY', { company });
        return Promise.resolve(company);
      });
    })
  ),

  UPDATE_COMPANY: ({ commit }, { id, company }) => (
    companiesApi.updateCompany(id, company).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedCompany) => {
        commit('SET_COMPANY', { company: updatedCompany });
        return Promise.resolve(updatedCompany);
      });
    })
  ),
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
