import * as skillsApi from '@/lib/api/skills';
import { Deserializer } from '@/lib/jsonApiSerializer';
import StringHelper from '@/lib/utils/string';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const initialState = {
  skills: {/* [id: string]: Skills */},
};

const getters = {
  getAlphabeticalSkills: state => (
    ObjectsHelper.sortResource(state.skills, 'name')
  ),
};

const mutations = {
  SET_SKILL: (state, { skill }) => {
    skill.name = StringHelper.capitalize(skill.name);
    Vue.set(state.skills, skill.id, skill);
  },

  SET_SKILLS: (state, { skills }) => {
    skills.forEach((skill) => {
      skill.name = StringHelper.capitalize(skill.name);
      Vue.set(state.skills, skill.id, skill);
    });
  },
};

const actions = {
  CREATE_SKILL: ({ commit }, { skill }) => (
    skillsApi.createSkill(skill).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newSkill) => {
        commit('SET_SKILL', { skill: newSkill });
        return Promise.resolve(newSkill);
      });
    })
  ),

  FETCH_SKILLS: ({ commit }) => {
    skillsApi.getSkills().then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, skills) => {
        commit('SET_SKILLS', { skills });
        return Promise.resolve(skills);
      });
    });
  },

};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
