import * as mailApi from '@/lib/api/emails';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const defaultEmailError = [{
  title: 'Oops',
  detail: 'Unable to send email. Please check that the email address is valid and try again.',
}];

const initialState = {
  emails: {/* [id: string]: Email */},
};

const getters = {
  getEmailsByLatestCreationDate: state => (
    ObjectsHelper.sortResource(state.emails, 'insertedAt', true)
  ),
};

const mutations = {
  CLEAR_EMAILS: (state) => {
    state.emails = {};
  },

  SET_EMAIL: (state, { email }) => {
    Vue.set(state.emails, email.id, email);
  },

  SET_EMAILS: (state, { emails }) => {
    emails.forEach((email) => {
      Vue.set(state.emails, email.id, email);
    });
  },
};

const actions = {
  CREATE_APPLICANT_EMAIL: ({ commit }, { email }) => (
    mailApi.createApplicantEmail(email)
    .then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newEmail) => {
        commit('SET_EMAIL', { email: newEmail });
        return Promise.resolve(newEmail);
      });
    })
    .catch(() => (Promise.reject(defaultEmailError)))
  ),

  CREATE_CLIENT_EMAIL: ({ commit }, { email }) => (
    mailApi.createClientEmail(email).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newEmail) => {
        commit('SET_EMAIL', { email: newEmail });
        return Promise.resolve(newEmail);
      });
    })
    .catch(() => (Promise.reject(defaultEmailError)))
  ),

  FETCH_APPLICANT_EMAILS: ({ commit }, { applicantId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return mailApi.getApplicantEmails(applicantId, query)
    .then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, emails) => {
        commit('SET_EMAILS', { emails });
        return Promise.resolve(emails);
      });
    });
  },

  FETCH_CLIENT_EMAILS: ({ commit }, { clientId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return mailApi.getClientEmails(clientId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, emails) => {
        commit('SET_EMAILS', { emails });
        return Promise.resolve(emails);
      });
    });
  },
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
