import countriesApi from '@/lib/api/countries';
import { Deserializer } from '@/lib/jsonApiSerializer';
import Vue from 'vue';
import lscache from 'lscache';

const initialState = {
  countries: {/* [id: string]: Country */},
};

const getters = {
};

const mutations = {
  SET_COUNTRIES: (state, { countries }) => {
    countries.forEach((country) => {
      Vue.set(state.countries, country.id, country);
    });
  },
};

const actions = {
  FETCH_COUNTRIES: ({ commit }) => {
    const cachedCountries = lscache.get('countries');
    if (cachedCountries) {
      commit('SET_COUNTRIES', { countries: cachedCountries });
      return Promise.resolve(cachedCountries);
    }

    return countriesApi.getCountries().then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, countries) => {
        commit('SET_COUNTRIES', { countries });
        lscache.set('countries', countries, 1440); // 24 hours
        return Promise.resolve(countries);
      });
    });
  },
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
