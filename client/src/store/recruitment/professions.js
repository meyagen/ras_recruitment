import * as professionsApi from '@/lib/api/professions';
import { Deserializer } from '@/lib/jsonApiSerializer';
import StringHelper from '@/lib/utils/string';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const initialState = {
  professions: {/* [id: string]: Profession */},
};

const getters = {
  getAlphabeticalProfessions: state => (
    ObjectsHelper.sortResource(state.professions, 'name')
  ),
};

const mutations = {
  SET_PROFESSION: (state, { profession }) => {
    profession.name = StringHelper.capitalize(profession.name);
    Vue.set(state.professions, profession.id, profession);
  },

  SET_PROFESSIONS: (state, { professions }) => {
    professions.forEach((profession) => {
      profession.name = StringHelper.capitalize(profession.name);
      Vue.set(state.professions, profession.id, profession);
    });
  },
};

const actions = {
  CREATE_PROFESSION: ({ commit }, { profession }) => (
    professionsApi.createProfession(profession).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newProfession) => {
        commit('SET_PROFESSION', { profession: newProfession });
        return Promise.resolve(newProfession);
      });
    })
  ),

  FETCH_PROFESSIONS: ({ commit }) => {
    professionsApi.getProfessions().then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, professions) => {
        commit('SET_PROFESSIONS', { professions });
        return Promise.resolve(professions);
      });
    });
  },
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
