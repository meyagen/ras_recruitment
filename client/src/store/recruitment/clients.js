import * as clientApi from '@/lib/api/clients';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';
import store from '@/store';

const initialState = {
  clients: {/* [id: string]: Client */},
};

const getters = {
  getAlphabeticalClients: state => (
    ObjectsHelper.sortResource(state.clients, 'firstName')
  ),

  getMostRecentClients: state => (
    ObjectsHelper.sortResource(state.clients, 'updatedAt', true)
  ),

  getClientFullName: state => (id) => {
    const client = state.clients[id];
    return `${client.firstName} ${client.lastName}`;
  },
};

const mutations = {
  ADD_CLIENTS: (state, { clients }) => {
    clients.forEach((client) => {
      Vue.set(state.clients, client.id, client);
    });
  },

  CLEAR_CLIENTS: (state) => {
    state.clients = {};
  },

  SET_CLIENT: (state, { client }) => {
    Vue.set(state.clients, client.id, client);
  },

  SET_CLIENT_AS_NEW: (state, { client }) => {
    client.new = true;
    Vue.set(state.clients, client.id, client);
  },

  SET_CLIENTS: (state, { clients }) => {
    state.clients = {};
    clients.forEach((client) => {
      Vue.set(state.clients, client.id, client);
    });
  },

  UNSET_CLIENT: (state, { id }) => {
    Vue.delete(state.clients, id);
  },
};

const actions = {
  CREATE_CLIENT: ({ commit }, { client }) => (
    clientApi.createClient(client).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newClient) => {
        commit('SET_CLIENT_AS_NEW', { client: newClient });
        return Promise.resolve(newClient);
      });
    })
  ),

  FETCH_CLIENT: ({ commit }, { id }) => (
    clientApi.getClient(id).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, client) => {
        commit('SET_CLIENT', { client });
        return Promise.resolve(client);
      });
    })
  ),

  FETCH_CLIENTS: ({ commit }, { query }) => {
    if (query === undefined) {
      query = {};
    }

    return clientApi.getClients(query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_COMPANY: ({ commit }, { companyId, query }) => {
    if (query === undefined) {
      query = {};
    }

    clientApi.getClientsByCompany(companyId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_COMPANY_AND_USER: ({ commit }, { companyId, userId, query }) => {
    if (query === undefined) {
      query = {};
    }

    clientApi.getClientsByCompanyAndUser(companyId, userId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_COMPANY_AND_TEAM: ({ commit }, { companyId, teamId, query }) => {
    if (query === undefined) {
      query = {};
    }

    clientApi.getClientsByCompanyAndTeam(companyId, teamId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('ADD_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_EMAIL: ({ commit }, { userId, email }) => (
    clientApi.getClientsByUser(userId, { email }).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        if (clients.length) {
          commit('SET_APPLICANT', { applicant: clients[0] });
        }

        return Promise.resolve(clients);
      });
    })
  ),

  FETCH_CLIENTS_BY_TEAM: ({ commit }, { teamId, query }) => {
    if (query === undefined) {
      query = {};
    }

    clientApi.getClientsByTeam(teamId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_TEAM_LEAD: ({ commit }, { teamLeadId, query }) => {
    if (query === undefined) {
      query = {};
    }

    clientApi.getClientsByTeamLeadId(teamLeadId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  FETCH_CLIENTS_BY_USER: ({ commit }, { userId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return clientApi.getClientsByUser(userId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, clients) => {
        commit('SET_CLIENTS', { clients });
        return Promise.resolve(clients);
      });
    });
  },

  REMOVE_CLIENT: ({ commit }, { id }) => (
    clientApi.removeClient(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, () => {
        commit('UNSET_CLIENT', { id });
        return Promise.resolve();
      });
    })
  ),

  UPDATE_CLIENT: ({ commit }, { id, client }) => (
    clientApi.updateClient(id, client).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedClient) => {
        commit('SET_CLIENT', { client: updatedClient });
        return Promise.resolve(updatedClient);
      });
    })
  ),
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
