import * as applicantApi from '@/lib/api/applicants';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';
import store from '@/store';

export const APPLICANT_STATUS = {
  open: 'open',
  placed: 'placed',
};

const initialState = {
  applicants: {/* [id: string]: Applicant */},
};

const getters = {
  getAlphabeticalApplicants: state => (
    ObjectsHelper.sortResource(state.applicants, 'firstName')
  ),

  getMostRecentApplicants: state => (
    ObjectsHelper.sortResource(state.applicants, 'updatedAt', true)
  ),

  getApplicantFullName: state => (id) => {
    const applicant = state.applicants[id];
    return `${applicant.firstName} ${applicant.lastName}`;
  },
};

const mutations = {
  ADD_APPLICANTS: (state, { applicants }) => {
    applicants.forEach((applicant) => {
      Vue.set(state.applicants, applicant.id, applicant);
    });
  },

  CLEAR_APPLICANTS: (state) => {
    state.applicants = {};
  },

  SET_APPLICANT: (state, { applicant }) => {
    Vue.set(state.applicants, applicant.id, applicant);
  },

  SET_APPLICANT_AS_NEW: (state, { applicant }) => {
    applicant.new = true;
    Vue.set(state.applicants, applicant.id, applicant);
  },

  SET_APPLICANT_SKILL: (state, { applicantSkill }) => {
    const applicantId = applicantSkill.applicantId;
    const skill = applicantSkill.skill;
    state.applicants[applicantId].skills.push(skill);
  },

  SET_APPLICANTS: (state, { applicants }) => {
    state.applicants = {};
    applicants.forEach((applicant) => {
      Vue.set(state.applicants, applicant.id, applicant);
    });
  },

  UNSET_APPLICANT: (state, { id }) => {
    Vue.delete(state.applicants, id);
  },
};

const actions = {
  CREATE_APPLICANT: ({ commit }, { applicant }) => (
    applicantApi.createApplicant(applicant).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newApplicant) => {
        commit('SET_APPLICANT_AS_NEW', { applicant: newApplicant });
        return Promise.resolve(newApplicant);
      });
    })
  ),

  CREATE_APPLICANT_SKILL: ({ commit }, { applicantId, skill }) => (
    applicantApi.createApplicantSkill(applicantId, skill).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newApplicantSkill) => {
        commit('SET_APPLICANT_SKILL', { applicantSkill: newApplicantSkill });
        return Promise.resolve(newApplicantSkill);
      });
    })
  ),

  FETCH_APPLICANT: ({ commit }, { id }) => (
    applicantApi.getApplicant(id).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicant) => {
        commit('SET_APPLICANT', { applicant });
        return Promise.resolve(applicant);
      });
    })
  ),

  FETCH_APPLICANTS: ({ commit }, { query }) => {
    if (query === undefined) {
      query = {};
    }

    return applicantApi.getApplicants(query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_COMPANY: ({ commit }, { companyId, query }) => {
    if (query === undefined) {
      query = {};
    }

    applicantApi.getApplicantsByCompany(companyId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_COMPANY_AND_TEAM: ({ commit }, { companyId, teamId, query }) => {
    if (query === undefined) {
      query = {};
    }

    applicantApi.getApplicantsByCompanyAndTeam(companyId, teamId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_COMPANY_AND_USER: ({ commit }, { companyId, userId, query }) => {
    if (query === undefined) {
      query = {};
    }

    applicantApi.getApplicantsByCompanyAndUser(companyId, userId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_EMAIL: ({ commit }, { userId, email }) => (
    applicantApi.getApplicantsByUser(userId, { email }).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        if (applicants.length) {
          commit('SET_APPLICANT', { applicant: applicants[0] });
        }

        return Promise.resolve(applicants);
      });
    })
  ),

  FETCH_APPLICANTS_BY_TEAM: ({ commit }, { teamId, query }) => {
    if (query === undefined) {
      query = {};
    }

    applicantApi.getApplicantsByTeam(teamId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('ADD_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_TEAM_LEAD: ({ commit }, { teamLeadId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return applicantApi.getApplicantsByTeamLead(teamLeadId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  FETCH_APPLICANTS_BY_USER: ({ commit }, { userId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return applicantApi.getApplicantsByUser(userId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        store.commit('SET_ERROR_BY_STATUS', { status: jsonApi.errors[0].status });
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, applicants) => {
        commit('SET_APPLICANTS', { applicants });
        return Promise.resolve(applicants);
      });
    });
  },

  UPDATE_APPLICANT: ({ commit }, { id, applicant }) => (
    applicantApi.updateApplicant(id, applicant).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedApplicant) => {
        commit('SET_APPLICANT', { applicant: updatedApplicant });
        return Promise.resolve(updatedApplicant);
      });
    })
  ),

  REMOVE_APPLICANT: ({ commit }, { id }) => (
    applicantApi.removeApplicant(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, () => {
        commit('UNSET_APPLICANT', { id });
        return Promise.resolve();
      });
    })
  ),
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
