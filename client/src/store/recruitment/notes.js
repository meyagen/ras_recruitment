import * as notesApi from '@/lib/api/notes';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const initialState = {
  notes: {/* [id: string]: Note */},
};

const getters = {
  getNotesByLatestCreationDate: state => (
    ObjectsHelper.sortResource(state.notes, 'insertedAt', true)
  ),
};

const mutations = {
  CLEAR_NOTES: (state) => {
    state.notes = {};
  },

  SET_NOTE_AS_NEW: (state, { note }) => {
    note.new = true;
    Vue.set(state.notes, note.id, note);
  },

  SET_NOTES: (state, { notes }) => {
    notes.forEach((note) => {
      Vue.set(state.notes, note.id, note);
    });
  },

  UNSET_NOTE: (state, { id }) => {
    Vue.delete(state.notes, id);
  },
};

const actions = {
  CREATE_APPLICANT_NOTE: ({ commit }, { applicantId, note }) => (
    notesApi.createApplicantNote(applicantId, note).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newNote) => {
        commit('SET_NOTE_AS_NEW', { note: newNote });
        return Promise.resolve(newNote);
      });
    })
  ),

  CREATE_CLIENT_NOTE: ({ commit }, { clientId, note }) => (
    notesApi.createClientNote(clientId, note).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newNote) => {
        commit('SET_NOTE_AS_NEW', { note: newNote });
        return Promise.resolve(newNote);
      });
    })
  ),

  FETCH_APPLICANT_NOTES: ({ commit }, { query, applicantId }) => {
    if (query === undefined) {
      query = {};
    }

    return notesApi.getApplicantNotes(applicantId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, notes) => {
        commit('SET_NOTES', { notes });
        return Promise.resolve(notes);
      });
    });
  },

  FETCH_CLIENT_NOTES: ({ commit }, { query, clientId }) => {
    if (query === undefined) {
      query = {};
    }

    return notesApi.getClientNotes(clientId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, notes) => {
        commit('SET_NOTES', { notes });
        return Promise.resolve(notes);
      });
    });
  },

  REMOVE_APPLICANT_NOTE: ({ commit }, { applicantId, noteId }) => (
    notesApi.deleteApplicantNote(applicantId, noteId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, () => {
        commit('UNSET_NOTE', { id: noteId });
        return Promise.resolve();
      });
    })
  ),

  REMOVE_CLIENT_NOTE: ({ commit }, { clientId, noteId }) => (
    notesApi.deleteClientNote(clientId, noteId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, () => {
        commit('UNSET_NOTE', { id: noteId });
        return Promise.resolve();
      });
    })
  ),
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
