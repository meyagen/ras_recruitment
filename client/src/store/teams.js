import * as api from '@/lib/api/teams';
import { Deserializer } from '@/lib/jsonApiSerializer';
import ObjectsHelper from '@/lib/utils/objects';
import Vue from 'vue';

const MEMBERSHIP_ROLE = {
  member: 'member',
  teamLead: 'team_lead',
};

const initialState = {
  teams: {/* [id: string]: Team */},
};

const teamGetters = {
  isMemberOfTeam: state => (userId, teamId) => {
    const team = state.teams[teamId];
    if (!team) {
      return false;
    }

    const memberships = team.memberships;
    if (!memberships) {
      return false;
    }

    const userMemberships = memberships.filter(member => (
      member.userId === parseInt(userId, 10)
    ));

    return userMemberships.length > 0;
  },

  isTeamLeadOfTeam: state => (userId, teamId) => {
    const memberships = state.teams[teamId].memberships;

    if (!memberships) {
      return false;
    }

    const userMemberships = memberships.filter(member => (
      member.userId === parseInt(userId, 10) && member.role === MEMBERSHIP_ROLE.teamLead
    ));

    return userMemberships.length > 0;
  },

  getTeamsLead: state => (userId) => {
    const teams = state.teams;

    if (!teams) {
      return [];
    }

    return Object.values(teams).filter((team) => {
      const userMemberships = team.memberships.filter(member => (
        member.userId === parseInt(userId, 10) && member.role === MEMBERSHIP_ROLE.teamLead
      ));

      return userMemberships.length > 0;
    });
  },

  getAlphabeticalTeams: state => (
    ObjectsHelper.sortResource(state.teams, 'name')
  ),

  getMostRecentTeams: state => (
    ObjectsHelper.sortResource(state.teams, 'updatedAt', true)
  ),
};

const mutations = {
  SET_TEAMS: (state, { teams }) => {
    teams.forEach((team) => {
      if (team.memberships === undefined) {
        team.memberships = [];
      }

      Vue.set(state.teams, team.id, team);
    });
  },

  SET_TEAM: (state, { team }) => {
    if (team.memberships === undefined) {
      team.memberships = [];
    }

    Vue.set(state.teams, team.id, team);
  },

  SET_TEAM_AS_NEW: (state, { team }) => {
    team.new = true;
    if (team.memberships === undefined) {
      team.memberships = [];
    }

    Vue.set(state.teams, team.id, team);
  },

  SET_TEAM_MEMBER: (state, { teamId, membership }) => {
    state.teams[teamId].memberships.push(membership);
  },

  SET_TEAM_MEMBER_AS_NEW: (state, { teamId, membership }) => {
    membership.new = true;
    state.teams[teamId].memberships.push(membership);
  },

  REMOVE_MEMBERSHIP: (state, { teamId, userId }) => {
    const memberships = state.teams[teamId].memberships;
    const newMemberships = memberships.filter(member => (
      member.userId !== parseInt(userId, 10)
    ));

    state.teams[teamId].memberships = newMemberships;
  },
};

const actions = {
  FETCH_TEAMS: ({ commit }, { query }) => {
    if (query === undefined) {
      query = {};
    }

    return api.getTeams(query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, teams) => {
        commit('SET_TEAMS', { teams });
        return Promise.resolve(teams);
      });
    });
  },

  FETCH_TEAMS_BY_USER: ({ commit }, { userId, query }) => {
    if (query === undefined) {
      query = {};
    }

    return api.getTeamsByUser(userId, query).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, teams) => {
        commit('SET_TEAMS', { teams });
        return Promise.resolve(teams);
      });
    });
  },

  FETCH_TEAM: ({ commit }, { id }) => (
    api.getTeam(id).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, team) => {
        commit('SET_TEAM', { team });
        return Promise.resolve(team);
      });
    })
  ),

  FETCH_MEMBERSHIP: ({ commit }, { teamId, userId }) => (
    api.getMembership(teamId, userId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, membership) => {
        commit('SET_TEAM_MEMBER', { teamId, membership });
        return Promise.resolve(membership);
      });
    })
  ),

  CREATE_TEAM: ({ commit }, { team }) => (
    api.createTeam(team).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newTeam) => {
        commit('SET_TEAM_AS_NEW', { team: newTeam });
        return Promise.resolve(newTeam);
      });
    })
  ),

  CREATE_MEMBERSHIP: ({ commit }, { teamId, userId }) => (
    api.createMembership(teamId, userId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newMembership) => {
        commit('SET_TEAM_MEMBER_AS_NEW', { teamId, membership: newMembership });
        return Promise.resolve(newMembership);
      });
    })
  ),

  CREATE_TEAM_LEAD: ({ commit }, { teamId, userId }) => (
    api.createTeamLead(teamId, userId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, newMembership) => {
        commit('SET_TEAM_MEMBER_AS_NEW', { teamId, membership: newMembership });
        return Promise.resolve(newMembership);
      });
    })
  ),

  UPDATE_TEAM: ({ commit }, { id, team }) => (
    api.updateTeam(id, team).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, updatedTeam) => {
        commit('SET_TEAM', { team: updatedTeam });
        return Promise.resolve(updatedTeam);
      });
    })
  ),

  REMOVE_MEMBERSHIP: ({ commit }, { teamId, userId }) => (
    api.removeMembership(teamId, userId).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, membership) => {
        commit('REMOVE_MEMBERSHIP', { teamId, userId });
        return Promise.resolve(membership);
      });
    })
  ),

};

export default {
  state: initialState,
  getters: teamGetters,
  mutations,
  actions,
};

export { MEMBERSHIP_ROLE };
