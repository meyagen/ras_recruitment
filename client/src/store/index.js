import Vue from 'vue';
import Vuex from 'vuex';
import authentication from './authentication';
import recruitment from './recruitment';
import errors from './errors';
import teams from './teams';
import users from './users';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    authentication,
    recruitment,
    errors,
    teams,
    users,
  },
});
