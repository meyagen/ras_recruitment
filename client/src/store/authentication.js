import fetchToken from '@/lib/api/authentication';
import { Deserializer } from '@/lib/jsonApiSerializer';

const initialState = {
  token: '',
};

const getters = {
  isLoggedIn: state => state.token !== '',
};

const mutations = {
  SET_TOKEN: (state, { token }) => {
    state.token = token;
  },
  REMOVE_TOKEN: (state) => {
    state.token = '';
  },
};

const actions = {
  FETCH_TOKEN: ({ commit }, payload) => (
    fetchToken(payload).then((jsonApi) => {
      if (jsonApi.errors) {
        return Promise.reject(jsonApi.errors);
      }

      return Deserializer.deserialize(jsonApi, (error, token) => {
        commit('SET_TOKEN', { token: token.id });
        return Promise.resolve(token);
      });
    })
  ),
};

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
