import applicants from '@/store/recruitment/applicants';
import clients from '@/store/recruitment/clients';
import companies from '@/store/recruitment/companies';
import countries from '@/store/recruitment/countries';
import emails from '@/store/recruitment/emails';
import notes from '@/store/recruitment/notes';
import professions from '@/store/recruitment/professions';
import skills from '@/store/recruitment/skills';

const initialState = Object.assign(
  applicants.state,
  clients.state,
  companies.state,
  countries.state,
  emails.state,
  notes.state,
  professions.state,
  skills.state,
);

const getters = Object.assign(
  applicants.getters,
  clients.getters,
  companies.getters,
  countries.getters,
  emails.getters,
  notes.getters,
  professions.getters,
  skills.getters,
);

const mutations = Object.assign(
  applicants.mutations,
  clients.mutations,
  companies.mutations,
  countries.mutations,
  emails.mutations,
  notes.mutations,
  professions.mutations,
  skills.mutations,
);

const actions = Object.assign(
  applicants.actions,
  clients.actions,
  companies.actions,
  countries.actions,
  emails.actions,
  notes.actions,
  professions.actions,
  skills.actions,
);

export default {
  state: initialState,
  getters,
  mutations,
  actions,
};
