import Vue from 'vue';
import Popover from 'vue-js-popover';
import App from '@/App';
import store from '@/store';
import { sync } from 'vuex-router-sync';
import router from '@/router';

require('assets/stylesheets/app.styl');
require('node_modules/font-awesome/css/font-awesome.min.css');

Vue.config.productionTip = false;
Vue.use(Popover);

sync(store, router);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App },
});
