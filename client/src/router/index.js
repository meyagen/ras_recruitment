import String from '@/lib/utils/string';
import store from '@/store';
import Applicants from '@/pages/Applicants';
import Applicant from '@/pages/Applicant';
import ApplicantForm from '@/pages/ApplicantForm';
import ApplicantEditForm from '@/pages/ApplicantEditForm';
import ApplicantMail from '@/pages/ApplicantMail';
import Client from '@/pages/Client';
import Clients from '@/pages/Clients';
import ClientForm from '@/pages/ClientForm';
import ClientEditForm from '@/pages/ClientEditForm';
import ClientMail from '@/pages/ClientMail';
import Companies from '@/pages/Companies';
import Company from '@/pages/Company';
import Dashboard from '@/pages/Dashboard';
import Login from '@/pages/Login';
import NotFound from '@/pages/NotFound';
import Team from '@/pages/Team';
import Teams from '@/pages/Teams';
import Professions from '@/pages/Professions';
import Skills from '@/pages/Skills';
import User from '@/pages/User';
import Users from '@/pages/Users';
import Vue from 'vue';
import VueRouter from 'vue-router';
import lscache from 'lscache';

Vue.use(VueRouter);

const authState = store.state.authentication;
const userState = store.state.users;

const getCurrentUser = () => {
  const currentUserId = userState.currentUserId;
  const currentUser = store.getters.currentUser;

  if (currentUserId > 0 && currentUser === undefined) {
    return store.dispatch('FETCH_USER', { id: currentUserId });
  }

  return Promise.resolve(currentUser);
};

const setSessionFromLocalStorage = () => {
  if (authState.token && userState.currentUserId) {
    return;
  }

  const token = lscache.get('ras-token');
  if (token) {
    store.commit('SET_TOKEN', { token });
  }

  const currentUserId = lscache.get('ras-currentUserId');
  if (currentUserId) {
    store.commit('SET_CURRENT_USER', { id: currentUserId });
  }

  const countries = lscache.get('countries');
  if (countries) {
    store.commit('SET_COUNTRIES', { countries });
  }
};

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    beforeEnter: (to, from, next) => {
      setSessionFromLocalStorage();
      if (authState.token !== '') {
        next({ name: 'dashboard' });
      } else {
        next();
      }
    },
  },
  {
    path: '/candidates/user',
    name: 'applicants-user',
    component: Applicants,
  },
  {
    path: '/candidates/team',
    name: 'applicants-team',
    component: Applicants,
  },
  {
    path: '/candidates/new',
    name: 'create-candidate',
    component: ApplicantForm,
  },
  {
    path: '/candidates/:id',
    name: 'candidate-overview',
    component: Applicant,
  },
  {
    path: '/candidates/:id/notes',
    name: 'candidate-notes',
    component: Applicant,
  },
  {
    path: '/candidates/:id/emails',
    name: 'candidate-emails',
    component: Applicant,
  },
  {
    path: '/candidates/:id/edit',
    name: 'edit-candidate',
    component: ApplicantEditForm,
  },
  {
    path: '/candidates/:id/mail',
    name: 'mail-candidate',
    component: ApplicantMail,
  },
  {
    path: '/clients/user',
    name: 'clients-user',
    component: Clients,
  },
  {
    path: '/clients/team',
    name: 'clients-team',
    component: Clients,
  },
  {
    path: '/clients/new',
    name: 'create-client',
    component: ClientForm,
  },
  {
    path: '/clients/:id',
    name: 'client-overview',
    component: Client,
  },
  {
    path: '/clients/:id/notes',
    name: 'client-notes',
    component: Client,
  },
  {
    path: '/clients/:id/emails',
    name: 'client-emails',
    component: Client,
  },
  {
    path: '/clients/:id/edit',
    name: 'edit-client',
    component: ClientEditForm,
  },
  {
    path: '/clients/:id/mail',
    name: 'mail-client',
    component: ClientMail,
  },
  {
    path: '/companies',
    name: 'companies',
    component: Companies,
  },
  {
    path: '/companies/:id',
    name: 'company-overview',
    component: Company,
  },
  {
    path: '/companies/:id/candidates',
    name: 'company-applicants',
    component: Company,
  },
  {
    path: '/companies/:id/clients',
    name: 'company-clients',
    component: Company,
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
  },
  {
    path: '/professions',
    name: 'professions',
    component: Professions,
  },
  {
    path: '/skills',
    name: 'skills',
    component: Skills,
  },
  {
    path: '/teams',
    name: 'teams',
    component: Teams,
  },
  {
    path: '/teams/:id',
    name: 'team-overview',
    component: Team,
  },
  {
    path: '/teams/:id/candidates',
    name: 'team-applicants',
    component: Team,
  },
  {
    path: '/teams/:id/clients',
    name: 'team-clients',
    component: Team,
  },
  {
    path: '/users',
    name: 'users',
    component: Users,
  },
  {
    path: '/users/:id',
    name: 'user',
    component: User,
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound,
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

const getTitle = (routeName) => {
  const firstWordOnly = [
    'applicants-user',
    'applicants-team',
    'clients-user',
    'clients-team',
  ];

  let title = routeName
    .split('-')
    .map(word => String.capitalize(word));

  if (firstWordOnly.includes(routeName)) {
    title = title[0];
  } else {
    title = title.join(' ');
  }


  return `${title} | RAS Directory`;
};

// make sure user is authenticated before routing to pages other than login
router.beforeEach((to, from, next) => {
  document.title = getTitle(to.name);
  setSessionFromLocalStorage();
  store.commit('CLEAR_ERRORS');

  if (to.name !== 'login' && authState.token === '') {
    next({ name: 'login' });
  } else {
    getCurrentUser()
    .then(() => {
      next();
    });
  }
});


export default router;
