# RasDirectoryClient

SPA client for RAS Recruitment Directory using Vue 2.0 and [vue-cli](https://github.com/vuejs/vue-cli)'s official webpack template.

## Build Setup

``` bash
# modify /etc/hosts
echo "127.0.0.1  ras-directory.com" > /etc/hosts

# install dependencies
yarn install

# serve with hot reload at ras-directory.com:4001
yarn run dev

# build for production with minification
yarn run build

# build for production and view the bundle analyzer report
yarn run build --report
```
## Guides

For detailed explanation on how things work, checkout the following guides:

* [VueJS Webpack Template](http://vuejs-templates.github.io/webpack/)
* [Vue 2.0](https://vuejs.org/v2/guide/)
* [Vuex](https://vuex.vuejs.org/en/intro.html)
* [Bulma](http://bulma.io/documentation/overview/start/)
