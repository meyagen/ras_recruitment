["rel", "plugins", "*.exs"]
|> Path.join()
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    default_release: :ras_directory_api,
    default_environment: :prod

environment :prod do
  set include_erts: true
  set include_src: false
  set cookie: :"rCZm`6%^qO)*bs[EiYL|%9PZLl!KM29cjUGE.oy{X$><IY9&RSxTL2$;7aTyipQ6"
  set commands: [
    migrate: "rel/commands/migrate.sh",
  ]
end

release :ras_directory_api do
  set version: current_version(:ras_directory_api)
  set applications: [
    :canada,
    :comeonin,
    :elixir_make,
    :runtime_tools
  ]
end
