defmodule RasDirectoryApi.OrganizationTest do
  alias RasDirectoryApi.Organization

  @valid_team %{name: "MySuperTeam"}

  def fixture(kind, attrs \\ @valid_team)
  def fixture(:team, attrs) do
    {:ok, team} = Organization.create_team(attrs)
    team
  end
end
