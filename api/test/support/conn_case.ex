defmodule RasDirectoryApiWeb.ConnCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Phoenix.ConnTest
      import RasDirectoryApiWeb.Router.Helpers

      alias RasDirectoryApi.Fixtures

      @endpoint RasDirectoryApiWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(RasDirectoryApi.Repo)
    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(RasDirectoryApi.Repo, {:shared, self()})
    end

    conn = Phoenix.ConnTest.build_conn()
      |> Plug.Conn.put_req_header("accept", "application/vnd.api+json")
      |> Plug.Conn.put_req_header("content-type", "application/vnd.api+json")

    {:ok, conn: conn}
  end
end
