defmodule RasDirectoryApi.FactTest do
  alias RasDirectoryApi.Fact

  @country_data %{name: "Philippines"}

  def fixture(:country, attrs \\ @country_data) do
    {:ok, country} = Fact.create_country(attrs)
    country
  end
end
