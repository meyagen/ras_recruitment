defmodule RasDirectoryApi.Fixtures do
  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Fact
  alias RasDirectoryApi.Messaging
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Outsourcing
  alias RasDirectoryApi.Outsourcing.Client
  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.Recruitment.Applicant

  @user_default_data %{
    username: "UserXYZ",
    password: "12345678",
    email: "userxyz@rasrecruitment.com",
    first_name: "User",
    last_name: "Xyz",
    position: "President",
    role: 1,
    status: 1
  }

  @applicant_default_data %{
    first_name: "Juan",
    last_name: "Dela Cruz",
    landline_number: "111-22-33",
    mobile_number: "+639061124975",
    phone_number: "111-22-33",
    email: "ncalabroso+test@gmail.com"
  }

  @client_default_data %{
    first_name: "Client",
    last_name: "XYZ",
    position: "President",
    mobile_number: "09061124975",
    primary_number: "09061124976",
    other_number: "09061124977",
    email: "mireyagenandres+test@gmail.com"
  }

  @company_default_data %{
    name: "Mithiin Labs",
    phone_number: "837-39-75",
    email: "test@mithiinlabs.ph",
  }

  def user do
    {:ok, user} = Account.create_user(@user_default_data)
    user
  end

  def user(username) do
    with nil <- Account.get_by_username(username) do
      attrs = @user_default_data
        |> Map.put(:username, username)
        |> Map.put(:email, "#{String.downcase(username)}@rasrecruitment.com")

      {:ok, user} = Account.create_user(attrs)
      user
    end
  end

  def admin do
    attrs = @user_default_data |> Map.put(:role, 2)
    {:ok, user} = Account.create_user(attrs)
    user
  end

  def applicants(count \\ 50, opts \\ []) do
    profession = profession("Applicant Profession")
    country = country("Applicant Country")
    creator = opts[:creator] || user("ApplicantCreator")

    base_attrs = @applicant_default_data
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, country.id)
      |> Map.put(:created_by, creator.id)

    Enum.map(1..count, fn e ->
      base_attrs
      |> Map.put(:first_name, "Juan#{e}")
      |> applicant()
    end)
  end

  def applicant do
    profession = profession("New Profession")
    country = country("Some Country")
    creator = user("Shikamaru")

    attrs = @applicant_default_data
      |> Map.put(:created_by, creator.id)
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, country.id)

    applicant(attrs)
  end

  def applicant([creator: creator]) do
    profession = profession("Another Profession")
    country = country("Another Country")

    attrs = @applicant_default_data
      |> Map.put(:created_by, creator.id)
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, country.id)

    applicant(attrs)
  end

  def applicant([creator: creator, skills: skills]) do
    profession = profession("Another Profession")
    country = country("Another Country")
    skill_ids = skills
      |> Enum.map(&(&1.id))

    attrs = @applicant_default_data
      |> Map.put(:created_by, creator.id)
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, country.id)
      |> Map.put(:skill_ids, skill_ids)

    applicant(attrs)
  end

  def applicant(%{} = attrs) do
    {:ok, applicant} = Recruitment.create_applicant(attrs)
    applicant
  end

  def applicant(attrs) do
    first_name = attrs[:first_name] || @applicant_default_data[:first_name]
    last_name = attrs[:last_name] || @applicant_default_data[:last_name]
    profession = attrs[:profession] || profession("Director")
    country = attrs[:country] || country("Cuba")
    skills = attrs[:skills] || [skill("Python")]
    creator = attrs[:creator] || user("SomeNewUser")
    company = attrs[:company] || company("Freelancer YVR")

    applicant_attrs = @applicant_default_data
      |> Map.put(:first_name, first_name)
      |> Map.put(:last_name, last_name)
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, country.id)
      |> Map.put(:skill_ids, Enum.map(skills, &(&1.id)))
      |> Map.put(:created_by, creator.id)
      |> Map.put(:company_id, company.id)

    applicant(applicant_attrs)
  end

  def notes(model, count \\ 50)
  def notes(%Applicant{} = applicant, count) do
    base_attrs = %{applicant_id: applicant.id, created_by: applicant.created_by}
    Enum.map(1..count, &(
      base_attrs
      |> Map.put(:content, "Hello#{&1}")
      |> note
    ))
  end

  def notes(%Client{} = client, count) do
    base_attrs = %{client_id: client.id, created_by: client.created_by}
    Enum.map(1..count, &(
      base_attrs
      |> Map.put(:content, "Hello#{&1}")
      |> note
    ))
  end

  def note(attrs) do
    {:ok, note} = Messaging.create_note(attrs)
    note
  end

  def mails(%Applicant{} = applicant, count) do
    base_attrs = %{applicant_id: applicant.id, created_by: applicant.created_by}
    Enum.map(1..count, &(
      base_attrs
      |> Map.put(:subject, "Subject#{&1}")
      |> Map.put(:content, "Hello#{&1}")
      |> mail
    ))
  end

  def mails(%Client{} = client, count) do
    base_attrs = %{client_id: client.id, created_by: client.created_by}
    Enum.map(1..count, &(
      base_attrs
      |> Map.put(:subject, "Subject#{&1}")
      |> Map.put(:content, "Hello#{&1}")
      |> mail
    ))
  end

  def mail(attrs) do
    {:ok, mail} = Messaging.create_mail_record(attrs)
    mail
  end

  def skill do
    {:ok, skill} = Recruitment.create_skill(%{name: "SomeSkill"})
    skill
  end

  def skill(name) do
    name = name |> String.downcase()
    with nil <- Recruitment.get_skill_by_name(name) do
      {:ok, skill} = Recruitment.create_skill(%{name: name})
      skill
    end
  end

  def clients(count \\ 50, opts \\ []) do
    company = opts[:company] || company("Marlboro")
    creator = opts[:creator] || user("ClientCreator")

    base_attrs = @client_default_data
      |> Map.put(:company_id, company.id)
      |> Map.put(:country_id, company.country_id)
      |> Map.put(:created_by, creator.id)

    Enum.map(1..count, &(
      base_attrs
      |> Map.put(:first_name, "Naruto#{&1}")
      |> client()
    ))
  end

  def client do
    creator = user("client_creator")
    company = company()

    attrs = @client_default_data
      |> Map.put(:company_id, company.id)
      |> Map.put(:country_id, company.country_id)
      |> Map.put(:created_by, creator.id)

    {:ok, client} = Outsourcing.create_client(attrs)
    client
  end

  def client(%{created_by: _, company_id: _, country_id: _} = attrs) do
    {:ok, client} = Outsourcing.create_client(attrs)
    client
  end

  def client(%{created_by: creator_id}) do
    company = company()

    attrs = @client_default_data
      |> Map.put(:company_id, company.id)
      |> Map.put(:country_id, company.country_id)
      |> Map.put(:created_by, creator_id)

    {:ok, client} = Outsourcing.create_client(attrs)
    client
  end

  def client(attrs) do
    first_name = attrs[:first_name] || @client_default_data[:first_name]
    last_name = attrs[:last_name] || @client_default_data[:last_name]
    position = attrs[:position] || @client_default_data[:position]
    creator = attrs[:creator] || user("ClientCreator")
    country = attrs[:country] || country("North Korea")
    company = attrs[:company] || company("Meralco")

    attrs = %{}
      |> Map.put(:first_name, first_name)
      |> Map.put(:last_name, last_name)
      |> Map.put(:position, position)
      |> Map.put(:created_by, creator.id)
      |> Map.put(:country_id, country.id)
      |> Map.put(:company_id, company.id)

    {:ok, client} = Outsourcing.create_client(attrs)
    client
  end

  def company do
    creator = user("company_creator")
    country = country("Portugal")

    attrs = @company_default_data
      |> Map.put(:created_by, creator.id)
      |> Map.put(:country_id, country.id)

    {:ok, company} = Recruitment.create_company(attrs)
    company
  end

  def company(%{created_by: _} = attrs) do
    country = country("Singapore#{attrs[:created_by]}")
    attrs = attrs
      |> Map.put(:country_id, country.id)

    {:ok, company} = Recruitment.create_company(attrs)
    company
  end

  def company(%{country_id: _} = attrs) do
    creator = user()

    attrs = attrs
      |> Map.put(:created_by, creator.id)

    {:ok, company} = Recruitment.create_company(attrs)
    company
  end

  def company(name) do
    with nil <- Recruitment.get_company_by_name(name) do
      creator = user("company_with_name_creator")
      country = country("Saudi Arabia")

      attrs = @company_default_data
        |> Map.put(:name, name)
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, country.id)

      {:ok, company} = Recruitment.create_company(attrs)
      company
    end
  end

  def profession(name \\ "Software Engineer") do
    name = name |> String.downcase()
    with nil <- Recruitment.get_profession_by_name(name) do
      {:ok, profession} = Recruitment.create_profession(%{name: name})
      profession
    end
  end

  def country(name \\ "Philippines") do
    with nil <- Fact.get_country_by_name(name) do
      {:ok, country} = Fact.create_country(%{name: name})
      country
    end
  end

  def teams(count \\ 10) do
    Enum.map(1..count, fn e -> team("Team#{e}") end)
  end

  def team(name \\ "Justice League") do
    {:ok, team} = Organization.create_team(%{name: name})
    team
  end

  def team_lead(team, user) do
    attrs = %{
      user_id: user.id,
      team_id: team.id,
      role: 2
    }
    Organization.create_membership(attrs)
  end

  def member(team, user) do
    attrs = %{
      user_id: user.id,
      team_id: team.id,
      role: 1
    }
    Organization.create_membership(attrs)
  end
end
