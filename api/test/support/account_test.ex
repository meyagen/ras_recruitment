defmodule RasDirectoryApi.AccountTest do
  alias RasDirectoryApi.Account

  @create_attrs %{
    username: "admin",
    password: "12345678",
    email: "admin@rasrecruitment.com",
    first_name: "RaS",
    last_name: "Admin",
    position: "President",
    role: 1,
    status: 1
  }

  def fixture(role, attrs \\ @create_attrs)
  def fixture(:user, attrs) do
    {:ok, user} = Account.create_user(attrs)
    user
  end

  def fixture(:banned_user, attrs) do
    attrs = attrs |> Map.put(:status, 0)
    {:ok, user} = Account.create_user(attrs)
    user
  end

  def fixture(:admin, attrs) do
    attrs = attrs |> Map.put(:role, 2)
    {:ok, user} = Account.create_user(attrs)
    user
  end
end
