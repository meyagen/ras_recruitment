defmodule RasDirectoryApi.RecruitmentTest do
  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.FactTest
  alias RasDirectoryApi.Recruitment

  @company_data %{
    name: "Clean Water Initiative",
    phone_number: "936-15-16",
    email: "president@cwi.ph"
  }

  @user_data %{
    username: "funderwood",
    password: "12345678",
    email: "funderwood@rasrecruitment.com",
    first_name: "Frank",
    last_name: "Underwood",
    position: "President"
  }

  def fixture(type, attrs \\ @company_data)
  def fixture(:company, attrs) do
    country = FactTest.fixture(:country)

    user = AccountTest.fixture(:user, @user_data)
    attrs = attrs
      |> Map.put(:created_by, user.id)
      |> Map.put(:country_id, country.id)

    {:ok, company} = Recruitment.create_company(attrs)

    company
  end

  def fixture(:vanilla_company, attrs) do
    country = FactTest.fixture(:country)
    attrs = attrs
      |> Map.put(:country_id, country.id)

    {:ok, company} = Recruitment.create_company(attrs)

    company
  end

  def fixture(:profession, name) do
    {:ok, profession} = Recruitment.create_profession(%{name: name})
    profession
  end

  def fixture(:applicant, attrs) do
    company = fixture(:company)
    {:ok, profession} = Recruitment.create_profession(%{name: "Software Engineer"})

    attrs = attrs
      |> Map.put(:profession_id, profession.id)
      |> Map.put(:country_id, company.country_id)
      |> Map.put(:company_id, company.id)

    {:ok, applicant} = Recruitment.create_applicant(attrs)
    applicant
  end
end
