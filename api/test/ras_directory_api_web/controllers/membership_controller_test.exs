defmodule RasDirectoryApiWeb.MembershipControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.OrganizationTest

  @ordinary_user %{
    username: "louisck",
    password: "12345678",
    email: "louisck@rasrecruitment.com",
    first_name: "Louis",
    last_name: "C.K.",
    position: "Normal",
    role: 1,
    status: 1
  }

  describe "appointing a team lead by an admin" do
    setup [:create_admin, :log_in, :create_user]

    test "to an existing team without team lead", %{conn: conn, user: user} do
      team = OrganizationTest.fixture(:team)

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: user.id}}
      })

      %{"data" => data} = conn
        |> post(team_membership_path(conn, :assign_team_lead, team.id), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == team.id
      assert data["attributes"]["user-id"] == user.id
      assert data["attributes"]["role"] == "team_lead"
    end

    test "to a non-existing team", %{conn: conn, user: user} do
      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: user.id}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :assign_team_lead, 1), params)
        |> json_response(400)

      assert error["detail"] == "Team does not exist"
    end

    test "with a user that is already a team member", %{conn: conn, user: user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 2})

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: user.id}}
      })

      %{"data" => data} = conn
        |> post(team_membership_path(conn, :assign_team_lead, team.id), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == team.id
      assert data["attributes"]["user-id"] == user.id
      assert data["attributes"]["role"] == "team_lead"
    end

    test "with a non-existing user", %{conn: conn} do
      team = OrganizationTest.fixture(:team)

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: 1}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :assign_team_lead, team.id), params)
        |> json_response(400)

      assert error["detail"] == "User does not exist"
    end
  end

  describe "appointing an existing team lead by a non-admin" do
    setup [:create_non_admin, :log_in]

    test "should fail", %{conn: conn} do
      team = OrganizationTest.fixture(:team)
      user = AccountTest.fixture(:user)

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: user.id}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :assign_team_lead, team.id), params)
        |> json_response(400)

      assert error["detail"] == "User is not allowed to access this resource"
    end
  end

  describe "adding a member to an existing team as the team lead" do
    setup [:create_team_lead, :log_in]

    test "with valid user", %{conn: conn, membership: membership} do
      target_user = AccountTest.fixture(:user)

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: target_user.id}}
      })

      %{"data" => data} = conn
        |> post(team_membership_path(conn, :create, membership.team_id), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == membership.team_id
      assert data["attributes"]["user-id"] == target_user.id
      assert data["attributes"]["role"] == "member"
    end

    test "with invalid user", %{conn: conn, membership: membership} do
      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: 1}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :create, membership.team_id), params)
        |> json_response(400)

      assert error["detail"] == "User does not exist"
    end

    test "with duplicate user", %{conn: conn, membership: membership} do
      target_user = AccountTest.fixture(:user)
      Organization.create_membership(%{team_id: membership.team_id, user_id: target_user.id, role: 1})

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: target_user.id}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :create, membership.team_id), params)
        |> json_response(400)

      assert error["detail"] == "User has already been taken"
    end
  end

  describe "adding a member to an existing team as a member" do
    setup [:create_non_admin, :log_in]

    test "should be unauthorized", %{conn: conn, current_user: user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 1})
      target_user = AccountTest.fixture(:user)

      params = Poison.encode!(%{
        data: %{type: "membership", attributes: %{user_id: target_user.id}}
      })

      %{"errors" => [error]} = conn
        |> post(team_membership_path(conn, :create, team.id), params)
        |> json_response(400)

      assert error["detail"] == "User is not allowed to access this resource"
    end
  end

  describe "getting membership of a user" do
    setup [:create_non_admin, :log_in]

    test "in a team the user is a member of", %{conn: conn, current_user: user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 1})

      %{"data" => data} = conn
        |> get(team_membership_path(conn, :get_by_user, team.id, user.id))
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == team.id
      assert data["attributes"]["user-id"] == user.id
      assert data["attributes"]["role"] == "member"
    end

    test "in a team the user is a team lead of", %{conn: conn, current_user: user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 2})

      %{"data" => data} = conn
        |> get(team_membership_path(conn, :get_by_user, team.id, user.id))
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == team.id
      assert data["attributes"]["user-id"] == user.id
      assert data["attributes"]["role"] == "team_lead"
    end

    test "in a team the user is not a member of", %{conn: conn, current_user: user} do
      team = OrganizationTest.fixture(:team)

      %{"errors" => [error]} = conn
        |> get(team_membership_path(conn, :get_by_user, team.id, user.id))
        |> json_response(400)

      assert error["status"] == 404
    end
  end

  describe "removing from a team" do
    setup [:create_team_lead, :log_in]

    test "a valid member", %{conn: conn, membership: membership} do
      target_user = AccountTest.fixture(:user)
      Organization.create_membership(%{team_id: membership.team_id, user_id: target_user.id, role: 1})

      %{"data" => data} = conn
        |> delete(team_membership_path(conn, :delete, membership.team_id, target_user.id))
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["team-id"] == membership.team_id
      assert data["attributes"]["user-id"] == target_user.id
      assert data["attributes"]["role"] == "member"
    end

    test "a non-member", %{conn: conn, membership: membership} do
      target_user = AccountTest.fixture(:user)

      %{"errors" => [error]} = conn
        |> delete(team_membership_path(conn, :delete, membership.team_id, target_user.id))
        |> json_response(400)

      assert error["detail"] == "Resource cannot be found"
    end
  end

  defp create_admin(%{conn: conn}) do
    user = AccountTest.fixture(:admin)
    {:ok, conn: conn, current_user: user}
  end

  defp create_non_admin(%{conn: conn}) do
    user = AccountTest.fixture(:user, @ordinary_user)
    {:ok, conn: conn, current_user: user}
  end

  def create_team_lead(%{conn: conn}) do
    team = OrganizationTest.fixture(:team)
    user = AccountTest.fixture(:user, @ordinary_user)
    {:ok, membership} = Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 2})

    {:ok, conn: conn, current_user: user, team: team, membership: membership}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end

  defp create_user(%{conn: conn}) do
    user = AccountTest.fixture(:user, @ordinary_user)
    {:ok, conn: conn, user: user}
  end
end
