defmodule RasDirectoryApi.ClientMailControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Fixtures

  describe "sending a mail to a client" do
    setup [:create_user, :log_in]

    test "with valid data", %{conn: conn, current_user: current_user} do
      client = Fixtures.client()
      attrs = %{subject: "RAS Message", content: "<h1>Hello, World!</h1>"}

      params = Poison.encode!(%{data: %{type: "client_mail", attributes: attrs}})

      %{"data" => result, "included" => [creator]} = conn
        |> post(client_client_mail_path(conn, :create, client.id), params)
        |> json_response(200)

      assert result["id"]
      assert result["attributes"]["client-id"] == client.id
      assert result["attributes"]["subject"] == "RAS Message"
      assert result["attributes"]["content"] == "<h1>Hello, World!</h1>"
      assert result["attributes"]["inserted-at"]
      assert result["attributes"]["updated-at"]
      assert creator["id"] == Integer.to_string(current_user.id)
    end
  end

  describe "getting all mails sent to the client" do
    setup [:create_user, :log_in]

    test "valid", %{conn: conn} do
      client = Fixtures.client()
      Fixtures.mails(client, 101)

      %{"data" => result} = conn
        |> get(client_client_mail_path(conn, :index, client.id))
        |> json_response(200)

      assert Kernel.length(result) == 101
      [head | _] = result
      assert head["attributes"]["subject"] == "Subject101"
    end

    test "valid with pagination", %{conn: conn} do
      client = Fixtures.client()
      Fixtures.mails(client, 101)

      %{"data" => result} = conn
        |> get(client_client_mail_path(conn, :index, client.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["subject"] == "Subject51"
    end
  end

  defp create_user(%{conn: conn}) do
    user = Fixtures.user("CurrentUser")
    {:ok, conn: conn, current_user: user}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
      |> Plug.Conn.put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
