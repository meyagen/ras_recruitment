defmodule RasDirectoryApi.ClientNoteControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Fixtures
  alias RasDirectoryApi.Messaging

  describe "creating a new note" do
    setup [:create_user, :log_in]

    test "for client with valid data", %{conn: conn, current_user: current_user} do
      client = Fixtures.client()
      attrs = %{content: "Hello, World!"}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"data" => result, "included" => [creator]} = conn
        |> post(client_client_note_path(conn, :create, client.id), params)
        |> json_response(200)

      assert result["id"]
      assert result["attributes"]["client-id"] == client.id
      assert result["attributes"]["content"] == "Hello, World!"
      assert result["attributes"]["created-by"] == current_user.id
      assert result["attributes"]["inserted-at"]
      assert result["attributes"]["updated-at"]
      assert creator["id"] == Integer.to_string(current_user.id)
    end

    test "for an client with invalid data", %{conn: conn} do
      client = Fixtures.client()
      attrs = %{content: ""}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"errors" => [error]} = conn
        |> post(client_client_note_path(conn, :create, client.id), params)
        |> json_response(400)

      assert error["title"] == "can't be blank"
      assert error["detail"] == "Content can't be blank"
    end

    test "for a missing client", %{conn: conn} do
      attrs = %{content: "Hello, World!"}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"errors" => [error]} = conn
        |> post(client_client_note_path(conn, :create, 1), params)
        |> json_response(400)

      assert error["title"] == "does not exist"
      assert error["detail"] == "Client does not exist"
    end
  end

  describe "getting all notes for an client" do
    setup [:create_user, :log_in]

    test "existing client", %{conn: conn} do
      client = Fixtures.client()
      Fixtures.notes(client, 51)

      %{"data" => result} = conn
        |> get(client_client_note_path(conn, :index, client.id))
        |> json_response(200)

      assert Kernel.length(result) == 51
    end

    test "existing client with pagination", %{conn: conn} do
      client = Fixtures.client()
      Fixtures.notes(client, 112)

      %{"data" => result} = conn
        |> get(client_client_note_path(conn, :index, client.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["content"] == "Hello62"
    end
  end

  describe "deleting a note for a client" do
    setup [:create_user, :log_in]

    test "by the creator", %{conn: conn, current_user: current_user} do
      client = Fixtures.client()
      note = Fixtures.note(%{client_id: client.id, created_by: current_user.id, content: "Hello!"})

      %{"data" => result} = conn
        |> delete(client_client_note_path(conn, :delete, client.id, note.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(note.id)
      refute Messaging.get_client_note(note.id)
    end

    test "by the non-creator", %{conn: conn} do
      client = Fixtures.client()
      note = Fixtures.note(%{client_id: client.id, created_by: client.created_by, content: "Hello!"})

      %{"errors" => [error]} = conn
        |> delete(client_client_note_path(conn, :delete, client.id, note.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert Messaging.get_client_note(note.id)
    end
  end

  defp create_user(%{conn: conn}) do
    user = Fixtures.user("CurrentUser")
    {:ok, conn: conn, current_user: user}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
      |> Plug.Conn.put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
