defmodule RasDirectoryApi.ApplicantNoteControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Fixtures
  alias RasDirectoryApi.Messaging

  describe "creating a new note" do
    setup [:create_user, :log_in]

    test "for an applicant with valid data", %{conn: conn, current_user: current_user} do
      applicant = Fixtures.applicant()
      attrs = %{content: "Hello, World!"}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"data" => result, "included" => [creator]} = conn
        |> post(applicant_applicant_note_path(conn, :create, applicant.id), params)
        |> json_response(200)

      assert result["id"]
      assert result["attributes"]["applicant-id"] == applicant.id
      assert result["attributes"]["content"] == "Hello, World!"
      assert result["attributes"]["created-by"] == current_user.id
      assert result["attributes"]["inserted-at"]
      assert result["attributes"]["updated-at"]
      assert creator["id"] == Integer.to_string(current_user.id)
    end

    test "for an applicant with invalid data", %{conn: conn} do
      applicant = Fixtures.applicant()
      attrs = %{content: ""}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"errors" => [error]} = conn
        |> post(applicant_applicant_note_path(conn, :create, applicant.id), params)
        |> json_response(400)

      assert error["title"] == "can't be blank"
      assert error["detail"] == "Content can't be blank"
    end

    test "for a missing applicant", %{conn: conn} do
      attrs = %{content: "Hello, World!"}

      params = Poison.encode!(%{data: %{type: "note", attributes: attrs}})

      %{"errors" => [error]} = conn
        |> post(applicant_applicant_note_path(conn, :create, 1), params)
        |> json_response(400)

      assert error["title"] == "does not exist"
      assert error["detail"] == "Applicant does not exist"
    end
  end

  describe "getting all notes for an applicant" do
    setup [:create_user, :log_in]

    test "existing applicant", %{conn: conn} do
      applicant = Fixtures.applicant()
      Fixtures.notes(applicant, 51)

      %{"data" => result} = conn
        |> get(applicant_applicant_note_path(conn, :index, applicant.id))
        |> json_response(200)

      assert Kernel.length(result) == 51
    end

    test "existing applicant with pagination", %{conn: conn} do
      applicant = Fixtures.applicant()
      Fixtures.notes(applicant, 112)

      %{"data" => result} = conn
        |> get(applicant_applicant_note_path(conn, :index, applicant.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["content"] == "Hello62"
    end
  end

  describe "deleting a note for an applicant" do
    setup [:create_user, :log_in]

    test "by the creator", %{conn: conn, current_user: current_user} do
      applicant = Fixtures.applicant()
      note = Fixtures.note(%{applicant_id: applicant.id, created_by: current_user.id, content: "Hello!"})

      %{"data" => result} = conn
        |> delete(applicant_applicant_note_path(conn, :delete, applicant.id, note.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(note.id)
      refute Messaging.get_applicant_note(note.id)
    end

    test "by the non-creator", %{conn: conn} do
      applicant = Fixtures.applicant()
      note = Fixtures.note(%{applicant_id: applicant.id, created_by: applicant.created_by, content: "Hello!"})

      %{"errors" => [error]} = conn
        |> delete(applicant_applicant_note_path(conn, :delete, applicant.id, note.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert Messaging.get_applicant_note(note.id)
    end
  end

  defp create_user(%{conn: conn}) do
    user = Fixtures.user("CurrentUser")
    {:ok, conn: conn, current_user: user}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
      |> Plug.Conn.put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
