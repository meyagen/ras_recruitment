defmodule RasDirectoryApiWeb.ApplicantControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Fact
  alias RasDirectoryApi.FactTest
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.OrganizationTest
  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.RecruitmentTest

  @company_data %{
    name: "Mithiin Labs",
    phone_number: "837-39-75",
    email: "test@mithiinlabs.ph",
  }

  @applicant_data %{
    first_name: "Alice",
    last_name: "Walker",
    landline_number: "938-26-45",
    mobile_number: "+63918272346",
    phone_number: "272-30-27",
    other_number: "21312321312",
    email: "alice@yale.com",
  }

  describe "creating an applicant as a normal user" do
    setup [
      :create_user,
      :log_in,
      :create_country,
      :create_skills,
      :create_profession,
      :create_company,
    ]

    test "with valid complete data", %{
      conn: conn,
      current_user: current_user,
      country: country,
      skills: skills,
      profession: profession,
      company: company,
    } do

      applicant_attrs = @applicant_data
        |> Map.put(:country_id, country.id)
        |> Map.put(:skill_ids, Enum.map(skills, fn(skill) -> skill.id end))
        |> Map.put(:profession_id, profession.id)
        |> Map.put(:company_id, company.id)

      params = Poison.encode!(%{data: %{type: "applicant", attributes: applicant_attrs}})

      %{"data" => data, "included" => included} = conn
        |> post(applicant_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["first-name"] == "Alice"
      assert data["attributes"]["last-name"] == "Walker"
      assert data["attributes"]["landline-number"] == "938-26-45"
      assert data["attributes"]["mobile-number"] == "+63918272346"
      assert data["attributes"]["phone-number"] == "272-30-27"
      assert data["attributes"]["email"] == "alice@yale.com"
      assert data["attributes"]["created-by"] == current_user.id
      assert data["attributes"]["profession-id"] == profession.id
      assert data["attributes"]["country-id"] == country.id
      assert data["attributes"]["company-id"] == company.id
      assert Kernel.length(data["relationships"]["skills"]["data"]) == 3

      [skill1, skill2, skill3, included_profession, included_company, included_country, included_creator] = included
      assert skill1["id"] == Integer.to_string(Enum.at(applicant_attrs[:skill_ids], 0))
      assert skill2["id"] == Integer.to_string(Enum.at(applicant_attrs[:skill_ids], 1))
      assert skill3["id"] == Integer.to_string(Enum.at(applicant_attrs[:skill_ids], 2))
      assert included_profession["id"] == Integer.to_string(profession.id)
      assert included_company["id"] == Integer.to_string(company.id)
      assert included_country["id"] == Integer.to_string(country.id)
      assert included_creator["id"] == Integer.to_string(current_user.id)
    end

    test "with a valid incomplete data", %{
      conn: conn,
      current_user: user,
      profession: applicant_profession,
      country: applicant_country,
    } do
      applicant_attrs = @applicant_data
        |> Map.delete(:landline_number)
        |> Map.delete(:mobile_number)
        |> Map.delete(:phone_number)
        |> Map.delete(:email)
        |> Map.put(:created_by, user.id)
        |> Map.put(:profession_id, applicant_profession.id)
        |> Map.put(:country_id, applicant_country.id)

      params = Poison.encode!(%{data: %{type: "applicant", attributes: applicant_attrs}})

      %{"data" => data, "included" => [profession, country, creator]} = conn
        |> post(applicant_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["first-name"] == "Alice"
      assert data["attributes"]["last-name"] == "Walker"

      assert data["attributes"]["created-by"] == user.id
      assert data["attributes"]["profession-id"] == applicant_profession.id
      assert data["attributes"]["country-id"] == applicant_country.id
      assert data["attributes"]["company-id"] == nil

      assert profession["id"] == Integer.to_string(applicant_profession.id)
      assert country["id"] == Integer.to_string(applicant_country.id)
      assert creator["id"] == Integer.to_string(user.id)
    end

    test "with short names", %{
      conn: conn,
      current_user: current_user,
      profession: profession,
      country: country,
      skills: skills,
      company: company,
    } do
      applicant_attrs = @applicant_data
        |> Map.put(:first_name, "Ki")
        |> Map.put(:last_name, "Sy")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:profession_id, profession.id)
        |> Map.put(:country_id, country.id)
        |> Map.put(:skill_ids, Enum.map(skills, fn(skill) -> skill.id end))
        |> Map.put(:company_id, company.id)

      params = Poison.encode!(%{data: %{type: "applicant", attributes: applicant_attrs}})

      %{"data" => data} = conn
        |> post(applicant_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["first-name"] == "Ki"
      assert data["attributes"]["last-name"] == "Sy"
      assert data["attributes"]["landline-number"] == "938-26-45"
      assert data["attributes"]["mobile-number"] == "+63918272346"
      assert data["attributes"]["phone-number"] == "272-30-27"
      assert data["attributes"]["email"] == "alice@yale.com"

      assert data["attributes"]["created-by"] == current_user.id
      assert data["attributes"]["profession-id"] == profession.id
      assert data["attributes"]["country-id"] == country.id
      assert data["attributes"]["company-id"] == company.id
    end

    test "with no profession", %{
      conn: conn,
      current_user: user,
      country: applicant_country,
      company: applicant_company,
    } do
      applicant_attrs = @applicant_data
        |> Map.put(:created_by, user.id)
        |> Map.put(:country_id, applicant_country.id)
        |> Map.put(:company_id, applicant_company.id)

      params = Poison.encode!(%{data: %{type: "applicant", attributes: applicant_attrs}})

      %{"errors" => [error]} = conn
        |> post(applicant_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Profession can't be blank"
    end
  end

  describe "getting all applicants as an admin" do
    setup [:create_admin, :log_in]

    test "with no applicants", %{conn: conn} do
      %{"data" => []} = conn
        |> get(applicant_path(conn, :index))
        |> json_response(200)
    end

    test "with more than 1 applicants", %{conn: conn, current_user: current_user} do
      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant2} = Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant3} = Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [result1, result2, result3]} = conn
        |> get(applicant_path(conn, :index))
        |> json_response(200)

      assert result1["id"] == Integer.to_string(applicant1.id)
      assert result2["id"] == Integer.to_string(applicant2.id)
      assert result3["id"] == Integer.to_string(applicant3.id)
    end

    test "with complete sorting parameters", %{conn: conn} do
      Fixtures.applicants(50)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, sort_by: "updated_at", sort: "desc", count: 20))
        |> json_response(200)

      [first | _] = result
      assert Kernel.length(result) == 20
      assert first["attributes"]["first-name"] == "Juan50"
    end

    test "with sorting with last_note_at", %{conn: conn} do
      applicants = Fixtures.applicants(50)

      applicants
      |> Enum.at(30)
      |> Fixtures.notes(10)

      applicants
      |> Enum.at(20)
      |> Fixtures.notes(20)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, sort_by: "last_note_at", sort: "desc", count: 5))
        |> json_response(200)

      [first | _] = result
      assert Kernel.length(result) == 5
      assert first["attributes"]["first-name"] == "Juan21"
      second = Enum.at(result, 1)
      assert second["attributes"]["first-name"] == "Juan31"
    end


    test "with incomplete sorting parameters", %{conn: conn} do
      Fixtures.applicants(50)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, sort: "desc", count: 20))
        |> json_response(200)

      [first | _] = result
      assert Kernel.length(result) == 20
      assert first["attributes"]["first-name"] == "Juan50"
    end

    test "with pagination", %{conn: conn} do
      Fixtures.applicants(120)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, page: 2))
        |> json_response(200)

      [first | _] = result
      assert Kernel.length(result) == 50
      assert first["attributes"]["first-name"] == "Juan70"
    end
  end

  describe "getting all applicants as a normal user" do
    setup [:create_user, :log_in]

    test "with more than 1 applicants", %{conn: conn, current_user: current_user} do
      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end
  end

  describe "getting an applicant" do
    setup [:create_user, :log_in]

    test "as a creator of the applicant", %{conn: conn, current_user: current_user} do
      attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)

      applicant = RecruitmentTest.fixture(:applicant, attrs)

      %{"data" => data} = conn
        |> get(applicant_path(conn, :show, applicant.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(applicant.id)
    end

    test "as a non-creator of the applicant", %{conn: conn} do
      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      attrs = @applicant_data
        |> Map.put(:created_by, creator.id)

      applicant = RecruitmentTest.fixture(:applicant, attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :show, applicant.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead of the creator of the applicant", %{conn: conn, current_user: current_user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: current_user.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      attrs = @applicant_data
        |> Map.put(:created_by, creator.id)

      applicant = RecruitmentTest.fixture(:applicant, attrs)

      %{"data" => data} = conn
        |> get(applicant_path(conn, :show, applicant.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(applicant.id)
    end
  end

  describe "updating an applicant" do
    setup [:create_user, :log_in, :create_skills]

    test "as the creator with all valid data", %{conn: conn, current_user: current_user, skills: skills} do
      attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)

      applicant = RecruitmentTest.fixture(:applicant, attrs)

      new_country = FactTest.fixture(:country, %{name: "Australia"})
      new_profession = RecruitmentTest.fixture(:profession, "Engineering Manager")

      new_company_attrs = %{
        name: "Freelancer.com",
        phone_number: "936-15-16",
        email: "matt@freelancer.com",
        country_id: new_country.id,
        created_by: current_user.id
      }
      {:ok, new_company} = Recruitment.create_company(new_company_attrs)

      new_applicant_attrs = %{
        first_name: "Neil",
        last_name: "Calabroso",
        landline_number: "238-19-47",
        mobile_number: "09061124975",
        phone_number: "936-15-16",
        email: "nmcalabroso@outlook.ph",
        country_id: new_country.id,
        profession_id: new_profession.id,
        company_id: new_company.id,
        skill_ids: Enum.map(skills, fn(skill) -> skill.id end),
        status: "placed"
      }

      params = Poison.encode!(%{data: %{type: "applicant", attributes: new_applicant_attrs}})

      %{"data" => data, "included" => included} = conn
        |> patch(applicant_path(conn, :update, applicant.id), params)
        |> json_response(200)

      assert data["id"] == Integer.to_string(applicant.id)
      assert data["attributes"]["first-name"] == "Neil"
      assert data["attributes"]["last-name"] == "Calabroso"
      assert data["attributes"]["landline-number"] == "238-19-47"
      assert data["attributes"]["mobile-number"] == "09061124975"
      assert data["attributes"]["phone-number"] == "936-15-16"
      assert data["attributes"]["email"] == "nmcalabroso@outlook.ph"
      assert data["attributes"]["country-id"] == new_country.id
      assert data["attributes"]["profession-id"] == new_profession.id
      assert data["attributes"]["company-id"] == new_company.id
      assert data["attributes"]["created-by"] == current_user.id
      assert data["attributes"]["status"] == "placed"

      [skill1, skill2, skill3, included_profession, included_company, included_country, included_creator] = included

      assert skill1["id"] == Integer.to_string(Enum.at(new_applicant_attrs[:skill_ids], 0))
      assert skill2["id"] == Integer.to_string(Enum.at(new_applicant_attrs[:skill_ids], 1))
      assert skill3["id"] == Integer.to_string(Enum.at(new_applicant_attrs[:skill_ids], 2))
      assert included_profession["id"] == Integer.to_string(new_profession.id)
      assert included_company["id"] == Integer.to_string(new_company.id)
      assert included_country["id"] == Integer.to_string(new_country.id)
      assert included_creator["id"] == Integer.to_string(current_user.id)
    end

    test "as a team lead of the creator of the applicant", %{conn: conn, current_user: current_user, skills: skills} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: current_user.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      attrs = @applicant_data
        |> Map.put(:created_by, creator.id)

      applicant = RecruitmentTest.fixture(:applicant, attrs)

      new_country = FactTest.fixture(:country, %{name: "Australia"})
      new_profession = RecruitmentTest.fixture(:profession, "Engineering Manager")

      new_company_attrs = %{
        name: "Freelancer.com",
        phone_number: "936-15-16",
        email: "matt@freelancer.com",
        country_id: new_country.id,
        created_by: current_user.id
      }
      {:ok, new_company} = Recruitment.create_company(new_company_attrs)

      new_applicant_attrs = %{
        first_name: "Neil",
        last_name: "Calabroso",
        landline_number: "238-19-47",
        mobile_number: "09061124975",
        phone_number: "936-15-16",
        email: "nmcalabroso@outlook.ph",
        country_id: new_country.id,
        profession_id: new_profession.id,
        company_id: new_company.id,
        skill_ids: Enum.map(skills, fn(skill) -> skill.id end)
      }

      params = Poison.encode!(%{data: %{type: "applicant", attributes: new_applicant_attrs}})

      %{"data" => data} = conn
        |> patch(applicant_path(conn, :update, applicant.id), params)
        |> json_response(200)

      assert data["id"] == Integer.to_string(applicant.id)
      assert data["attributes"]["first-name"] == "Neil"
      assert data["attributes"]["last-name"] == "Calabroso"
      assert data["attributes"]["landline-number"] == "238-19-47"
      assert data["attributes"]["mobile-number"] == "09061124975"
      assert data["attributes"]["phone-number"] == "936-15-16"
      assert data["attributes"]["email"] == "nmcalabroso@outlook.ph"
      assert data["attributes"]["country-id"] == new_country.id
      assert data["attributes"]["profession-id"] == new_profession.id
      assert data["attributes"]["company-id"] == new_company.id
      assert data["attributes"]["created-by"] == creator.id
    end

    test "as the creator removing a skill", %{conn: conn, current_user: current_user} do
      elixir = Fixtures.skill("elixir")
      mysql = Fixtures.skill("mysql")
      applicant = Fixtures.applicant(creator: current_user, skills: [elixir, mysql])

      attrs = %{
        first_name: applicant.first_name,
        last_name: applicant.last_name,
        landline_number: applicant.landline_number,
        mobile_number: applicant.mobile_number,
        phone_number: applicant.phone_number,
        email: applicant.email,
        country_id: applicant.country_id,
        profession_id: applicant.profession_id,
        company_id: applicant.company_id,
        skill_ids: [elixir.id]
      }

      params = Poison.encode!(%{data: %{type: "applicant", attributes: attrs}})

      %{"data" => result} = conn
        |> patch(applicant_path(conn, :update, applicant.id), params)
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant.id)
      assert Kernel.length(result["relationships"]["skills"]["data"]) == 1
    end
  end

  describe "getting user's all created applicants" do
    setup [:create_user, :log_in]

    test "as the creator", %{conn: conn, current_user: current_user} do
      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant2} = Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant3} = Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [result1, result2, result3]} = conn
        |> get(applicant_path(conn, :index_by_creator, current_user.id))
        |> json_response(200)

      assert result1["id"] == Integer.to_string(applicant3.id)
      assert result2["id"] == Integer.to_string(applicant2.id)
      assert result3["id"] == Integer.to_string(applicant1.id)
    end

    test "as the creator with pagination", %{conn: conn, current_user: current_user} do
      Fixtures.applicants(120, creator: current_user)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index_by_creator, current_user.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["first-name"] == "Juan70"
    end

    test "as non-creator", %{conn: conn} do
      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_creator, creator.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "as team lead", %{conn: conn, current_user: current_user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: current_user.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant2} = Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant3} = Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [result1, result2, result3]} = conn
        |> get(applicant_path(conn, :index_by_creator, creator.id))
        |> json_response(200)

      assert result1["id"] == Integer.to_string(applicant3.id)
      assert result2["id"] == Integer.to_string(applicant2.id)
      assert result3["id"] == Integer.to_string(applicant1.id)
    end

    test "as the creator with sorting parameters", %{conn: conn, current_user: current_user} do
      Fixtures.applicants(20, creator: current_user)

      %{"data" => [result1, result2, result3]} = conn
        |> get(applicant_path(conn, :index_by_creator, current_user.id, sort_by: "first_name", sort: "desc", count: 3))
        |> json_response(200)

      assert result1["attributes"]["first-name"] == "Juan9"
      assert result2["attributes"]["first-name"] == "Juan8"
      assert result3["attributes"]["first-name"] == "Juan7"
    end

    test "as the creator with filter by first_name", %{conn: conn, current_user: current_user} do
      Fixtures.applicant(first_name: "AB", creator: current_user)
      Fixtures.applicant(first_name: "BA", creator: current_user)
      Fixtures.applicant(first_name: "AC", creator: current_user)
      Fixtures.applicant(first_name: "A2", creator: current_user)

      %{"data" => [result1, result2]} = conn
        |> get(applicant_path(conn, :index_by_creator, current_user.id, page: 1, count: 2, first_name: "A"))
        |> json_response(200)

      assert result1["attributes"]["first-name"] == "A2"
      assert result2["attributes"]["first-name"] == "AC"
    end
  end

  describe "getting all applicants in a company as an admin" do
    setup [:create_admin, :log_in]

    test "with more than one data", %{conn: conn} do
      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [applicant]} = conn
        |> get(applicant_path(conn, :index_by_company, applicant1.company_id))
        |> json_response(200)

      assert applicant["id"] == Integer.to_string(applicant1.id)
    end
  end

  describe "getting all applicants in a company as a normal user" do
    setup [:create_user, :log_in]

    test "with more than one data", %{conn: conn} do
      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_company, applicant1.company_id))
        |> json_response(400)

        assert error["title"] == "User is unauthorized"
        assert error["status"] == 403
    end
  end

  describe "getting user's created applicants in a company" do
    setup [:create_user, :log_in]

    test "as the creator", %{conn: conn, current_user: current_user} do
      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, current_user.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, current_user.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [result]} = conn
        |> get(applicant_path(conn, :index_by_company_and_creator, applicant1.company_id, current_user.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant1.id)
    end

    test "as non-creator", %{conn: conn} do
      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_company_and_creator, applicant1.company_id, creator.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "as team lead", %{conn: conn, current_user: current_user} do
      team = OrganizationTest.fixture(:team)
      Organization.create_membership(%{team_id: team.id, user_id: current_user.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      applicant3_attrs = @applicant_data
        |> Map.put(:first_name, "Louis")
        |> Map.put(:last_name, "Clarak")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant3_attrs)

      %{"data" => [result]} = conn
        |> get(applicant_path(conn, :index_by_company_and_creator, applicant1.company_id, creator.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant1.id)
    end
  end

  describe "getting all applicants by team" do
    setup [:create_team, :create_user, :log_in]

    test "as a team lead with single team", %{conn: conn, current_user: current_user, team: team} do
      Organization.create_membership(%{user_id: current_user.id, team_id: team.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      {:ok, applicant2} = Recruitment.create_applicant(applicant2_attrs)

      %{"data" => [result1, result2]} = conn
        |> get(applicant_path(conn, :index_by_team, team.id))
        |> json_response(200)

      assert result1["id"] == Integer.to_string(applicant2.id)
      assert result2["id"] == Integer.to_string(applicant1.id)
    end

    test "as a team lead with single team with pagination", %{conn: conn, current_user: current_user, team: team} do
      Organization.create_membership(%{user_id: current_user.id, team_id: team.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
       Recruitment.create_applicant(applicant2_attrs)

      %{"data" => [result]} = conn
        |> get(applicant_path(conn, :index_by_team, team.id, page: 2, count: 1))
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant1.id)
    end

    test "as a normal user with single team", %{conn: conn, current_user: current_user, team: team} do
      Organization.create_membership(%{user_id: current_user.id, team_id: team.id, role: 1})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_team, team.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end
  end

  describe "getting applicants by team in a company" do
    setup [:create_team, :create_user, :log_in]

    test "as a team lead", %{conn: conn, current_user: current_user, team: team} do
      Organization.create_membership(%{user_id: current_user.id, team_id: team.id, role: 2})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
      Recruitment.create_applicant(applicant2_attrs)

      %{"data" => [result1]} = conn
        |> get(applicant_path(conn, :index_by_company_and_team, applicant1.company_id, team.id))
        |> json_response(200)

      assert result1["id"] == Integer.to_string(applicant1.id)
    end

    test "as a normal user", %{conn: conn, current_user: current_user, team: team} do
      Organization.create_membership(%{user_id: current_user.id, team_id: team.id, role: 1})

      creator_attrs = %{
        username: "nmcalabroso",
        password: "12345678",
        email: "neil@rasrecruitment.com",
        first_name: "RaS",
        last_name: "Admin",
        position: "President"
      }
      creator = AccountTest.fixture(:user, creator_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: creator.id, role: 1})

      applicant1_attrs = @applicant_data
        |> Map.put(:created_by, creator.id)
      applicant1 = RecruitmentTest.fixture(:applicant, applicant1_attrs)

      applicant2_attrs = @applicant_data
        |> Map.put(:first_name, "Lance")
        |> Map.put(:last_name, "Calabroso")
        |> Map.put(:created_by, creator.id)
        |> Map.put(:country_id, applicant1.country_id)
        |> Map.put(:profession_id, applicant1.profession_id)
        |> Map.put(:company_id, applicant1.company_id)
      Recruitment.create_applicant(applicant2_attrs)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_company_and_team, applicant1.company_id, team.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead with pagination and filter parameters", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      member = Fixtures.user("MyMember")
      Fixtures.member(team, member)
      company = Fixtures.company("CompanyTeam")
      Fixtures.applicant(first_name: "AA", company: company, creator: member)
      Fixtures.applicant(first_name: "BD", company: company, creator: member)
      Fixtures.applicant(first_name: "CF", company: company, creator: member)
      Fixtures.applicant(first_name: "AD", company: company, creator: member)
      Fixtures.applicant(first_name: "AC", creator: member)

      %{"data" => [result1]} = conn
        |> get(applicant_path(conn, :index_by_company_and_team, company.id, team.id, first_name: "A", page: 2, count: 1))
        |> json_response(200)

      assert result1["attributes"]["first-name"] == "AA"
    end
  end

  describe "getting applicants managed by a team lead" do
    setup [:create_user, :log_in]

    test "as a team lead of a single team", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.applicants(5, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.applicants(5, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 5
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
    end

    test "as a team lead of a single team with pagination", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.applicants(100, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.applicants(10, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
      assert head["attributes"]["first-name"] == "Juan50"
    end

    test "as a team lead with his own applicants with pagination", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      Fixtures.applicants(80, creator: current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.applicants(10, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.applicants(10, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id, page: 1))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
      assert head["attributes"]["first-name"] == "Juan10"
    end

    test "as a user without a team under management", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.member(team, current_user)
      Fixtures.applicants(80, creator: current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.applicants(10, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.applicants(10, creator: creator_non_member)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead inspecting other team leads", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      Fixtures.applicants(80, creator: current_user)

      team2 = Fixtures.team("NewCompany")
      creator_member = Fixtures.user("Creator1")
      Fixtures.team_lead(team2, creator_member)
      Fixtures.applicants(10, creator: creator_member)

      %{"errors" => [error]} = conn
        |> get(applicant_path(conn, :index_by_team_lead, creator_member.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead with sorting parameters", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      creator = Fixtures.user("Creator")
      Fixtures.member(team, creator)
      Fixtures.applicant(first_name: "AA", creator: creator)
      Fixtures.applicant(first_name: "BB", creator: creator)
      some_member = Fixtures.user("SomeMember")
      Fixtures.member(team, some_member)
      skill = Fixtures.skill("VueJS")
      Fixtures.applicant(first_name: "CC", creator: some_member, skills: [skill])

      %{"data" => [result1, result2]} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id, created_by: creator.id))
        |> json_response(200)

      assert result1["attributes"]["first-name"] == "BB"
      assert result2["attributes"]["first-name"] == "AA"

      %{"data" => [result3]} = conn
        |> get(applicant_path(conn, :index_by_team_lead, current_user.id, skill_id: skill.id))
        |> json_response(200)

      assert result3["attributes"]["first-name"] == "CC"
    end
  end

  describe "filtering applicants as an admin" do
    setup [:create_admin, :log_in]

    test "by last_name", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.applicant([last_name: "AB"])
      Fixtures.applicant([last_name: "NA"])
      Fixtures.applicant([last_name: "nB"])
      Fixtures.applicant([last_name: "AC"])
      Fixtures.applicant([last_name: "CD"])
      Fixtures.applicant([last_name: "DDE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, last_name: "N"))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["last-name"] == "nB"
      assert applicant2["attributes"]["last-name"] == "NA"
    end

    test "by first_name with pagination", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.applicant([first_name: "AB"])
      Fixtures.applicant([first_name: "NA"])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, first_name: "A", page: 2, count: 2))
        |> json_response(200)

      assert Kernel.length(result) == 1
      [applicant1] = result
      assert applicant1["attributes"]["first-name"] == "AB"
    end

    test "by profession", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      profession = Fixtures.profession("Writer")
      Fixtures.applicant([first_name: "AB", profession: profession])
      Fixtures.applicant([first_name: "NA", profession: profession])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, profession_id: profession.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["first-name"] == "NA"
      assert applicant2["attributes"]["first-name"] == "AB"
    end

    test "by country", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      country = Fixtures.country("El Salvador")
      Fixtures.applicant([first_name: "AB", country: country])
      Fixtures.applicant([first_name: "NA", country: country])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, country_id: country.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["first-name"] == "NA"
      assert applicant2["attributes"]["first-name"] == "AB"
    end

    test "by creator", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.applicant([first_name: "AB", creator: current_user])
      Fixtures.applicant([first_name: "NA", creator: current_user])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, created_by: current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["first-name"] == "NA"
      assert applicant2["attributes"]["first-name"] == "AB"
    end

    test "by skill", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      target_skill = Fixtures.skill("Elixir")
      Fixtures.applicant([first_name: "AB", skills: [target_skill]])
      Fixtures.applicant([first_name: "NA", skills: [Fixtures.skill("PHP"), target_skill]])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, skill_id: target_skill.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["first-name"] == "NA"
      assert applicant2["attributes"]["first-name"] == "AB"
    end

    test "by company", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      target_company = Fixtures.company("Upwork Inc. Ltd.")
      target_skill = Fixtures.skill("Elixir")
      Fixtures.applicant([first_name: "AB", skills: [target_skill], company: target_company])
      Fixtures.applicant([first_name: "NA", skills: [Fixtures.skill("PHP"), target_skill], company: target_company])
      Fixtures.applicant([first_name: "nB"])
      Fixtures.applicant([first_name: "aC"])
      Fixtures.applicant([first_name: "CD"])
      Fixtures.applicant([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(applicant_path(conn, :index, company_id: target_company.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [applicant1, applicant2] = result
      assert applicant1["attributes"]["first-name"] == "NA"
      assert applicant2["attributes"]["first-name"] == "AB"
    end
  end

  describe "deleting a candidate" do
    setup [:create_user, :log_in]

    test "as the creator", %{conn: conn, current_user: current_user} do
      applicant = Fixtures.applicant(creator: current_user)

      %{"data" => result} = conn
        |> delete(applicant_path(conn, :delete, applicant.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant.id)
      assert Recruitment.get_applicant(applicant.id) == nil
    end

    test "as a non-creator", %{conn: conn} do
      applicant = Fixtures.applicant()

      %{"errors" => [error]} = conn
        |> delete(applicant_path(conn, :delete, applicant.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "with attached skills", %{conn: conn, current_user: current_user} do
      skill = Fixtures.skill()
      applicant = Fixtures.applicant(creator: current_user, skills: [skill])

      %{"data" => result} = conn
        |> delete(applicant_path(conn, :delete, applicant.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(applicant.id)
      assert Recruitment.get_applicant(applicant.id) == nil
      assert Recruitment.get_skill(skill.id)
    end
  end

  defp create_country(%{conn: conn}) do
    {:ok, country} = Fact.create_country(%{name: "Philippines"})
    {:ok, conn: conn, country: country}
  end

  defp create_company(%{conn: conn, current_user: user, country: country}) do
    company_attrs = @company_data
      |> Map.put(:created_by, user.id)
      |> Map.put(:country_id, country.id)

    {:ok, company} = Recruitment.create_company(company_attrs)
    {:ok, conn: conn, company: company}
  end

  defp create_profession(%{conn: conn}) do
    {:ok, profession} = Recruitment.create_profession(%{name: "Software Engineer"})
    {:ok, conn: conn, profession: profession}
  end

  defp create_skills(%{conn: conn}) do
    {:ok, skill1} = Recruitment.create_skill(%{name: "Python"})
    {:ok, skill2} = Recruitment.create_skill(%{name: "Agile"})
    {:ok, skill3} = Recruitment.create_skill(%{name: "DevOps"})

    {:ok, conn: conn, skills: [skill1, skill2, skill3]}
  end

  defp create_user(%{conn: conn}) do
    user = Fixtures.user()
    {:ok, conn: conn, current_user: user}
  end

  defp create_admin(%{conn: conn}) do
    user = Fixtures.admin()
    {:ok, conn: conn, current_user: user}
  end

  def create_team(%{conn: conn}) do
    team = OrganizationTest.fixture(:team)
    %{conn: conn, team: team}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
