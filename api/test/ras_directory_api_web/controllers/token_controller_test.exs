defmodule RasDirectoryApiWeb.TokenControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest

  test "sign in with valid credential and set the token in the Authorization header", %{conn: conn} do
    AccountTest.fixture(:admin)
    credentials = %{username: "admin", password: "12345678"}
    params = Poison.encode!(%{data: %{type: "token", attributes: credentials}})

    conn = conn |> post(token_path(conn, :create), params)
    response = json_response(conn, 200)

    assert ["Bearer " <> jwt] = get_resp_header(conn, "authorization")
    data = response["data"]
    assert data["type"] == "token"
    assert data["id"] == jwt
    assert data["attributes"]["expiration"]
  end

  test "sign in with invalid credential should return an error", %{conn: conn} do
    AccountTest.fixture(:admin)
    credentials = %{username: "admin123", password: "12345678"}
    params = Poison.encode!(%{data: %{type: "token", attributes: credentials}})

    conn = conn |> post(token_path(conn, :create), params)
    response = json_response(conn, 400)

    assert [] = get_resp_header(conn, "authorization")
    [error | _] = response["errors"]
    assert error["status"] == 422
    assert error["id"] == "invalid_credentials"
  end
end
