defmodule RasDirectoryApiWeb.UserControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Auth
  alias RasDirectoryApi.Fixtures

  @create_attrs %{
    username: "louisck",
    password: "12345678",
    email: "louisck@rasrecruitment.com",
    first_name: "Louis",
    last_name: "C.K.",
    position: "Normal",
    role: "user",
    status: 1
  }

  describe "creation of user by an admin" do
    setup [:create_admin, :log_in]
    test "with valid and complete data", %{conn: conn} do
      params = Poison.encode!(%{data: %{type: "user", attributes: @create_attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] > 0
      assert data["attributes"]["username"] == "louisck"
      assert data["attributes"]["email"] == "louisck@rasrecruitment.com"
      assert data["attributes"]["role"] == "user"
      assert data["attributes"]["first-name"] == "Louis"
      assert data["attributes"]["last-name"] == "C.K."
      refute data["attributes"]["password"]

      user = Account.get_user(data["id"])
      assert user.username == "louisck"
    end

    test "with only required fields", %{conn: conn} do
      attrs = @create_attrs
        |> Map.drop([:first_name, :last_name])

      params = Poison.encode!(%{data: %{type: "user", attributes: attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] > 0
      assert data["attributes"]["username"] == "louisck"
      assert data["attributes"]["email"] == "louisck@rasrecruitment.com"
      assert data["attributes"]["role"] == "user"
      refute data["attributes"]["first-name"]
      refute data["attributes"]["last-name"]
      refute data["attributes"]["password"]

      user = Account.get_user(data["id"])
      assert user.username == "louisck"
    end

    test "without role", %{conn: conn} do
      attrs = @create_attrs
        |> Map.delete(:role)

      params = Poison.encode!(%{data: %{type: "user", attributes: attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] > 0
      assert data["attributes"]["username"] == "louisck"
      assert data["attributes"]["email"] == "louisck@rasrecruitment.com"
      assert data["attributes"]["role"] == "user"
      assert data["attributes"]["first-name"] == "Louis"
      assert data["attributes"]["last-name"] == "C.K."
      refute data["attributes"]["password"]

      user = Account.get_user(data["id"])
      assert user.username == "louisck"
    end

    test "with invalid data", %{conn: conn} do
      attrs = @create_attrs
        |> Map.delete(:username)

      params = Poison.encode!(%{data: %{type: "user", attributes: attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["title"] == "can't be blank"
      assert error["detail"] == "Username can't be blank"

      refute Account.get_by_username(@create_attrs[:username])
    end
  end

  describe "creation of user by a logged-in non-admin user" do
    setup [:create_user, :log_in]

    test "with valid data", %{conn: conn} do
      params = Poison.encode!(%{type: "user", data: %{attributes: @create_attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 403
      assert error["id"] == "unauthorized_user"
      assert error["title"] == "User is unauthorized"

      refute Account.get_by_username(@create_attrs[:username])
    end
  end

  describe "creation of user by a logged-out user" do
    test "with valid data", %{conn: conn} do
      params = Poison.encode!(%{type: "user", data: %{attributes: @create_attrs}})

      conn = conn |> post(user_path(conn, :create), params)
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 401
      assert error["id"] == "unauthenticated_user"
      assert error["title"] == "User is unauthenticated"

      refute Account.get_by_username(@create_attrs[:username])
    end
  end

  describe "banning of user by an admin" do
    setup [:create_admin, :log_in]
    test "with an active user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> delete(user_path(conn, :delete, user.id))
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["attributes"]["status"] == "banned"
      refute data["attributes"]["password"]

      user = Account.get_user(data["id"])
      refute Account.is_active(user)
    end

    test "with an already banned user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1, status: 0})

      conn = conn |> delete(user_path(conn, :delete, user.id))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 410
      assert error["id"] == "invalid_user_ban"
      assert error["title"] == "Invalid user to ban"
      assert error["detail"] == "User is no longer active"
    end

    test "with a non-existing user", %{conn: conn} do
      conn = conn |> delete(user_path(conn, :delete, 1))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 404
    end
  end

  describe "banning of user by non-admin" do
    setup [:create_user, :log_in]
    test "with an active user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> delete(user_path(conn, :delete, user.id))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 403
      assert error["id"] == "unauthorized_user"
      assert error["title"] == "User is unauthorized"
    end
  end

  describe "banning of user by a logged-out user" do
    test "with an active user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> delete(user_path(conn, :delete, user.id))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 401
      assert error["id"] == "unauthenticated_user"
      assert error["title"] == "User is unauthenticated"
    end
  end

  describe "getting user information by id" do
    setup [:create_user, :log_in]
    test "with an active user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> get(user_path(conn, :show, user.id))
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] > 0
      assert data["attributes"]["username"] == "louisck"
      assert data["attributes"]["email"] == "louisck@rasrecruitment.com"
      assert data["attributes"]["role"] == "user"
      assert data["attributes"]["first-name"]
      assert data["attributes"]["last-name"]
      assert data["attributes"]["status"] == "active"
      assert data["attributes"]["position"] == "Normal"
      refute data["attributes"]["password"]
    end

    test "with non-existing user", %{conn: conn} do
      conn = conn |> get(user_path(conn, :show, 1))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 404
    end
  end

  describe "getting user information by username" do
    setup [:create_user, :log_in]
    test "with an active user", %{conn: conn} do
      user = AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> get(user_path(conn, :get_by_username, user.username))
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] > 0
      assert data["attributes"]["username"] == "louisck"
      assert data["attributes"]["email"] == "louisck@rasrecruitment.com"
      assert data["attributes"]["role"] == "user"
      assert data["attributes"]["first-name"]
      assert data["attributes"]["last-name"]
      assert data["attributes"]["status"] == "active"
      assert data["attributes"]["position"] == "Normal"
      refute data["attributes"]["password"]
    end

    test "with non-existing user", %{conn: conn} do
      conn = conn |> get(user_path(conn, :get_by_username, "louisck"))
      response = json_response(conn, 400)

      [error | _] = response["errors"]
      assert error["status"] == 404
    end
  end

  describe "updating user's own profile" do
    setup [:create_user, :log_in]
    test "with valid data", %{conn: conn, user: user} do
      attrs = %{
        first_name: "Neil",
        last_name: "Calabroso",
        email: "nmcalabroso@rasrecruitment.com",
        password: "helloworld"
      }

      params = Poison.encode!(%{data: %{type: "user", id: user.id, attributes: attrs}})

      conn = conn |> patch(user_path(conn, :update, user.id), params)
      response = json_response(conn, 200)

      data = response["data"]
      assert data["type"] == "user"
      assert data["id"] == Integer.to_string(user.id)
      assert data["attributes"]["username"] == user.username
      assert data["attributes"]["email"] == "nmcalabroso@rasrecruitment.com"
      assert data["attributes"]["first-name"] == "Neil"
      assert data["attributes"]["last-name"] == "Calabroso"
      refute data["attributes"]["password"]

      updated_user = Account.get_user(data["id"])
      assert {:ok, %User{}} = Auth.sign_in(updated_user.username, "helloworld")
    end
  end

  describe "getting all users by admin" do
    setup [:create_admin, :log_in]
    test "many users", %{conn: conn} do
      AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> get(user_path(conn, :index))
      response = json_response(conn, 200)

      data = response["data"]
      assert Kernel.length(data) == 2
    end

    test "single user", %{conn: conn} do
      conn = conn |> get(user_path(conn, :index))
      response = json_response(conn, 200)

      data = response["data"]
      assert Kernel.length(data) == 1
    end
  end

  describe "getting all users by non-admin" do
    setup [:create_user, :log_in]
    test "many users", %{conn: conn} do
      AccountTest.fixture(:user, %{@create_attrs | role: 1})

      conn = conn |> get(user_path(conn, :index))
      response = json_response(conn, 200)

      data = response["data"]
      assert Kernel.length(data) == 2
    end

    test "single user", %{conn: conn} do
      conn = conn |> get(user_path(conn, :index))
      response = json_response(conn, 200)

      data = response["data"]
      assert Kernel.length(data) == 1
    end
  end

  describe "activating user" do
    setup [:create_admin, :log_in]

    test "already banned user", %{conn: conn} do
      banned_user = Fixtures.user("BannedUser")
      Account.update_status(banned_user.id, 0)


      %{"data" => result} = conn
        |> patch(user_path(conn, :activate, banned_user.id))
        |> json_response(200)

      assert result["id"] == Integer.to_string(banned_user.id)
      assert result["attributes"]["status"] == "active"
    end
  end

  defp create_user(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, user: user}
  end

  defp create_admin(%{conn: conn}) do
    user = AccountTest.fixture(:admin)
    {:ok, conn: conn, user: user}
  end

  defp log_in(%{conn: conn, user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
