defmodule RasDirectoryApiWeb.CompanyControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Fact

  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.RecruitmentTest

  @company_data %{
    name: "Clean Water Initiative",
    phone_number: "936-15-16",
    email: "president@cwi.ph"
  }

  describe "creating a company as a normal user" do
    setup [:create_country, :create_user, :log_in]

    test "with a valid complete data", %{conn: conn, country: company_country, current_user: user} do
      company_attrs = @company_data
        |> Map.put(:country_id, company_country.id)

      params = Poison.encode!(%{data: %{type: "company", attributes: company_attrs}})

      %{"data" => data, "included" => [country, creator]} = conn
        |> post(company_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["name"] == "Clean Water Initiative"
      assert data["attributes"]["country-id"] == company_country.id
      assert data["attributes"]["created-by"] == user.id
      assert data["relationships"]["creator"]
      assert data["relationships"]["country"]
      assert creator["id"] == Integer.to_string(user.id)
      assert country["id"] == Integer.to_string(company_country.id)
    end

    test "with a valid incomplete data", %{conn: conn, country: company_country, current_user: user} do
      company_attrs = @company_data
        |> Map.delete(:phone_number)
        |> Map.delete(:email)
        |> Map.put(:country_id, company_country.id)

      params = Poison.encode!(%{data: %{type: "company", attributes: company_attrs}})

      %{"data" => data, "included" => [country, creator]} = conn
        |> post(company_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["name"] == "Clean Water Initiative"
      assert data["attributes"]["country-id"] == company_country.id
      assert data["attributes"]["created-by"] == user.id
      assert data["relationships"]["creator"]
      assert creator["id"] == Integer.to_string(user.id)
      assert country["id"] == Integer.to_string(company_country.id)
    end

    test "with short name", %{conn: conn, country: company_country} do
      company_attrs = @company_data
        |> Map.put(:name, "A")
        |> Map.delete(:phone_number)
        |> Map.delete(:email)
        |> Map.put(:country_id, company_country.id)

      params = Poison.encode!(%{data: %{type: "company", attributes: company_attrs}})

      %{"errors" => [error]} = conn
        |> post(company_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name should be at least 3 character(s)"
    end

    test "with no country", %{conn: conn} do
      company_attrs = @company_data
        |> Map.delete(:phone_number)
        |> Map.delete(:email)

      params = Poison.encode!(%{data: %{type: "company", attributes: company_attrs}})

      %{"errors" => [error]} = conn
        |> post(company_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Country can't be blank"
    end
  end

  describe "getting a company as a normal user" do
    setup [:create_user, :log_in]

    test "that is existing", %{conn: conn} do
      company = RecruitmentTest.fixture(:company)

      %{"data" => data, "included" => [country, creator]} = conn
        |> get(company_path(conn, :show, company.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(company.id)
      assert data["type"] == "company"
      assert data["attributes"]["name"] == "Clean Water Initiative"
      assert data["relationships"]["creator"]
      assert creator["id"] == Integer.to_string(company.created_by)
      assert country["id"] == Integer.to_string(company.country_id)
    end

    test "that is non-existing", %{conn: conn} do
      %{"errors" => [error]} = conn
        |> get(company_path(conn, :show, 1))
        |> json_response(400)

      assert error["status"] == 404
      assert error["detail"] == "Resource cannot be found"
    end
  end

  describe "updating a company" do
    setup [:create_user, :log_in]

    test "as a creator", %{conn: conn, current_user: user} do
      attrs = @company_data
        |> Map.put(:created_by, user.id)

      company = RecruitmentTest.fixture(:vanilla_company, attrs)

      attrs = attrs
        |> Map.put(:email, "hello@yahoo.com")

      params = Poison.encode!(%{data: %{type: "company", attributes: attrs}})

      %{"data" => data, "included" => [_, _]} = conn
        |> put(company_path(conn, :update, company.id), params)
        |> json_response(200)

      assert data["id"] == Integer.to_string(company.id)
      assert data["attributes"]["name"] == company.name
      assert data["attributes"]["created-by"] == user.id
      assert data["attributes"]["email"] == "hello@yahoo.com"
      assert data["attributes"]["country-id"] == company.country_id
    end

    test "as a non-creator", %{conn: conn} do
      creator_attrs = %{
        username: "member",
        password: "12345678",
        email: "member@rasrecruitment.com",
        first_name: "Nino",
        last_name: "Dennis",
        position: "President",
        role: 1,
        status: 1
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      attrs = @company_data
        |> Map.put(:created_by, creator.id)

      company = RecruitmentTest.fixture(:vanilla_company, attrs)

      attrs = attrs
        |> Map.put(:name, "Capitol Hill")

      params = Poison.encode!(%{data: %{type: "company", attributes: attrs}})

      %{"data" => data} = conn
        |> put(company_path(conn, :update, company.id), params)
        |> json_response(200)

      assert data["id"] == Integer.to_string(company.id)
      assert data["attributes"]["name"] == "Capitol Hill"
      assert data["attributes"]["created-by"] == creator.id
    end

    test "as an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      creator_attrs = %{
        username: "member",
        password: "12345678",
        email: "member@rasrecruitment.com",
        first_name: "Nino",
        last_name: "Dennis",
        position: "President",
        role: 1,
        status: 1
      }
      creator = AccountTest.fixture(:user, creator_attrs)

      attrs = @company_data
        |> Map.put(:created_by, creator.id)

      company = RecruitmentTest.fixture(:vanilla_company, attrs)

      new_country = Fixtures.country("Australia")
      new_creator = Fixtures.user

      attrs = attrs
        |> Map.put(:name, "Capitol Hill")
        |> Map.put(:email, "hello@yahoo.com")
        |> Map.put(:country_id, new_country.id)
        |> Map.put(:created_by, new_creator.id)

      params = Poison.encode!(%{data: %{type: "company", attributes: attrs}})

      %{"data" => data, "included" => [country, creator]} = conn
        |> put(company_path(conn, :update, company.id), params)
        |> json_response(200)

      assert data["id"] == Integer.to_string(company.id)
      assert data["attributes"]["name"] == "Capitol Hill"
      assert data["attributes"]["email"] == "hello@yahoo.com"
      assert data["attributes"]["created-by"] == new_creator.id
      assert data["attributes"]["country-id"] == new_country.id

      assert country["type"] == "country"
      assert country["id"] == Integer.to_string(new_country.id)

      assert creator["type"] == "user"
      assert creator["id"] == Integer.to_string(new_creator.id)
    end
  end

  describe "getting all companies" do
    setup [:create_user, :log_in]

    test "as a normal user", %{conn: conn} do
      cwi = RecruitmentTest.fixture(:company)
      {:ok, capitol_hill} = Recruitment.create_company(%{name: "Capitol Hill", country_id: cwi.country_id, created_by: cwi.created_by})
      {:ok, freelancer} = Recruitment.create_company(%{name: "Freelancer", country_id: cwi.country_id, created_by: cwi.created_by})

      %{"data" => [company1, company2, company3]} = conn
        |> get(company_path(conn, :index))
        |> json_response(200)

      assert company1["id"] == Integer.to_string(cwi.id)
      assert company2["id"] == Integer.to_string(capitol_hill.id)
      assert company3["id"] == Integer.to_string(freelancer.id)
    end

    test "as a normal user with no existing companies", %{conn: conn} do
      %{"data" => data} = conn
        |> get(company_path(conn, :index))
        |> json_response(200)

      assert data == []
    end
  end

  defp create_country(%{conn: conn}) do
    {:ok, country} = Fact.create_country(%{name: "Philippines"})
    {:ok, conn: conn, country: country}
  end

  defp create_user(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, current_user: user}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
