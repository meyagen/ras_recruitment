defmodule RasDirectoryApiWeb.TeamControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest

  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.OrganizationTest

  @team_lead_attrs %{
    username: "team_lead",
    password: "12345678",
    email: "team_lead@rasrecruitment.com",
    first_name: "Juan",
    last_name: "Dela Cruz",
    position: "President",
    role: 2,
    status: 1
  }

  describe "creation of team by an admin" do
    setup [:create_admin, :log_in]

    test "with valid and complete data", %{conn: conn} do
      attrs = %{data: %{type: "team", attributes: %{name: "MySuperTeam"}}}
      params = Poison.encode!(attrs)

      %{"data" => data} = conn
        |> post(team_path(conn, :create), params)
        |> json_response(200)

      assert data["type"] == "team"
      assert data["id"] > 0
      assert data["attributes"]["name"] == "MySuperTeam"
      assert data["attributes"]["active?"]
      assert data["attributes"]["inserted-at"]
      assert data["attributes"]["updated-at"]
      assert data["relationships"]["memberships"]["data"] == []
    end

    test "with duplicate name", %{conn: conn} do
      team = OrganizationTest.fixture(:team)

      attrs = %{data: %{type: "team", attributes: %{name: team.name}}}
      params = Poison.encode!(attrs)

      %{"errors" => [error]} = conn
        |> post(team_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name has already been taken"
    end
  end

  describe "getting a team" do
    setup [:create_non_admin, :log_in]

    test "existing team", %{conn: conn} do
      team = OrganizationTest.fixture(:team)

      %{"data" => data} = conn
        |> get(team_path(conn, :show, team.id))
        |> json_response(200)

      assert data["type"] == "team"
      assert data["id"] > 0
      assert data["attributes"]["name"] == "MySuperTeam"
      assert data["attributes"]["active?"]
      assert data["attributes"]["inserted-at"]
      assert data["attributes"]["updated-at"]
    end

    test "existing team with members", %{conn: conn} do
      team = OrganizationTest.fixture(:team)

      team_lead = AccountTest.fixture(:user, @team_lead_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: team_lead.id, role: 2})

      member_attrs = @team_lead_attrs
        |> Map.put(:username, "member")
        |> Map.put(:email, "member@rasrecruitment.com")

      member = AccountTest.fixture(:user, member_attrs)
      Organization.create_membership(%{team_id: team.id, user_id: member.id, role: 1})

      %{"data" => data, "included" => included} = conn
        |> get(team_path(conn, :show, team.id))
        |> json_response(200)

      assert data["type"] == "team"
      assert data["id"] > 0
      assert data["attributes"]["name"] == "MySuperTeam"
      assert data["attributes"]["active?"]
      assert data["attributes"]["inserted-at"]
      assert data["attributes"]["updated-at"]
      assert Kernel.length(data["relationships"]["memberships"]["data"]) == 2
      assert Kernel.length(included) == 2
    end

    test "non-existing team", %{conn: conn} do
      %{"errors" => [error]} = conn
        |> get(team_path(conn, :show, 1))
        |> json_response(400)

      assert error["status"] == 404
    end
  end

  describe "getting all teams" do
    setup [:create_admin, :log_in]

    test "more than one", %{conn: conn} do
      [team1, team2, team3] = Fixtures.teams(3)

      %{"data" => [result1, result2, result3]} = conn
        |> get(team_path(conn, :index))
        |> json_response(200)

      assert result1["attributes"]["name"] == team1.name
      assert result2["attributes"]["name"] == team2.name
      assert result3["attributes"]["name"] == team3.name
    end

    test "more than one with memberships", %{conn: conn} do
      team_lead = Fixtures.user()
      [team1, team2, team3] = Fixtures.teams(3)
      Organization.create_membership(%{team_id: team1.id, user_id: team_lead.id, role: 2})
      Organization.create_membership(%{team_id: team2.id, user_id: team_lead.id, role: 2})
      Organization.create_membership(%{team_id: team3.id, user_id: team_lead.id, role: 2})

      %{"data" => [result1, result2, result3], "included" => included} = conn
        |> get(team_path(conn, :index))
        |> json_response(200)

      assert result1["attributes"]["name"] == team1.name
      assert Kernel.length(result1["relationships"]["memberships"]["data"]) == 1
      assert result2["attributes"]["name"] == team2.name
      assert Kernel.length(result2["relationships"]["memberships"]["data"]) == 1
      assert result3["attributes"]["name"] == team3.name
      assert Kernel.length(result3["relationships"]["memberships"]["data"]) == 1
      assert Kernel.length(included) == 3
    end

    test "with complete sorting parameters", %{conn: conn} do
      Fixtures.teams(50)

      %{"data" => [result1, result2, result3]} = conn
        |> get(team_path(conn, :index, sort_by: "name", sort: "desc", count: 3))
        |> json_response(200)

      assert result1["attributes"]["name"] == "Team9"
      assert result2["attributes"]["name"] == "Team8"
      assert result3["attributes"]["name"] == "Team7"
    end

    test "with incomplete sorting parameters", %{conn: conn} do
      Fixtures.teams(50)

      %{"data" => [result1, result2, result3]} = conn
        |> get(team_path(conn, :index, sort_by: "name", count: 3))
        |> json_response(200)

      assert result1["attributes"]["name"] == "Team9"
      assert result2["attributes"]["name"] == "Team8"
      assert result3["attributes"]["name"] == "Team7"
    end
  end

  describe "updating a team as a team lead" do
    setup [:create_team_lead, :log_in]

    test "with valid data", %{conn: conn, membership: membership} do
      member_attrs = @team_lead_attrs
        |> Map.put(:username, "member")
        |> Map.put(:email, "member@rasrecruitment.com")

      member = AccountTest.fixture(:user, member_attrs)
      Organization.create_membership(%{team_id: membership.team_id, user_id: member.id, role: 1})

      params = Poison.encode!(%{data: %{type: "team", id: membership.team_id, attributes: %{"name" => "NotSuperTeam"}}})

      %{"data" => data, "included" => [team_lead, member]} = conn
        |> patch(team_path(conn, :update, membership.team_id), params)
        |> json_response(200)

      assert data["type"] == "team"
      assert data["id"] == Integer.to_string(membership.team_id)
      assert data["attributes"]["name"] == "NotSuperTeam"
      assert data["attributes"]["active?"]
      assert data["attributes"]["inserted-at"]
      assert data["attributes"]["updated-at"]
      assert team_lead["attributes"]["team-id"] == membership.team_id
      assert member["attributes"]["team-id"] == membership.team_id
    end

    test "with invalid data", %{conn: conn, membership: membership} do
      params = Poison.encode!(%{data: %{type: "team", id: membership.team_id, attributes: %{"name" => ""}}})

      %{"errors" => [error]} = conn
        |> patch(team_path(conn, :update, membership.team_id), params)
        |> json_response(400)

      assert error["detail"] == "Name can't be blank"
    end
  end

  describe "getting teams a user belongs to" do
    setup [:create_non_admin, :log_in]

    test "with no membership", %{conn: conn, user: user} do
      %{"data" => data} = conn
        |> get(team_path(conn, :get_by_user, user.id))
        |> json_response(200)

      assert data == []
    end

    test "with membership", %{conn: conn, user: user} do
      created_team = OrganizationTest.fixture(:team)
      attrs = %{team_id: created_team.id, user_id: user.id, role: 1}
      {:ok, created_membership} = Organization.create_membership(attrs)

      %{"data" => [team], "included" => [membership]} = conn
        |> get(team_path(conn, :get_by_user, user.id))
        |> json_response(200)

      assert team["attributes"]["name"] == created_team.name
      assert membership["id"] == Integer.to_string(created_membership.id)
    end

    test "with sorting parameters", %{conn: conn, user: user} do
      [team1, team2, team3] = Fixtures.teams(3)

      Organization.create_membership(%{team_id: team1.id, user_id: user.id, role: 1})
      Organization.create_membership(%{team_id: team2.id, user_id: user.id, role: 1})
      Organization.create_membership(%{team_id: team3.id, user_id: user.id, role: 1})

      %{"data" => [result1, result2]} = conn
        |> get(team_path(conn, :get_by_user, user.id, count: 2))
        |> json_response(200)

      assert result1["attributes"]["name"] == team3.name
      assert result2["attributes"]["name"] == team2.name
    end
  end

  defp create_admin(%{conn: conn}) do
    user = AccountTest.fixture(:admin)
    {:ok, conn: conn, user: user}
  end

  def create_team_lead(%{conn: conn}) do
    team = OrganizationTest.fixture(:team)
    user = AccountTest.fixture(:user)
    {:ok, membership} = Organization.create_membership(%{team_id: team.id, user_id: user.id, role: 2})

    {:ok, conn: conn, user: user, team: team, membership: membership}
  end

  defp create_non_admin(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, user: user}
  end

  defp log_in(%{conn: conn, user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
