defmodule RasDirectoryApiWeb.CountryControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Fact

  describe "getting all countries as normal user" do
    setup [:create_non_admin, :log_in]

    test "should be similar to Fact.all_countries/0", %{conn: conn} do
      Fact.create_country(%{name: "Philippines"})
      Fact.create_country(%{name: "China"})
      Fact.create_country(%{name: "United States"})

      %{"data" => data} = conn
        |> get(country_path(conn, :index))
        |> json_response(200)

      assert Kernel.length(data) == 3
    end
  end

  defp create_non_admin(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, user: user}
  end

  defp log_in(%{conn: conn, user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
