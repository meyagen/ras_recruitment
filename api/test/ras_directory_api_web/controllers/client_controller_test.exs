defmodule RasDirectoryApiWeb.ClientControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.Account

  @client_data %{
    first_name: "Client",
    last_name: "XYZ",
    position: "President",
    mobile_number: "09061124975",
    primary_number: "09061124976",
    secondary_number: "09061124977",
    tertiary_number: "09061124978",
    email: "client@hello.com"
  }

  describe "creating a client" do
    setup [:create_user, :log_in]

    test "with valid and complete data", %{conn: conn, current_user: user} do
      company = Fixtures.company()

      attrs = @client_data
        |> Map.put(:company_id, company.id)
        |> Map.put(:country_id, company.country_id)

      params = Poison.encode!(%{data: %{type: "client", attributes: attrs}})

      %{"data" => data, "included" => [result_company, result_country, result_creator]} = conn
        |> post(client_path(conn, :create), params)
        |> json_response(200)

      assert String.to_integer(data["id"]) > 0
      assert data["attributes"]["first-name"] == attrs[:first_name]
      assert data["attributes"]["last-name"] == attrs[:last_name]
      assert data["attributes"]["email"] == attrs[:email]
      assert data["attributes"]["company-id"] == company.id
      assert data["attributes"]["country-id"] == company.country_id
      assert data["attributes"]["created-by"] == user.id

      assert result_company["id"] == Integer.to_string(company.id)
      assert result_country["id"] == Integer.to_string(company.country_id)
      assert result_creator["id"] == Integer.to_string(user.id)
    end

    test "with valid and incomplete data", %{conn: conn, current_user: current_user} do
      company = Fixtures.company()

      attrs = @client_data
        |> Map.delete(:position)
        |> Map.delete(:mobile_number)
        |> Map.delete(:primary_number)
        |> Map.delete(:secondary_number)
        |> Map.delete(:email)
        |> Map.put(:company_id, company.id)
        |> Map.put(:country_id, company.country_id)

      params = Poison.encode!(%{data: %{type: "client", attributes: attrs}})

      %{"data" => data, "included" => [result_company, result_country, result_creator]} = conn
        |> post(client_path(conn, :create), params)
        |> json_response(200)

      assert String.to_integer(data["id"]) > 0
      assert data["attributes"]["first-name"] == attrs[:first_name]
      assert data["attributes"]["last-name"] == attrs[:last_name]
      assert data["attributes"]["company-id"] == company.id
      assert data["attributes"]["country-id"] == company.country_id
      assert data["attributes"]["created-by"] == current_user.id

      assert result_company["id"] == Integer.to_string(company.id)
      assert result_country["id"] == Integer.to_string(company.country_id)
      assert result_creator["id"] == Integer.to_string(current_user.id)
    end
  end

  describe "updating a client" do
    setup [:create_user, :log_in]

    test "as the creator with all valid data", %{conn: conn, current_user: current_user} do
      client = Fixtures.client(%{created_by: current_user.id})
      new_company = Fixtures.company("Cuba Libre")

      new_client_attrs = %{
        first_name: "NewClient",
        last_name: "ABC",
        mobile_number: "123",
        primary_number: "1234",
        secondary_number: "12345",
        email: "newclient@hello.com",
        country_id: new_company.country_id,
        company_id: new_company.id
      }

      params = Poison.encode!(%{data: %{type: "client", attributes: new_client_attrs}})

      %{"data" => data, "included" => [result_company, result_country, result_creator]} = conn
        |> patch(client_path(conn, :update, client.id), params)
        |> json_response(200)

      assert String.to_integer(data["id"]) == client.id
      assert data["attributes"]["first-name"] == new_client_attrs[:first_name]
      assert data["attributes"]["last-name"] == new_client_attrs[:last_name]
      assert data["attributes"]["company-id"] == new_company.id
      assert data["attributes"]["country-id"] == new_company.country_id
      assert data["attributes"]["created-by"] == current_user.id

      assert result_company["id"] == Integer.to_string(new_company.id)
      assert result_country["id"] == Integer.to_string(new_company.country_id)
      assert result_creator["id"] == Integer.to_string(current_user.id)
    end
  end

  describe "getting a client" do
    setup [:create_user, :log_in]

    test "that is existing and owned by own user", %{conn: conn, current_user: current_user} do
      client = Fixtures.client(%{created_by: current_user.id})

      %{"data" => data, "included" => [company, country, creator]} = conn
        |> get(client_path(conn, :show, client.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(client.id)
      assert data["attributes"]["first-name"] == client.first_name
      assert data["attributes"]["last-name"] == client.last_name
      assert company["id"] == Integer.to_string(client.company_id)
      assert country["id"] == Integer.to_string(client.country_id)
      assert creator["id"] == Integer.to_string(client.created_by)
    end

    test "that is non-existent", %{conn: conn} do
      %{"errors" => [error]} = conn
        |> get(client_path(conn, :show, 1))
        |> json_response(400)

      assert error["id"] == "not_found"
      assert error["status"] == 404
    end

    test "that is not owned by this user", %{conn: conn} do
      client = Fixtures.client()

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :show, client.id))
        |> json_response(400)

      assert error["id"] == "unauthorized_user"
      assert error["status"] == 403
    end

    test "by a team lead of the creator", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      creator = Fixtures.user("creator")
      Fixtures.member(team, creator)
      client = Fixtures.client(%{created_by: creator.id})

      %{"data" => data, "included" => [company, country, creator]} = conn
        |> get(client_path(conn, :show, client.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(client.id)
      assert data["attributes"]["first-name"] == client.first_name
      assert data["attributes"]["last-name"] == client.last_name
      assert company["id"] == Integer.to_string(client.company_id)
      assert country["id"] == Integer.to_string(client.country_id)
      assert creator["id"] == Integer.to_string(client.created_by)
    end
  end

  describe "getting clients" do
    setup [:create_user, :log_in]

    test "all as an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      Fixtures.clients(120)

      %{"data" => result} = conn
        |> get(client_path(conn, :index))
        |> json_response(200)

      assert Kernel.length(result) == 120
    end

    test "all with pagination as an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      Fixtures.clients(120)

      %{"data" => result} = conn
        |> get(client_path(conn, :index, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["first-name"] == "Naruto70"
    end

    test "all as a normal user", %{conn: conn} do
      Fixtures.clients(120)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "owned as the creator", %{conn: conn, current_user: current_user} do
      Fixtures.clients(120, creator: current_user)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_creator, current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 120
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == current_user.id
    end

    test "owned with pagination as the creator", %{conn: conn, current_user: current_user} do
      Fixtures.clients(120, creator: current_user)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_creator, current_user.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == current_user.id
      assert first["attributes"]["first-name"] == "Naruto70"
    end

    test "owned within a company", %{conn: conn, current_user: current_user} do
      company = Fixtures.company("Owned Company")
      Fixtures.clients(120, creator: current_user, company: company)
      new_creator = Fixtures.user("SomeNewCreator")
      new_company = Fixtures.company(%{name: "NewCompany1", created_by: new_creator.id})
      Fixtures.clients(50, creator: new_creator, company: new_company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_company_and_creator, company.id, current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 120
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == current_user.id
      assert first["attributes"]["company-id"] == company.id
      assert first["attributes"]["first-name"] == "Naruto120"
    end

    test "owned within a company with pagination", %{conn: conn, current_user: current_user} do
      company = Fixtures.company("Owned Company")
      Fixtures.clients(120, creator: current_user, company: company)
      new_creator = Fixtures.user("SomeNewCreator")
      new_company = Fixtures.company(%{name: "NewCompany1", created_by: new_creator.id})
      Fixtures.clients(50, creator: new_creator, company: new_company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_company_and_creator, company.id, current_user.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == current_user.id
      assert first["attributes"]["company-id"] == company.id
      assert first["attributes"]["first-name"] == "Naruto70"
    end

    test "all within company as an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      old_creator = Fixtures.user("SomeOldCreator")
      old_company = Fixtures.company(%{name: "OldCompany2", created_by: old_creator.id})
      Fixtures.clients(120, creator: old_creator, company: old_company)
      new_creator = Fixtures.user("SomeNewCreator")
      new_company = Fixtures.company(%{name: "NewCompany1", created_by: new_creator.id})
      Fixtures.clients(50, creator: new_creator, company: new_company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_company, new_company.id))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == new_creator.id
      assert first["attributes"]["company-id"] == new_company.id
      assert first["attributes"]["first-name"] == "Naruto50"
    end

    test "all within company as a normal user", %{conn: conn} do
      old_creator = Fixtures.user("SomeOldCreator")
      old_company = Fixtures.company(%{name: "OldCompany2", created_by: old_creator.id})
      Fixtures.clients(120, creator: old_creator, company: old_company)
      new_creator = Fixtures.user("SomeNewCreator")
      new_company = Fixtures.company(%{name: "NewCompany1", created_by: new_creator.id})
      Fixtures.clients(50, creator: new_creator, company: new_company)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_company, new_company.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "all owned by a user as an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      old_creator = Fixtures.user("SomeOldCreator")
      old_company = Fixtures.company(%{name: "OldCompany2", created_by: old_creator.id})
      Fixtures.clients(120, creator: old_creator, company: old_company)
      new_creator = Fixtures.user("SomeNewCreator")
      new_company = Fixtures.company(%{name: "NewCompany1", created_by: new_creator.id})
      Fixtures.clients(50, creator: new_creator, company: new_company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_creator, old_creator.id))
        |> json_response(200)

      assert Kernel.length(result) == 120
      [first | _] = result
      assert first["type"] == "client"
      assert first["attributes"]["created-by"] == old_creator.id
      assert first["attributes"]["first-name"] == "Naruto120"
    end

    test "all owned by a user by another user", %{conn: conn} do
      old_creator = Fixtures.user("SomeOldCreator")
      old_company = Fixtures.company(%{name: "OldCompany2", created_by: old_creator.id})
      Fixtures.clients(120, creator: old_creator, company: old_company)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_creator, old_creator.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "all within a team as a team lead", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      company = Fixtures.company()

      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company)

      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(30, creator: user2, company: company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_team, team.id))
        |> json_response(200)

      assert Kernel.length(result) == 50
    end

    test "all within a team with pagination as a team lead", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      company = Fixtures.company()

      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company)

      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(80, creator: user2, company: company)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_team, team.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["first-name"] == "Naruto30"
    end

    test "all within a team as a normal member", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.member(team, current_user)

      company = Fixtures.company()

      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company)

      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(80, creator: user2, company: company)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_team, team.id, page: 2))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "all within a company by team as a team lead", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      company1 = Fixtures.company()
      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company1)

      company2 = Fixtures.company("Company2")
      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(40, creator: user2, company: company2)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_company_and_team, company1.id, team.id))
        |> json_response(200)

      assert Kernel.length(result) == 20
    end

    test "all within a company by team with pagination as a team lead", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      company1 = Fixtures.company()
      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company1)

      company2 = Fixtures.company("Company2")
      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(40, creator: user2, company: company2)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_company_and_team, company1.id, team.id, page: 2, count: 10))
        |> json_response(200)

      assert Kernel.length(result) == 10
    end

    test "all within a company by team as a normal member", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.member(team, current_user)

      company1 = Fixtures.company()
      user1 = Fixtures.user("member1")
      Fixtures.member(team, user1)
      Fixtures.clients(20, creator: user1, company: company1)

      company2 = Fixtures.company("Company2")
      user2 = Fixtures.user("member2")
      Fixtures.member(team, user2)
      Fixtures.clients(40, creator: user2, company: company2)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_company_and_team, company1.id, team.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
    end

    test "with sorting with last_note_at", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      clients = Fixtures.clients(50)

      clients
      |> Enum.at(30)
      |> Fixtures.notes(10)

      clients
      |> Enum.at(20)
      |> Fixtures.notes(20)

      %{"data" => result} = conn
        |> get(client_path(conn, :index, sort_by: "last_note_at", sort: "desc", count: 5))
        |> json_response(200)

      [first | _] = result
      assert Kernel.length(result) == 5
      assert first["attributes"]["first-name"] == "Naruto21"
      second = Enum.at(result, 1)
      assert second["attributes"]["first-name"] == "Naruto31"
    end
  end

  describe "getting clients managed by a team lead" do
    setup [:create_user, :log_in]

    test "as a team lead of a single team", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.clients(5, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.clients(5, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_team_lead, current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 5
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
    end

    test "as a team lead of a single team with pagination", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.clients(100, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.clients(10, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_team_lead, current_user.id, page: 2))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
      assert head["attributes"]["first-name"] == "Naruto50"
    end

    test "as a team lead with his own clients with pagination", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      Fixtures.clients(80, creator: current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.clients(10, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.clients(10, creator: creator_non_member)

      %{"data" => result} = conn
        |> get(client_path(conn, :index_by_team_lead, current_user.id, page: 1))
        |> json_response(200)

      assert Kernel.length(result) == 50
      [head | _] = result
      assert head["attributes"]["created-by"] == creator_member.id
      assert head["attributes"]["first-name"] == "Naruto10"
    end

    test "as a user without a team under management", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.member(team, current_user)
      Fixtures.clients(80, creator: current_user)

      creator_member = Fixtures.user("Creator1")
      Fixtures.member(team, creator_member)
      Fixtures.clients(10, creator: creator_member)

      creator_non_member = Fixtures.user("Creator2")
      Fixtures.clients(10, creator: creator_non_member)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_team_lead, current_user.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead inspecting other team leads", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      Fixtures.clients(80, creator: current_user)

      team2 = Fixtures.team("NewCompany")
      creator_member = Fixtures.user("Creator1")
      Fixtures.team_lead(team2, creator_member)
      Fixtures.clients(10, creator: creator_member)

      %{"errors" => [error]} = conn
        |> get(client_path(conn, :index_by_team_lead, creator_member.id))
        |> json_response(400)

      assert error["title"] == "User is unauthorized"
      assert error["status"] == 403
    end

    test "as a team lead with sorting parameters", %{conn: conn, current_user: current_user} do
      team = Fixtures.team()
      Fixtures.team_lead(team, current_user)
      creator = Fixtures.user("Creator")
      Fixtures.member(team, creator)
      Fixtures.client(first_name: "AA", creator: creator)
      Fixtures.client(first_name: "BB", creator: creator)
      some_member = Fixtures.user("SomeMember")
      Fixtures.member(team, some_member)
      Fixtures.client(first_name: "CC", creator: some_member)

      %{"data" => [result1, result2]} = conn
        |> get(client_path(conn, :index_by_team_lead, current_user.id, created_by: creator.id))
        |> json_response(200)

      assert result1["attributes"]["first-name"] == "BB"
      assert result2["attributes"]["first-name"] == "AA"
    end
  end

  describe "filtering clients as an admin" do
    setup [:create_user, :log_in]

    test "by last_name", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.client([last_name: "AB"])
      Fixtures.client([last_name: "NA"])
      Fixtures.client([last_name: "nB"])
      Fixtures.client([last_name: "AC"])
      Fixtures.client([last_name: "CD"])
      Fixtures.client([last_name: "DDE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, last_name: "N"))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [client1, client2] = result
      assert client1["attributes"]["last-name"] == "nB"
      assert client2["attributes"]["last-name"] == "NA"
    end

    test "by first_name with pagination", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.client([first_name: "AB"])
      Fixtures.client([first_name: "NA"])
      Fixtures.client([first_name: "nB"])
      Fixtures.client([first_name: "aC"])
      Fixtures.client([first_name: "CD"])
      Fixtures.client([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, first_name: "A", page: 2, count: 2))
        |> json_response(200)

      assert Kernel.length(result) == 1
      [client1] = result
      assert client1["attributes"]["first-name"] == "AB"
    end

    test "by position", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.client([first_name: "AB", position: "CEO"])
      Fixtures.client([first_name: "NA", position: "Chief Executive Officer"])
      Fixtures.client([first_name: "nB"])
      Fixtures.client([first_name: "aC"])
      Fixtures.client([first_name: "CD"])
      Fixtures.client([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, position: "C"))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [client1, client2] = result
      assert client1["attributes"]["first-name"] == "NA"
      assert client2["attributes"]["first-name"] == "AB"
    end

    test "by country", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      country = Fixtures.country("El Salvador")
      Fixtures.client([first_name: "AB", country: country])
      Fixtures.client([first_name: "NA", country: country])
      Fixtures.client([first_name: "nB"])
      Fixtures.client([first_name: "aC"])
      Fixtures.client([first_name: "CD"])
      Fixtures.client([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, country_id: country.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [client1, client2] = result
      assert client1["attributes"]["first-name"] == "NA"
      assert client2["attributes"]["first-name"] == "AB"
    end

    test "by creator", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      Fixtures.client([first_name: "AB", creator: current_user])
      Fixtures.client([first_name: "NA", creator: current_user])
      Fixtures.client([first_name: "nB"])
      Fixtures.client([first_name: "aC"])
      Fixtures.client([first_name: "CD"])
      Fixtures.client([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, created_by: current_user.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [client1, client2] = result
      assert client1["attributes"]["first-name"] == "NA"
      assert client2["attributes"]["first-name"] == "AB"
    end

    test "by company", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)

      target_company = Fixtures.company("Upwork Inc. Ltd.")
      target_skill = Fixtures.skill("Elixir")
      Fixtures.client([first_name: "AB", skills: [target_skill], company: target_company])
      Fixtures.client([first_name: "NA", skills: [Fixtures.skill("PHP"), target_skill], company: target_company])
      Fixtures.client([first_name: "nB"])
      Fixtures.client([first_name: "aC"])
      Fixtures.client([first_name: "CD"])
      Fixtures.client([first_name: "ADE"])

      %{"data" => result} = conn
        |> get(client_path(conn, :index, company_id: target_company.id))
        |> json_response(200)

      assert Kernel.length(result) == 2
      [client1, client2] = result
      assert client1["attributes"]["first-name"] == "NA"
      assert client2["attributes"]["first-name"] == "AB"
    end
  end

  describe "deleting a client" do
    setup [:create_user, :log_in]

    test "that is existing and owned by this user", %{conn: conn, current_user: user} do
      client = Fixtures.client(%{created_by: user.id})

      %{"data" => data} = conn
        |> delete(client_path(conn, :delete, client.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(client.id)
    end

    test "by an admin", %{conn: conn, current_user: current_user} do
      Account.update_user_role(current_user.id, 2)
      client = Fixtures.client()

      %{"data" => data} = conn
        |> delete(client_path(conn, :delete, client.id))
        |> json_response(200)

      assert data["id"] == Integer.to_string(client.id)
    end
  end

  defp create_user(%{conn: conn}) do
    user = Fixtures.user("CurrentUser")
    {:ok, conn: conn, current_user: user}
  end

  defp log_in(%{conn: conn, current_user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
      |> Plug.Conn.put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
