defmodule RasDirectoryApiWeb.SkillControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Recruitment

  @skill_data %{
    name: "Leadership",
  }

  describe "creating a skill as a normal user" do
    setup [:create_non_admin, :log_in]

    test "with valid data", %{conn: conn} do
      params = Poison.encode!(%{data: %{type: "skill", attributes: @skill_data}})

      %{"data" => data} = conn
        |> post(skill_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["name"] == "leadership"
    end

    test "with invalid data", %{conn: conn} do
      params = Poison.encode!(%{data: %{type: "skill", attributes: %{}}})

      %{"errors" => [error]} = conn
        |> post(skill_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name can't be blank"
    end

    test "with duplicate names", %{conn: conn} do
      Recruitment.create_skill(%{name: "leadership"})

      params = Poison.encode!(%{data: %{type: "skill", attributes: @skill_data}})

      %{"errors" => [error]} = conn
        |> post(skill_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name has already been taken"
    end
  end

  describe "getting all skills as normal user" do
    setup [:create_non_admin, :log_in]

    test "should be similar to Recruitment.all_skills/0", %{conn: conn} do
      Recruitment.create_skill(%{name: "Translations"})
      Recruitment.create_skill(%{name: "Photography"})
      Recruitment.create_skill(%{name: "Cooking"})

      %{"data" => data} = conn
        |> get(skill_path(conn, :index))
        |> json_response(200)

      assert Kernel.length(data) == 3
    end
  end

  defp create_non_admin(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, user: user}
  end

  defp log_in(%{conn: conn, user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
