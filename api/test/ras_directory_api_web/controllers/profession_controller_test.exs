defmodule RasDirectoryApiWeb.ProfessionControllerTest do
  use RasDirectoryApiWeb.ConnCase

  alias RasDirectoryApi.AccountTest
  alias RasDirectoryApi.Recruitment

  @profession_data %{
    name: "Chief Architect",
  }

  describe "creating a profession as a normal user" do
    setup [:create_non_admin, :log_in]

    test "with valid data", %{conn: conn} do
      params = Poison.encode!(%{data: %{type: "profession", attributes: @profession_data}})

      %{"data" => data} = conn
        |> post(profession_path(conn, :create), params)
        |> json_response(200)

      assert data["id"] > 0
      assert data["attributes"]["name"] == "chief architect"
    end

    test "with invalid data", %{conn: conn} do
      params = Poison.encode!(%{data: %{type: "profession", attributes: %{}}})

      %{"errors" => [error]} = conn
        |> post(profession_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name can't be blank"
    end

    test "with duplicate names", %{conn: conn} do
      Recruitment.create_profession(%{name: "chief architect"})

      params = Poison.encode!(%{data: %{type: "profession", attributes: @profession_data}})

      %{"errors" => [error]} = conn
        |> post(profession_path(conn, :create), params)
        |> json_response(400)

      assert error["detail"] == "Name has already been taken"
    end
  end

  describe "getting all professions as normal user" do
    setup [:create_non_admin, :log_in]

    test "should be similar to Recruitment.all_professions/0", %{conn: conn} do
      Recruitment.create_profession(%{name: "CEO"})
      Recruitment.create_profession(%{name: "VP for Engineering"})
      Recruitment.create_profession(%{name: "Accountant"})

      %{"data" => data} = conn
        |> get(profession_path(conn, :index))
        |> json_response(200)

      assert Kernel.length(data) == 3
    end
  end

  defp create_non_admin(%{conn: conn}) do
    user = AccountTest.fixture(:user)
    {:ok, conn: conn, user: user}
  end

  defp log_in(%{conn: conn, user: user}) do
    {:ok, jwt, claims} = Guardian.encode_and_sign(user)
    expiration = Map.fetch!(claims, "exp")

    conn = conn
      |> put_req_header("authorization", "Bearer #{jwt}")
      |> put_req_header("x-expires", Integer.to_string(expiration))

    {:ok, conn: conn}
  end
end
