defmodule RasDirectoryApiWeb.ErrorViewTest do
  use RasDirectoryApiWeb.ConnCase, async: true

  import Phoenix.View

  test "renders 404.json-api" do
    assert render(RasDirectoryApiWeb.ErrorView, "404.json-api", []) ==
           %{errors: %{detail: "Page not found"}}
  end

  test "render 500.json-api" do
    assert render(RasDirectoryApiWeb.ErrorView, "500.json-api", []) ==
           %{errors: %{detail: "Internal server error"}}
  end

  test "render any other" do
    assert render(RasDirectoryApiWeb.ErrorView, "505.json-api", []) ==
           %{errors: %{detail: "Internal server error"}}
  end
end
