defmodule RasDirectoryApiWeb.CompanyView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :name,
    :country_id,
    :phone_number,
    :email,
    :created_by,
    :inserted_at,
    :updated_at
  ]

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true

  has_one :country,
    serializer: RasDirectoryApiWeb.CountryView,
    include: true
end
