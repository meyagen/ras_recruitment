defmodule RasDirectoryApiWeb.SkillView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end
