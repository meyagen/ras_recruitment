defmodule RasDirectoryApiWeb.ApplicantMailView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :applicant_id,
    :subject,
    :content,
    :created_by,
    :inserted_at,
    :updated_at
  ]

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true
end
