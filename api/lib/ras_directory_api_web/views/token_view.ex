defmodule RasDirectoryApiWeb.TokenView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:expiration]
end
