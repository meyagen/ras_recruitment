defmodule RasDirectoryApiWeb.TeamView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:name, :active?, :inserted_at, :updated_at]

  has_many :memberships,
    serializer: RasDirectoryApiWeb.MembershipView,
    include: true
end
