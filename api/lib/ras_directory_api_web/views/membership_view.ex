defmodule RasDirectoryApiWeb.MembershipView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  alias RasDirectoryApi.Organization

  attributes [:team_id, :user_id, :role]

  def role(membership, _conn) do
    Organization.role_to_name(membership.role)
  end
end
