defmodule RasDirectoryApiWeb.ApplicantNoteView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :applicant_id,
    :content,
    :created_by,
    :inserted_at,
    :updated_at
  ]

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true
end
