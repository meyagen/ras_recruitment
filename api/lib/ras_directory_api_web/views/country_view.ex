defmodule RasDirectoryApiWeb.CountryView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end
