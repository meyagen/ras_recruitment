defmodule RasDirectoryApiWeb.UserView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Auth

  attributes [:username,
    :email,
    :first_name,
    :last_name,
    :role,
    :status,
    :position
  ]

  def role(user, _conn) do
    Auth.role_name(user.role)
  end

  def status(user, __conn) do
    Account.status(user.status)
  end
end
