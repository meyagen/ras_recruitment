defmodule RasDirectoryApiWeb.ClientMailView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :client_id,
    :subject,
    :content,
    :created_by,
    :inserted_at,
    :updated_at
  ]

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true
end
