defmodule RasDirectoryApiWeb.ChangesetView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView
  alias Ecto.Changeset

  def translate_errors(changeset) do
    Changeset.traverse_errors(changeset, &translate_error/1)
  end

  def render("error.json-api", %{changeset: changeset}) do
    %{errors: translate_errors(changeset)}
  end
end
