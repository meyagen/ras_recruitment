defmodule RasDirectoryApiWeb.ClientView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :first_name,
    :last_name,
    :position,
    :mobile_number,
    :primary_number,
    :secondary_number,
    :tertiary_number,
    :email,
    :created_by,
    :country_id,
    :company_id,
    :last_note_at,
    :inserted_at,
    :updated_at,
  ]

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true

  has_one :country,
    serializer: RasDirectoryApiWeb.CountryView,
    include: true

  has_one :company,
    serializer: RasDirectoryApiWeb.CompanyOnlyView,
    include: true
end
