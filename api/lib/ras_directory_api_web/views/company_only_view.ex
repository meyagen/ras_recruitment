defmodule RasDirectoryApiWeb.CompanyOnlyView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [
    :name,
    :country_id,
    :phone_number,
    :email,
    :created_by,
    :inserted_at,
    :updated_at
  ]
end
