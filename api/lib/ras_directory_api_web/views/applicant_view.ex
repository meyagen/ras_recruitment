defmodule RasDirectoryApiWeb.ApplicantView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  alias RasDirectoryApi.Recruitment

  attributes [
    :first_name,
    :last_name,
    :landline_number,
    :mobile_number,
    :phone_number,
    :other_number,
    :email,
    :created_by,
    :country_id,
    :profession_id,
    :company_id,
    :status,
    :last_note_at,
    :inserted_at,
    :updated_at,
  ]

  def status(applicant, __conn) do
    Recruitment.applicant_status(applicant.status)
  end

  has_one :creator,
    serializer: RasDirectoryApiWeb.UserView,
    include: true

  has_one :country,
    serializer: RasDirectoryApiWeb.CountryView,
    include: true

  has_one :profession,
    serializer: RasDirectoryApiWeb.ProfessionView,
    include: true

  has_one :company,
    serializer: RasDirectoryApiWeb.CompanyOnlyView,
    include: true

  has_many :skills,
    serializer: RasDirectoryApiWeb.SkillView,
    include: true
end
