defmodule RasDirectoryApiWeb.ProfessionView do
  use RasDirectoryApiWeb, :view
  use JaSerializer.PhoenixView

  attributes [:name]
end
