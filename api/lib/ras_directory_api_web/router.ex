defmodule RasDirectoryApiWeb.Router do
  use RasDirectoryApiWeb, :router

  pipeline :api do
    plug :accepts, ["json-api"]
    plug JaSerializer.ContentTypeNegotiation
    plug JaSerializer.Deserializer
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
    plug RasDirectoryApi.Auth.CurrentUser
  end

  pipeline :authenticated do
    plug Guardian.Plug.EnsureAuthenticated, handler: RasDirectoryApiWeb.ErrorController
  end

  scope "/v1", RasDirectoryApiWeb do
    pipe_through :api

    resources "/tokens", TokenController, only: [:create]
  end

  scope "/v1", RasDirectoryApiWeb do
    pipe_through [:api, :authenticated]

    get "/users/username/:username", UserController, :get_by_username
    patch "/users/:id/activate", UserController, :activate
    resources "/users", UserController, except: [:new, :edit]

    get "/teams/user/:user_id", TeamController, :get_by_user
    resources "/teams", TeamController, only: [:create, :show, :update, :index] do
      get "/memberships/user/:user_id", MembershipController, :get_by_user
      post "/memberships", MembershipController, :create
      post "/memberships/team_lead", MembershipController, :assign_team_lead
      delete "/memberships/user/:user_id", MembershipController, :delete
    end

    resources "/applicants", ApplicantController, only: [:create, :show, :update, :index, :delete] do
      resources "/notes", ApplicantNoteController, only: [:create, :index, :delete]
      resources "/mails", ApplicantMailController, only: [:create, :index]
    end
    get "/applicants/company/:company_id/user/:user_id", ApplicantController, :index_by_company_and_creator
    get "/applicants/company/:company_id/team/:team_id", ApplicantController, :index_by_company_and_team
    get "/applicants/company/:company_id", ApplicantController, :index_by_company
    get "/applicants/user/:user_id", ApplicantController, :index_by_creator
    get "/applicants/team/:team_id", ApplicantController, :index_by_team
    get "/applicants/team_lead/:team_lead_id", ApplicantController, :index_by_team_lead

    resources "/companies", CompanyController, only: [:create, :show, :update, :index]
    resources "/countries", CountryController, only: [:index]
    resources "/professions", ProfessionController, only: [:create, :index]
    resources "/skills", SkillController, only: [:create, :index]

    resources "/clients", ClientController, only: [:index, :create, :show, :update, :delete] do
      resources "/notes", ClientNoteController, only: [:create, :index, :delete]
      resources "/mails", ClientMailController, only: [:create, :index]
    end

    get "/clients/user/:user_id", ClientController, :index_by_creator
    get "/clients/company/:company_id", ClientController, :index_by_company
    get "/clients/company/:company_id/user/:user_id", ClientController, :index_by_company_and_creator
    get "/clients/company/:company_id/team/:team_id", ClientController, :index_by_company_and_team
    get "/clients/team/:team_id", ClientController, :index_by_team
    get "/clients/team_lead/:team_lead_id", ClientController, :index_by_team_lead
  end
end
