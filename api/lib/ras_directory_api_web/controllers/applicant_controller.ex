defmodule RasDirectoryApiWeb.ApplicantController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Auth
  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.Recruitment.Applicant

  plug :authorize_resource,
    model: Applicant,
    non_id_actions: [
      :index_by_creator,
      :index_by_company,
      :index_by_company_and_creator,
      :index_by_company_and_team,
      :index_by_team,
      :index_by_team_lead
    ]

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, params) do
    {:ok, params} = params |> atomorphiform
    applicants = Recruitment.all_applicants(params)
    conn |> render("index.json-api", data: applicants)
  end

  def index_by_creator(conn, params) do
    {:ok, params} = params |> atomorphiform

    user_id = String.to_integer(params[:user_id])
    current_user = current_resource(conn)
    case Auth.authorized?(current_user, :index_by_creator, creator_id: user_id) do
      true ->
        applicants = Recruitment.get_applicants_by_creator(user_id, params)
        conn |> render("index.json-api", data: applicants)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_company(conn, %{"company_id" => company_id} = params) do
    {:ok, params} = params |> atomorphiform
    applicants = Recruitment.get_applicants_by_company(String.to_integer(company_id), params)
    conn |> render("index.json-api", data: applicants)
  end

  def index_by_company_and_creator(conn, %{"company_id" => company_id, "user_id" => user_id} = params) do
    {:ok, params} = params |> atomorphiform
    company_id = String.to_integer(company_id)
    user_id = String.to_integer(user_id)
    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_company_and_creator, creator_id: user_id) do
      true ->
        applicants = Recruitment.get_applicants_by_company_and_creator(company_id, user_id, params)
        conn |> render("index.json-api", data: applicants)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_company_and_team(conn, %{"company_id" => company_id, "team_id" => team_id} = params) do
    {:ok, params} = params |> atomorphiform
    company_id = String.to_integer(company_id)
    team_id = String.to_integer(team_id)
    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_company_and_team, team_id: team_id) do
      true ->
        applicants = Recruitment.get_applicants_by_company_and_team(company_id, team_id, params)
        conn |> render("index.json-api", data: applicants)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_team(conn, %{"team_id" => team_id} = params) do
    {:ok, params} = params |> atomorphiform
    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_team, team_id: team_id) do
      true ->
        applicants = Recruitment.get_applicants_by_team(team_id, params)
        conn |> render("index.json-api", data: applicants)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_team_lead(conn, %{"team_lead_id" => team_lead_id} = params) do
    {:ok, params} = params |> atomorphiform()
    team_lead_id = team_lead_id |> String.to_integer()
    current_user = conn |> current_resource()

    case Auth.authorized?(current_user, :index_by_team_lead, team_lead_id: team_lead_id) do
      true ->
        applicants = team_lead_id |> Recruitment.get_applicants_under_team_lead(params)
        conn |> render("index.json-api", data: applicants)
      false ->
        {:error, :unauthorized}
    end
  end

  def show(conn, %{"id" => id}) do
    case Recruitment.get_applicant(id) do
      %Applicant{} = applicant ->
        conn |> render("show.json-api", data: applicant)
      nil ->
        {:error, :not_found}
    end
  end

  def create(conn, %{"data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> Morphix.atomorphiform

     attrs = attrs
      |> Map.put(:created_by, current_user.id)
      |> Map.put(:status, Recruitment.applicant_status(attrs[:status]))

    with {:ok, applicant} <- Recruitment.create_applicant(attrs) do
      conn |> render("show.json-api", data: applicant)
    end
  end

  def update(conn, %{"id" => applicant_id, "data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    attrs = attrs |> Map.put(:status, Recruitment.applicant_status(attrs[:status]))

    with {:ok, applicant} <- Recruitment.update_applicant(applicant_id, attrs) do
      conn |> render("show.json-api", data: applicant)
    end
  end

  def delete(conn, %{"id" => applicant_id}) do
    with {:ok, applicant} <- Recruitment.delete_applicant(applicant_id) do
      conn |> render("show.json-api", data: applicant)
    end
  end
end
