defmodule RasDirectoryApiWeb.TokenController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Auth

  def create(conn, %{"data" => data}) do
    attrs = to_attributes(data)
    case Auth.sign_in(attrs["username"], attrs["password"]) do
      {:ok, user} ->
        new_conn = api_sign_in(conn, user)
        jwt = current_token(new_conn)
        {:ok, claims} = claims(new_conn)
        expiration = Map.get(claims, "exp")

        new_conn
        |> put_resp_header("authorization", "Bearer #{jwt}")
        |> put_resp_header("x-expires", Integer.to_string(expiration))
        |> render("show.json-api", data: %{type: "token", id: jwt, expiration: expiration})
      {:error, message} ->
        error = %{
          id: "invalid_credentials",
          status: 422,
          title: "Invalid Credentials",
          detail: message
        }

        conn
        |> put_status(:bad_request)
        |> render(:errors, data: error)
    end
  end
end
