defmodule RasDirectoryApiWeb.ClientMailController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Messaging
  alias RasDirectoryApi.Messaging.ClientMail

  plug :authorize_resource, model: ClientMail

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, %{"client_id" => client_id} = params) do
    {:ok, params} = params
      |> atomorphiform

    mails = client_id
      |> String.to_integer()
      |> Messaging.client_mails(params)

    render(conn, "index.json-api", data: mails)
  end

  def create(conn, %{"client_id" => client_id, "data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    attrs = attrs
      |> Map.put(:created_by, current_user.id)
      |> Map.put(:client_id, String.to_integer(client_id))

    with {:ok, mail} <- Messaging.send_and_record_mail(attrs) do
      conn
      |> render("show.json-api", data: mail)
    end
  end
end
