defmodule RasDirectoryApiWeb.ApplicantNoteController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Messaging
  alias RasDirectoryApi.Messaging.ApplicantNote

  plug :authorize_resource, model: ApplicantNote

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, %{"applicant_id" => applicant_id} = params) do
    {:ok, params} = params
      |> atomorphiform

    notes = applicant_id
      |> String.to_integer()
      |> Messaging.applicant_notes(params |> Map.delete(:applicant_id))

    render(conn, "index.json-api", data: notes)
  end

  def create(conn, %{"applicant_id" => applicant_id, "data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    attrs = attrs
      |> Map.put(:created_by, current_user.id)
      |> Map.put(:applicant_id, String.to_integer(applicant_id))

    with {:ok, note} <- Messaging.create_note(attrs) do
      conn
      |> render("show.json-api", data: note)
    end
  end

  def delete(conn, %{"id" => note_id}) do
    note_id = String.to_integer(note_id)
    with {:ok, note} <- Messaging.delete_applicant_note(note_id) do
      conn |> render("show.json-api", data: note)
    end
  end
end
