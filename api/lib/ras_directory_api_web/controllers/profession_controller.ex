defmodule RasDirectoryApiWeb.ProfessionController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Recruitment

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, _) do
    professions = Recruitment.all_professions()
    conn |> render("index.json-api", data: professions)
  end

  def create(conn, %{"data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    with {:ok, profession} <- Recruitment.create_profession(attrs) do
      conn |> render("show.json-api", data: profession)
    end
  end

end
