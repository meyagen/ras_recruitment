defmodule RasDirectoryApiWeb.SkillController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Recruitment

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, _) do
    skills = Recruitment.all_skills()
    conn |> render("index.json-api", data: skills)
  end

  def create(conn, %{"data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    with {:ok, skill} <- Recruitment.create_skill(attrs) do
      conn |> render("show.json-api", data: skill)
    end
  end
end
