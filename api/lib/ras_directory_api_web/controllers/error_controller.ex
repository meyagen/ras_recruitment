defmodule RasDirectoryApiWeb.ErrorController do
  use RasDirectoryApiWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:bad_request)
    |> render(:errors, data: changeset)
  end

  def call(conn, {:error, :not_found}) do
    error = %{
      id: "not_found",
      status: 404,
      title: "Resource not found",
      detail: "Resource cannot be found"
    }

    conn
    |> put_status(:bad_request)
    |> render(:errors, data: error)
  end

  def call(conn, {:error, :already_banned}) do
    error = %{
      id: "invalid_user_ban",
      status: 410,
      title: "Invalid user to ban",
      detail: "User is no longer active"
    }

    conn
    |> put_status(:bad_request)
    |> render(:errors, data: error)
  end

  def call(conn, {:error, :unauthorized}) do
    unauthorized(conn)
  end

  def unauthenticated(conn, _params) do
    error = %{
      id: "unauthenticated_user",
      status: 401,
      title: "User is unauthenticated",
      detail: "Cannot access this resource because user is not logged in"
    }

    conn
    |> put_status(:bad_request)
    |> put_view(RasDirectoryApiWeb.ErrorView)
    |> render(:errors, data: error)
  end

  def unauthorized(conn) do
    error = %{
      id: "unauthorized_user",
      status: 403,
      title: "User is unauthorized",
      detail: "User is not allowed to access this resource"
    }

    conn
    |> put_status(:bad_request)
    |> render(:errors, data: error)
    |> halt
  end
end
