defmodule RasDirectoryApiWeb.CompanyController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.Recruitment.Company

  plug :authorize_resource, model: Company
  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, params) do
    {:ok, params} = params |> atomorphiform
    companies = Recruitment.all_companies(params)
    conn |> render("index.json-api", data: companies)
  end

  def show(conn, %{"id" => company_id}) do
    case Recruitment.get_company(company_id) do
      %Company{} = company ->
        conn
        |> render("show.json-api", data: company)
      nil ->
        {:error, :not_found}
    end
  end

  def create(conn, %{"data" => data}) do
    current_user = current_resource(conn)

    attrs = data
      |> to_attributes
      |> Map.put("created_by", current_user.id)

    with {:ok, company} <- Recruitment.create_company(attrs) do
      conn |> render("show.json-api", data: company)
    end
  end

  def update(conn, %{"id" => company_id, "data" => data}) do
    attrs = data
      |> to_attributes

    with {:ok, company} <- Recruitment.update_company(company_id, attrs) do
      conn |> render("show.json-api", data: company)
    end
  end
end
