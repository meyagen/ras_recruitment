defmodule RasDirectoryApiWeb.UserController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Auth

  plug :authorize_resource, model: User, except: [:show, :get_by_username]
  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, _) do
    users = Account.get_all_users()
    conn |> render("index.json-api", data: users)
  end

  def show(conn, %{"id" => id}) do
    case Account.get_user(id) do
      %User{} = user ->
        conn |> render("show.json-api", data: user)
      nil ->
        {:error, :not_found}
    end
  end

  def get_by_username(conn, %{"username" => username}) do
    case Account.get_by_username(username) do
      %User{} = user ->
        conn |> render("show.json-api", data: user)
      nil ->
        {:error, :not_found}
    end
  end

  def create(conn, %{"data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> Morphix.atomorphiform

    attrs = attrs
      |> Map.put(:role, Auth.role_from_name(attrs[:role]))

    with {:ok, user} <- Account.create_user(attrs) do
      conn |> render("show.json-api", data: user)
    end
  end

  def update(conn, %{"id" => id, "data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    case Account.get_user(id) do
      %User{} = user ->
        with {:ok, user} <- Account.update_user(user, attrs) do
          conn |> render("show.json-api", data: user)
        end
      nil ->
        {:error, :not_found}
    end
  end

  def activate(conn, %{"id" => id}) do
    with {:ok, user} <- Account.update_status(id, Account.to_status("active")) do
      conn |> render("show.json-api", data: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    case Account.get_user(id) do
      %User{} = user ->
        with {:ok, user} <- Account.ban_user(user) do
          conn |> render("show.json-api", data: user)
        end
      nil ->
        {:error, :not_found}
    end
  end
end
