defmodule RasDirectoryApiWeb.ApplicantMailController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Messaging
  alias RasDirectoryApi.Messaging.ApplicantMail

  plug :authorize_resource, model: ApplicantMail

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, %{"applicant_id" => applicant_id} = params) do
    {:ok, params} = params
      |> atomorphiform

    mails = applicant_id
      |> String.to_integer()
      |> Messaging.applicant_mails(params)

    render(conn, "index.json-api", data: mails)
  end

  def create(conn, %{"applicant_id" => applicant_id, "data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    attrs = attrs
      |> Map.put(:created_by, current_user.id)
      |> Map.put(:applicant_id, String.to_integer(applicant_id))

    with {:ok, mail} <- Messaging.send_and_record_mail(attrs) do
      conn
      |> render("show.json-api", data: mail)
    end
  end
end
