defmodule RasDirectoryApiWeb.CountryController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Fact

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, _) do
    countries = Fact.all_countries()
    conn |> render("index.json-api", data: countries)
  end
end
