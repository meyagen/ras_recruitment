defmodule RasDirectoryApiWeb.MembershipController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Organization.Membership
  alias RasDirectoryApi.Organization.Team

  plug :authorize_resource, model: Team, id_name: "team_id", persisted: true
  action_fallback RasDirectoryApiWeb.ErrorController

  def get_by_user(conn, %{"team_id" => team_id, "user_id" => user_id}) do
    case Organization.get_membership(team_id, user_id) do
      %Membership{} = membership ->
        conn |> render("show.json-api", data: membership)
      nil ->
        {:error, :not_found}
    end
  end

  def create(conn, %{"team_id" => team_id, "data" => %{"attributes" => %{"user_id" => user_id}}}) do
    attrs = %{
      team_id: team_id,
      user_id: user_id,
      role: Organization.role_from_name("member")
    }

    with {:ok, membership} <- Organization.create_membership(attrs) do
      conn |> render("show.json-api", data: membership)
    end
  end

  def assign_team_lead(conn, %{"team_id" => team_id, "data" => %{"attributes" => %{"user_id" => user_id}}}) do
    attrs = %{
      team_id: team_id,
      user_id: user_id,
      role: Organization.role_from_name("team_lead")
    }

    case Organization.get_membership(team_id, attrs[:user_id]) do
      %Membership{} = old_membership ->
        with {:ok, membership} <- Organization.update_membership_role(old_membership, attrs[:role]) do
          conn |> render("show.json-api", data: membership)
        end
      nil ->
        with {:ok, membership} <- Organization.create_membership(attrs) do
          conn |> render("show.json-api", data: membership)
        end
    end
  end

  def delete(conn, %{"team_id" => team_id, "user_id" => user_id}) do
    with {:ok, membership} <- Organization.remove_user_from_team(user_id, team_id) do
      conn |> render("show.json-api", data: membership)
    end
  end
end
