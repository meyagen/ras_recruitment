defmodule RasDirectoryApiWeb.ClientNoteController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Messaging
  alias RasDirectoryApi.Messaging.ClientNote

  plug :authorize_resource, model: ClientNote

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, %{"client_id" => client_id} = params) do
    {:ok, params} = params
      |> atomorphiform

    notes = client_id
      |> String.to_integer()
      |> Messaging.client_notes(params |> Map.delete(:client_id))

    render(conn, "index.json-api", data: notes)
  end

  def create(conn, %{"client_id" => client_id, "data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    attrs = attrs
      |> Map.put(:created_by, current_user.id)
      |> Map.put(:client_id, String.to_integer(client_id))

    with {:ok, note} <- Messaging.create_note(attrs) do
      conn
      |> render("show.json-api", data: note)
    end
  end

  def delete(conn, %{"id" => note_id}) do
    note_id = String.to_integer(note_id)
    with {:ok, note} <- Messaging.delete_client_note(note_id) do
      conn |> render("show.json-api", data: note)
    end
  end
end
