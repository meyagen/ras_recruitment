defmodule RasDirectoryApiWeb.TeamController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Organization.Team

  plug :authorize_resource, model: Team, except: [:get_by_user]
  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, params) do
    {:ok, params} = params |> atomorphiform
    teams = Organization.all_teams(params)
    conn |> render("index.json-api", data: teams)
  end

  def show(conn, %{"id" => team_id}) do
    case Organization.get_team(team_id) do
      %Team{} = team ->
        conn |> render("show.json-api", data: team)
      nil ->
        {:error, :not_found}
    end
  end

  def get_by_user(conn, params) do
    {:ok, params} = params |> atomorphiform

    user_id = String.to_integer(params[:user_id])
    query_params = params |> Map.delete(:user_id)

    teams = Organization.get_user_teams(user_id, query_params)
    conn |> render("index.json-api", data: teams)
  end

  def create(conn, %{"data" => data}) do
    attrs = data |> to_attributes
    with {:ok, team} <- Organization.create_team(attrs) do
      conn |> render("show.json-api", data: team)
    end
  end

  def update(conn, %{"id" => team_id, "data" => data}) do
    attrs = data |> to_attributes
    with {:ok, team} <- Organization.update_team(team_id, attrs) do
      conn |> render("show.json-api", data: team)
    end
  end
end
