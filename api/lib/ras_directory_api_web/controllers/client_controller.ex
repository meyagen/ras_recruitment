defmodule RasDirectoryApiWeb.ClientController do
  use RasDirectoryApiWeb, :controller

  alias RasDirectoryApi.Auth
  alias RasDirectoryApi.Outsourcing
  alias RasDirectoryApi.Outsourcing.Client

  plug :authorize_resource,
    model: Client,
    non_id_actions: [
      :index_by_creator,
      :index_by_company,
      :index_by_company_and_creator,
      :index_by_company_and_team,
      :index_by_team,
      :index_by_team_lead
    ]

  action_fallback RasDirectoryApiWeb.ErrorController

  def index(conn, params) do
    {:ok, params} = params |> atomorphiform

    clients = Outsourcing.all_clients(params)

    conn |> render("index.json-api", data: clients)
  end

  def index_by_creator(conn, params) do
    {:ok, params} = params |> atomorphiform
    user_id = String.to_integer(params[:user_id])

    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_creator, creator_id: user_id) do
      true ->
        clients = Outsourcing.clients_by_creator(user_id, params)
        conn |> render("index.json-api", data: clients)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_company(conn, params) do
    {:ok, params} = params |> atomorphiform
    company_id = String.to_integer(params[:company_id])
    clients = Outsourcing.clients_by_company(company_id, params)
    conn |> render("index.json-api", data: clients)
  end

  def index_by_company_and_creator(conn, params) do
    {:ok, params} = params |> atomorphiform
    company_id = String.to_integer(params[:company_id])
    user_id = String.to_integer(params[:user_id])

    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_company_and_creator, creator_id: user_id) do
      true ->
        clients = Outsourcing.clients_by_company_and_creator(company_id, user_id, params)
        conn |> render("index.json-api", data: clients)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_company_and_team(conn, params) do
    {:ok, params} = params |> atomorphiform
    company_id = String.to_integer(params[:company_id])
    team_id = String.to_integer(params[:team_id])

    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_company_and_team, team_id: team_id) do
      true ->
        clients = Outsourcing.clients_by_company_and_team(company_id, team_id, params)
        conn |> render("index.json-api", data: clients)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_team(conn, params) do
    {:ok, params} = params |> atomorphiform
    team_id = String.to_integer(params[:team_id])

    current_user = current_resource(conn)

    case Auth.authorized?(current_user, :index_by_team, team_id: team_id) do
      true ->
        clients = Outsourcing.clients_by_team(team_id, params)
        conn |> render("index.json-api", data: clients)
      false ->
        {:error, :unauthorized}
    end
  end

  def index_by_team_lead(conn, %{"team_lead_id" => team_lead_id} = params) do
    {:ok, params} = params |> atomorphiform()
    team_lead_id = team_lead_id |> String.to_integer()
    current_user = conn |> current_resource()

    case Auth.authorized?(current_user, :index_by_team_lead, team_lead_id: team_lead_id) do
      true ->
        clients = team_lead_id |> Outsourcing.get_clients_under_team_lead(params)
        conn |> render("index.json-api", data: clients)
      false ->
        {:error, :unauthorized}
    end
  end

  def create(conn, %{"data" => data}) do
    current_user = current_resource(conn)

    {:ok, attrs} = data
      |> to_attributes
      |> Map.put("created_by", current_user.id)
      |> Morphix.atomorphiform

    with {:ok, client} <- Outsourcing.create_client(attrs) do
      conn |> render("show.json-api", data: client)
    end
  end

  def update(conn, %{"id" => client_id, "data" => data}) do
    {:ok, attrs} = data
      |> to_attributes
      |> atomorphiform

    with {:ok, client} <- Outsourcing.update_client(client_id, attrs) do
      conn |> render("show.json-api", data: client)
    end
  end

  def show(conn, %{"id" => id}) do
    case Outsourcing.get_client(id) do
      %Client{} = client ->
        conn |> render("show.json-api", data: client)
      nil ->
        {:error, :not_found}
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, client} <- Outsourcing.delete_client(id) do
      conn |> render("show.json-api", data: client)
    end
  end
end
