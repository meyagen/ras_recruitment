defmodule RasDirectoryApi.Organization.Role do
  @member 1
  @team_lead 2

  def from_name("team_lead"), do: @team_lead
  def from_name(_), do: @member

  def to_name(@team_lead), do: "team_lead"
  def to_name(_), do: "member"
end
