defmodule RasDirectoryApi.Organization.Team do
  use Ecto.Schema

  alias RasDirectoryApi.Organization.Membership

  schema "organization_teams" do
    field :name, :string, null: false
    field :active?, :boolean, default: true

    has_many :memberships, Membership

    timestamps()
  end
end
