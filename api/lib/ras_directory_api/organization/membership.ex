defmodule RasDirectoryApi.Organization.Membership do
  use Ecto.Schema
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Organization.Team

  schema "organization_memberships" do
    field :role, :integer, default: 1

    belongs_to :team, Team
    belongs_to :user, User

    timestamps()
  end
end
