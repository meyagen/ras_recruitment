defmodule RasDirectoryApi.Auth.Authentication do
  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias Comeonin.Bcrypt

  def find_and_confirm_password(username, password) do
    with %User{} = user <- Account.get_by_username(username),
         true <- Bcrypt.checkpw(password, user.password) do
           user
    else
      invalid ->
        Bcrypt.dummy_checkpw()
        invalid
    end
  end
end
