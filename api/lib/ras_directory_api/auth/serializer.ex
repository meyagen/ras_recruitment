defmodule RasDirectoryApi.Auth.Serializer do
  @behaviour Guardian.Serializer

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User

  def for_token(%User{} = user) do
    {:ok, "User:#{user.id}"}
  end

  def for_token(_) do
    {:error, "unknown resource type"}
  end

  def from_token("User:" <> id) do
    {:ok, Account.get_user(id)}
  end

  def from_token(_) do
    {:error, "unknown resource type"}
  end
end
