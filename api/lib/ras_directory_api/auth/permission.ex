defmodule RasDirectoryApi.Auth.Permission do
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Auth
  alias RasDirectoryApi.Organization

  def authorized?(user, action, opts \\ [])
  def authorized?(%User{id: user_id}, _, creator_id: user_id), do: true
  def authorized?(user, action, creator_id: creator_id) when action in [:index_by_creator, :index_by_company_and_creator]do
    Auth.is_admin(user) or Organization.is_team_lead_of_member(user.id, creator_id)
  end

  def authorized?(user, action, team_id: team_id) when action in [:index_by_company_and_team, :index_by_team] do
    team = Organization.get_team(team_id)
    Auth.is_admin(user) or Organization.is_team_lead(team, user)
  end

  def authorized?(%User{id: user_id} = user, :index_by_team_lead, team_lead_id: user_id) do
    Organization.team_lead?(user)
  end

  def authorized?(user, :index_by_team_lead, _) do
    Auth.is_admin(user)
  end
end
