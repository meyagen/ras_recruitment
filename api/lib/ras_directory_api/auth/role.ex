defmodule RasDirectoryApi.Auth.Role do
  alias RasDirectoryApi.Account.User

  @user_role 1
  @admin_role 2

  def is_admin(%User{} = user) do
    user.role == @admin_role
  end

  def from_name("admin"), do: @admin_role
  def from_name(_), do: @user_role

  def to_name(@admin_role), do: "admin"
  def to_name(@user_role), do: "user"
end
