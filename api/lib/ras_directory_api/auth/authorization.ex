defimpl Canada.Can, for: RasDirectoryApi.Account.User do
  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Auth
  alias RasDirectoryApi.Messaging.ApplicantMail
  alias RasDirectoryApi.Messaging.ApplicantNote
  alias RasDirectoryApi.Messaging.ClientMail
  alias RasDirectoryApi.Messaging.ClientNote
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Organization.Team
  alias RasDirectoryApi.Outsourcing.Client
  alias RasDirectoryApi.Recruitment.Applicant
  alias RasDirectoryApi.Recruitment.Company
  alias RasDirectoryApi.Recruitment.Profession
  alias RasDirectoryApi.Recruitment.Skill

  # Users
  def can?(current_user, :index, User) do
    Account.is_active(current_user)
  end

  def can?(current_user, :show, %User{}) do
    Account.is_active(current_user)
  end

  def can?(current_user, :create, User) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(%User{id: id}, :update, %User{id: id}), do: true
  def can?(current_user, :update, %User{}) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, :activate, %User{}) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  # Team
  def can?(current_user, action, Team) when action in [:index, :get_by_user] do
    Account.is_active(current_user)
  end

  def can?(current_user, :show, %Team{}) do
    Account.is_active(current_user)
  end

  def can?(current_user, :create, Team) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, :update, %Team{} = team) do
    Account.is_active(current_user) and (Auth.is_admin(current_user) or Organization.is_team_lead(team, current_user))
  end

  # Membership
  def can?(current_user, action, %Team{} = team) when action in [:create, :delete] do
    Account.is_active(current_user) and (Auth.is_admin(current_user) or Organization.is_team_lead(team, current_user))
  end

  def can?(current_user, :assign_team_lead, %Team{}) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, :assign_team_lead, nil) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, :get_by_user, %Team{}) do
    Account.is_active(current_user)
  end

  # Company
  def can?(current_user, action, Company) when action in [:create, :index] do
    Account.is_active(current_user)
  end

  def can?(current_user, action, %Company{}) when action in [:show, :update] do
    Account.is_active(current_user)
  end

  # Profession
  def can?(current_user, action, Profession) when action in [:create, :index] do
    Account.is_active(current_user)
  end

  # Skill
  def can?(current_user, action, Skill) when action in [:create, :index] do
    Account.is_active(current_user)
  end

  # Applicant
  def can?(current_user, :create, Applicant) do
    Account.is_active(current_user)
  end

  def can?(current_user, :index, Applicant) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, action, Applicant) when action in
    [:index_by_creator, :index_by_team, :index_by_company_and_creator, :index_by_company_and_team, :index_by_team_lead] do

    Account.is_active(current_user)
  end

  def can?(current_user, :index_by_company, Applicant) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(%User{id: user_id} = current_user, action, %Applicant{created_by: user_id}) when action in [:show, :update, :delete] do
    Account.is_active(current_user)
  end

  def can?(current_user, action, %Applicant{created_by: creator_id}) when action in [:show, :update, :delete] do
    Account.is_active(current_user) and (Auth.is_admin(current_user) or Organization.is_team_lead_of_member(current_user.id, creator_id))
  end

  # Client
  def can?(current_user, action, Client) when action in [:index, :index_by_company] do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, action, Client) when action in
    [:index_by_creator, :index_by_company_and_creator, :index_by_company_and_team, :index_by_team, :index_by_team_lead] do

    Account.is_active(current_user)
  end

  def can?(current_user, :create, Client) do
    Account.is_active(current_user)
  end

  def can?(%User{id: user_id} = current_user, action, %Client{created_by: user_id}) when action in [:show, :update] do
    Account.is_active(current_user)
  end

  def can?(current_user, action, %Client{created_by: creator_id}) when action in [:show, :update] do
    Account.is_active(current_user) and (Auth.is_admin(current_user) or Organization.is_team_lead_of_member(current_user.id, creator_id))
  end

  def can?(%User{id: user_id} = user, :delete, %Client{created_by: user_id}) do
    Account.is_active(user)
  end

  def can?(current_user, :delete, %Client{}) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  # Applicant Note
  def can?(%User{id: user_id} = user, :delete, %ApplicantNote{created_by: user_id}) do
    Account.is_active(user)
  end

  def can?(user, :delete, %ApplicantNote{}) do
    Account.is_active(user) and Auth.is_admin(user)
  end

  def can?(user, _, %ApplicantNote{}) do
    Account.is_active(user)
  end

  def can?(user, _, ApplicantNote) do
    Account.is_active(user)
  end

  # Applicant Mail
  def can?(user, action, ApplicantMail) when action in [:create, :index] do
    Account.is_active(user)
  end

  # Client Note
  def can?(%User{id: user_id} = user, :delete, %ClientNote{created_by: user_id}) do
    Account.is_active(user)
  end

  def can?(user, :delete, %ClientNote{}) do
    Account.is_active(user) and Auth.is_admin(user)
  end

  def can?(user, _, %ClientNote{}) do
    Account.is_active(user)
  end

  def can?(user, _, ClientNote) do
    Account.is_active(user)
  end

  # Applicant Mail
  def can?(user, action, ClientMail) when action in [:create, :index] do
    Account.is_active(user)
  end

  # Fallbacks
  def can?(current_user, :show, nil) do
    Account.is_active(current_user)
  end

  def can?(current_user, :create, nil) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end

  def can?(current_user, :delete, _) do
    Account.is_active(current_user) and Auth.is_admin(current_user)
  end
end
