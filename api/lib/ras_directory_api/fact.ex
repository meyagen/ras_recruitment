defmodule RasDirectoryApi.Fact do
  import Ecto.{Query, Changeset}, warn: false
  alias RasDirectoryApi.Fact.Country
  alias RasDirectoryApi.Repo

  def all_countries do
    Country
    |> Repo.all()
  end

  def get_country_by_name(name) do
    Country
    |> Repo.get_by(name: name)
  end

  def create_country(attrs \\ %{}) do
    %Country{}
    |> country_changeset(attrs)
    |> Repo.insert()
  end

  def country_changeset(model, params) do
    model
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, min: 3)
    |> unique_constraint(:name)
  end
end
