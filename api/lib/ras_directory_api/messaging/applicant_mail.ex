defmodule RasDirectoryApi.Messaging.ApplicantMail do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Recruitment.Applicant

  schema "messaging_applicant_mails" do
    field :subject, :string, null: false
    field :content, :string, null: false

    belongs_to :applicant, Applicant
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
