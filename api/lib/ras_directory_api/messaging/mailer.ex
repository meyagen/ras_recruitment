defmodule RasDirectoryApi.Messaging.Mailer do
  alias Phoenix.View
  alias RasDirectoryApiWeb.EmailView

  @config domain: Application.get_env(:ras_directory_api, :mailgun_domain),
    key: Application.get_env(:ras_directory_api, :mailgun_key)

  use Mailgun.Client, @config

  def basic_message(sender, recipient_email, subject, content) do
    html_content = content
      |> with_template

    send_email to: recipient_email,
      from: sender,
      subject: subject,
      html: html_content
  end

  defp with_template(content) do
    View.render_to_string(EmailView, "base.html", message: content)
  end
end
