defmodule RasDirectoryApi.Messaging.ClientNote do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Outsourcing.Client

  schema "messaging_client_notes" do
    field :content, :string, null: false

    belongs_to :client, Client
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
