defmodule RasDirectoryApi.Messaging.ClientMail do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Recruitment.Client

  schema "messaging_client_mails" do
    field :subject, :string, null: false
    field :content, :string, null: false

    belongs_to :client, Client
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
