defmodule RasDirectoryApi.ReleaseTasks do
  alias Ecto.Migrator
  alias RasDirectoryApi.Repo

  @start_apps [
    :postgrex,
    :ecto
  ]

  def migrate do
    IO.puts "Starting migration..."
    boot()
    Migrator.run(Repo, migrations_path(:ras_directory_api), :up, all: true)
    IO.puts "Done!"
    :init.stop()
  end

  defp boot do
    IO.puts "Booting the application..."

    Application.load(:ras_directory_api)
    Enum.each(@start_apps, &(Application.ensure_all_started(&1)))
    Repo.start_link(pool_size: 1)

    IO.puts "Done!"
  end

  defp migrations_path(app) do
    path = Path.join(["priv", "repo", "migrations"])
    Application.app_dir(app, path)
  end
end
