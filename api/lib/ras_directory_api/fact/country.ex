defmodule RasDirectoryApi.Fact.Country do
  use Ecto.Schema

  schema "fact_countries" do
    field :name, :string, null: false

    timestamps()
  end
end
