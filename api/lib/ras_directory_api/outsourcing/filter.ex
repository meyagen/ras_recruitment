defmodule RasDirectoryApi.Outsourcing.Filter do
  import Ecto.Query, warn: false

  @valid_filters [
    :first_name,
    :last_name,
    :email,
    :position,
    :country_id,
    :company_id,
    :created_by,
  ]

  def filter_with(query, filters) do
    clean_filters = filters
      |> Map.take(@valid_filters)

    query
    |> do_filter(clean_filters)
  end

  defp do_filter(query, %{} = filters) when map_size(filters) > 0 do
    query
    |> like(:first_name, filters[:first_name])
    |> like(:last_name, filters[:last_name])
    |> like(:position, filters[:position])
    |> equal(:email, filters[:email])
    |> equal(:country_id, filters[:country_id])
    |> equal(:company_id, filters[:company_id])
    |> equal(:created_by, filters[:created_by])
  end

  defp do_filter(query, _filters) do
    query
  end

  defp like(query, attr, val) when is_atom(attr) and is_binary(val) do
    query
    |> where([a], ilike(field(a, ^attr), ^"#{val}%"))
  end

  defp like(query, _attr, _val) do
    query
  end

  defp equal(query, attr, val) when is_atom(attr) and val != nil do
    query
    |> where([a], field(a, ^attr) == ^val)
  end

  defp equal(query, _attr, _val) do
    query
  end
end
