defmodule RasDirectoryApi.Outsourcing.Client do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Fact.Country
  alias RasDirectoryApi.Recruitment.Company

  schema "outsourcing_clients" do
    field :first_name, :string, null: false
    field :last_name, :string, null: false
    field :position, :string
    field :mobile_number, :string
    field :primary_number, :string
    field :secondary_number, :string
    field :tertiary_number, :string
    field :email, :string
    field :last_note_at, :naive_datetime, null: false

    belongs_to :company, Company
    belongs_to :country, Country
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
