defmodule RasDirectoryApi.Recruitment.Skill do
  use Ecto.Schema

  schema "recruitment_skills" do
    field :name, :string, null: false

    timestamps()
  end
end
