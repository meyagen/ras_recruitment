defmodule RasDirectoryApi.Recruitment.Applicant do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Fact.Country
  alias RasDirectoryApi.Recruitment.ApplicantSkill
  alias RasDirectoryApi.Recruitment.Company
  alias RasDirectoryApi.Recruitment.Profession
  alias RasDirectoryApi.Recruitment.Skill

  schema "recruitment_applicants" do
    field :first_name, :string, null: false
    field :last_name, :string, null: false
    field :landline_number, :string
    field :mobile_number, :string
    field :phone_number, :string
    field :other_number, :string
    field :email, :string
    field :status, :integer, default: 1
    field :last_note_at, :naive_datetime, null: false

    many_to_many :skills, Skill, join_through: ApplicantSkill, on_replace: :delete

    belongs_to :country, Country
    belongs_to :profession, Profession
    belongs_to :company, Company, foreign_key: :company_id
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
