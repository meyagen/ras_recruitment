defmodule RasDirectoryApi.Recruitment.ApplicantSkill do
  use Ecto.Schema

  alias RasDirectoryApi.Recruitment.Applicant
  alias RasDirectoryApi.Recruitment.Skill

  schema "recruitment_applicant_skills" do
    belongs_to :applicant, Applicant
    belongs_to :skill, Skill

    timestamps()
  end
end
