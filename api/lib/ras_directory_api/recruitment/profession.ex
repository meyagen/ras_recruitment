defmodule RasDirectoryApi.Recruitment.Profession do
  use Ecto.Schema

  schema "recruitment_professions" do
    field :name, :string, null: false

    timestamps()
  end
end
