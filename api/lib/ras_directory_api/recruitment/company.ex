defmodule RasDirectoryApi.Recruitment.Company do
  use Ecto.Schema

  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Fact.Country

  schema "recruitment_companies" do
    field :name, :string, null: false
    field :phone_number, :string
    field :email, :string

    belongs_to :country, Country
    belongs_to :creator, User, foreign_key: :created_by

    timestamps()
  end
end
