defmodule RasDirectoryApi.Recruitment.Filter do
  import Ecto.Query, warn: false

  @valid_filters [
    :first_name,
    :last_name,
    :email,
    :country_id,
    :profession_id,
    :company_id,
    :created_by,
    :skill_id
  ]

  def filter_with(query, filters) do
    clean_filters = filters
      |> Map.take(@valid_filters)

    query
    |> do_filter(clean_filters)
  end

  defp do_filter(query, %{} = filters) when map_size(filters) > 0 do
    query
    |> like(:first_name, filters[:first_name])
    |> like(:last_name, filters[:last_name])
    |> equal(:email, filters[:email])
    |> equal(:profession_id, filters[:profession_id])
    |> equal(:country_id, filters[:country_id])
    |> equal(:company_id, filters[:company_id])
    |> equal(:created_by, filters[:created_by])
    |> skill(filters[:skill_id])
  end

  defp do_filter(query, _filters) do
    query
  end

  defp like(query, attr, val) when is_atom(attr) and is_binary(val) do
    query
    |> where([a], ilike(field(a, ^attr), ^"#{val}%"))
  end

  defp like(query, _attr, _val) do
    query
  end

  defp equal(query, attr, val) when is_atom(attr) and val != nil do
    query
    |> where([a], field(a, ^attr) == ^val)
  end

  defp equal(query, _attr, _val) do
    query
  end

  defp skill(query, nil) do
    query
  end

  defp skill(query, skill_id) do
    query
    |> join(:inner, [a], s in assoc(a, :skills))
    |> where([a, ..., s], s.id == ^skill_id)
  end
end
