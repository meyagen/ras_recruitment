defmodule RasDirectoryApi.Outsourcing do
  import Ecto.{Query, Changeset}, warn: false

  alias RasDirectoryApi.Messaging.ClientNote
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Organization.Membership
  alias RasDirectoryApi.Outsourcing.Client
  alias RasDirectoryApi.Outsourcing.Filter
  alias RasDirectoryApi.Repo
  alias RasDirectoryApi.Utils

  @client_fields [
    :first_name,
    :last_name,
    :position,
    :mobile_number,
    :primary_number,
    :secondary_number,
    :tertiary_number,
    :email,
    :company_id,
    :country_id,
    :created_by
  ]

  @client_preload_fields [:country, :company, :creator]

  def all_clients(params) do
    query = from client in Client,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def clients_by_creator(user_id, params) do
    query = from client in Client,
      where: client.created_by == ^user_id,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def clients_by_company(company_id, params) do
    query = from client in Client,
      where: client.company_id == ^company_id,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def clients_by_company_and_creator(company_id, user_id, params) do
    query = from client in Client,
      where: client.company_id == ^company_id and client.created_by == ^user_id,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def clients_by_company_and_team(company_id, team_id, params) do
    query = from client in Client,
      join: membership in Membership, on: client.created_by == membership.user_id,
      where: client.company_id == ^company_id and membership.team_id == ^team_id,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def clients_by_team(team_id, params) do
    query = from client in Client,
      join: membership in Membership, on: client.created_by == membership.user_id,
      where: membership.team_id == ^team_id,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_clients_under_team_lead(team_lead_id, params) do
    lead_team_ids = team_lead_id
      |> Organization.get_user_lead_teams()
      |> Enum.map(&(&1.id))

    query = from client in Client,
      join: membership in Membership, on: client.created_by == membership.user_id,
      where: membership.team_id in ^lead_team_ids,
      preload: ^@client_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def create_client(attrs) do
    changeset = %Client{}
      |> client_changeset(attrs)

    with {:ok, client} <- Repo.insert(changeset) do
      {:ok, client |> Repo.preload(@client_preload_fields)}
    end
  end

  def update_client(client_id, attrs) do
    case get_client(client_id) do
      %Client{} = client ->
        with {:ok, client} <- Repo.update(client_changeset(client, attrs)) do
          {:ok, client |> Repo.preload(@client_preload_fields, force: true)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def update_client_last_note_at(client_id, %ClientNote{} = note) do
    case get_client(client_id) do
      %Client{} = client ->
        changeset = client
          |> change(last_note_at: note.inserted_at)

        with {:ok, client} <- Repo.update(changeset) do
          {:ok, client |> Repo.preload(@client_preload_fields, force: true)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def get_client(client_id) do
    Client
    |> Repo.get(client_id)
    |> Repo.preload(@client_preload_fields)
  end

  def delete_client(client_id) do
    case get_client(client_id) do
      %Client{} = client ->
        Repo.delete(client)
      nil ->
        {:error, :not_found}
    end
  end

  defp client_changeset(model, params) do
    model
    |> cast(params, @client_fields)
    |> validate_required([:first_name, :last_name, :country_id, :company_id, :created_by])
    |> validate_length(:first_name, min: 2)
    |> validate_length(:last_name, min: 2)
    |> foreign_key_constraint(:country_id)
    |> foreign_key_constraint(:company_id)
    |> foreign_key_constraint(:created_by)
    |> put_change(:last_note_at, NaiveDateTime.utc_now())
  end
end
