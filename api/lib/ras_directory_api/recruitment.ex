defmodule RasDirectoryApi.Recruitment do
  import Ecto.{Query, Changeset}, warn: false

  alias RasDirectoryApi.Messaging.ApplicantNote
  alias RasDirectoryApi.Organization
  alias RasDirectoryApi.Organization.Membership
  alias RasDirectoryApi.Recruitment.Applicant
  alias RasDirectoryApi.Recruitment.Company
  alias RasDirectoryApi.Recruitment.Filter
  alias RasDirectoryApi.Recruitment.Profession
  alias RasDirectoryApi.Recruitment.Skill
  alias RasDirectoryApi.Repo
  alias RasDirectoryApi.Utils

  @company_fields [:name, :phone_number, :email, :country_id, :created_by]

  @applicant_fields [
    :first_name,
    :last_name,
    :landline_number,
    :mobile_number,
    :phone_number,
    :other_number,
    :email,
    :status,
    :country_id,
    :profession_id,
    :company_id,
    :created_by
  ]

  @applicant_preload_fields [:skills, :country, :profession, :company, :creator]

  def all_applicants(params) do
    Applicant
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
    |> Repo.preload(@applicant_preload_fields)
  end

  def get_applicants_by_creator(user_id, params) do
    query = from applicant in Applicant,
      where: applicant.created_by == ^user_id,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicants_by_company(company_id, params) do
    query = from applicant in Applicant,
      where: applicant.company_id == ^company_id,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicants_by_company_and_creator(company_id, user_id, params) do
    query = from applicant in Applicant,
      where: applicant.company_id == ^company_id and applicant.created_by == ^user_id,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicants_by_company_and_team(company_id, team_id, params) do
    query = from applicant in Applicant,
      join: membership in Membership, on: applicant.created_by == membership.user_id,
      where: applicant.company_id == ^company_id and membership.team_id == ^team_id,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicants_by_team(team_id, params) do
    query = from applicant in Applicant,
      join: membership in Membership, on: applicant.created_by == membership.user_id,
      where: membership.team_id == ^team_id,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicants_under_team_lead(team_lead_id, params) do
    lead_team_ids = team_lead_id
      |> Organization.get_user_lead_teams()
      |> Enum.map(&(&1.id))

    query = from applicant in Applicant,
      join: membership in Membership, on: applicant.created_by == membership.user_id,
      where: membership.team_id in ^lead_team_ids,
      preload: ^@applicant_preload_fields

    query
    |> Filter.filter_with(params)
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_applicant(applicant_id) do
    Applicant
    |> Repo.get(applicant_id)
    |> Repo.preload(@applicant_preload_fields)
  end

  def create_applicant(%{skill_ids: _} = attrs) do
    attrs = attrs
      |> Map.put(:skills, get_skills(attrs[:skill_ids]))
      |> Map.delete(:skill_ids)

    create_applicant(attrs)
  end

  def create_applicant(%{skills: _} = attrs) do
    changeset = %Applicant{}
      |> applicant_changeset(attrs)

    with {:ok, applicant} <- Repo.insert(changeset) do
      {:ok, applicant |> Repo.preload(@applicant_preload_fields)}
    end
  end

  def create_applicant(attrs) do
    attrs = attrs
      |> Map.put(:skills, [])

    create_applicant(attrs)
  end

  def update_applicant(applicant_id, %{skill_ids: _} = attrs) do
    attrs = attrs
      |> Map.put(:skills, get_skills(attrs[:skill_ids]))
      |> Map.delete(:skill_ids)

    update_applicant(applicant_id, attrs)
  end

  def update_applicant(applicant_id, %{skills: _} = attrs) do
    case get_applicant(applicant_id) do
      %Applicant{} = applicant ->
        with {:ok, applicant} <- Repo.update(applicant_changeset(applicant, attrs)) do
          {:ok, applicant |> Repo.preload(@applicant_preload_fields, force: true)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def update_applicant(applicant_id, attrs) do
    attrs = attrs
      |> Map.put(:skills, [])

    update_applicant(applicant_id, attrs)
  end

  def update_applicant_last_note_at(applicant_id, %ApplicantNote{} = note) do
    case get_applicant(applicant_id) do
      %Applicant{} = applicant ->
        changeset = applicant
          |> change(last_note_at: note.inserted_at)

        with {:ok, applicant} <- Repo.update(changeset) do
          {:ok, applicant |> Repo.preload(@applicant_preload_fields, force: true)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def delete_applicant(applicant_id) do
    case get_applicant(applicant_id) do
      %Applicant{} = applicant ->
        Repo.delete(applicant)
      nil ->
        {:error, :not_found}
    end
  end

  def all_applicant_skills do
    ApplicantSkill
    |> Repo.all()
  end

  def applicant_status("placed"), do: 2
  def applicant_status("open"), do: 1
  def applicant_status(1), do: "open"
  def applicant_status(2), do: "placed"
  def applicant_status(_), do: 1

  # Companies
  def all_companies(params) do
    Company
    |> Utils.query_params(params)
    |> Repo.all()
    |> Repo.preload([:creator, :country])
  end

  def create_company(attrs \\ %{}) do
    changeset = %Company{}
      |> company_changeset(attrs)

    with {:ok, company} <- Repo.insert(changeset) do
      {:ok, company |> Repo.preload([:creator, :country])}
    end
  end

  def update_company(company_id, attrs) do
    case get_company(company_id) do
      %Company{} = company ->
        with {:ok, company} <- Repo.update(company_changeset(company, attrs)) do
          {:ok, company |> Repo.preload([:creator, :country], force: true)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def get_company(company_id) do
    Company
    |> Repo.get(company_id)
    |> Repo.preload([:creator, :country])
  end

  def get_company_by_name(name) do
    Company
    |> Repo.get_by(name: name)
    |> Repo.preload([:creator, :country])
  end

  # Professions
  def all_professions do
    Profession
    |> Repo.all()
  end

  def get_profession_by_name(name) do
    Profession
    |> Repo.get_by(name: name)
  end

  def create_profession(%{name: name}) do
    attrs = %{name: String.downcase(name)}

    %Profession{}
    |> profession_changeset(attrs)
    |> Repo.insert()
  end

  def create_profession(attrs) do
    %Profession{}
    |> profession_changeset(attrs)
    |> Repo.insert()
  end

  # Skills
  def all_skills do
    Skill
    |> Repo.all()
  end

  def get_skills([_ | _] = skill_ids) do
    query = from skill in Skill,
      where: skill.id in ^skill_ids

    Repo.all(query)
  end

  def create_skill(%{name: name}) do
    attrs = %{name: String.downcase(name)}

    %Skill{}
    |> skill_changeset(attrs)
    |> Repo.insert()
  end

  def create_skill(attrs) do
    %Skill{}
    |> skill_changeset(attrs)
    |> Repo.insert()
  end

  def get_skill(skill_id) do
    Skill
    |> Repo.get(skill_id)
  end

  def get_skill_by_name(name) do
    Skill
    |> Repo.get_by(name: name)
  end

  defp applicant_changeset(model, params) do
    model
    |> cast(params, @applicant_fields)
    |> put_assoc(:skills, params[:skills])
    |> validate_required([:first_name, :last_name, :country_id, :profession_id, :created_by])
    |> validate_length(:first_name, min: 2)
    |> validate_length(:last_name, min: 2)
    |> foreign_key_constraint(:country_id)
    |> foreign_key_constraint(:profession_id)
    |> foreign_key_constraint(:company_id)
    |> put_change(:last_note_at, NaiveDateTime.utc_now())
  end

  defp company_changeset(model, params) do
    model
    |> cast(params, @company_fields)
    |> validate_required([:name, :country_id, :created_by])
    |> validate_length(:name, min: 3)
    |> foreign_key_constraint(:country_id)
    |> unique_constraint(:name)
  end

  defp profession_changeset(model, params) do
    model
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, min: 3)
    |> unique_constraint(:name)
  end

  defp skill_changeset(model, params) do
    model
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, min: 3)
    |> unique_constraint(:name)
  end
end
