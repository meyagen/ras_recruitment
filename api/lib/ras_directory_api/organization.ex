defmodule RasDirectoryApi.Organization do
  import Ecto.{Query, Changeset}, warn: false
  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Organization.Membership
  alias RasDirectoryApi.Organization.Role
  alias RasDirectoryApi.Organization.Team
  alias RasDirectoryApi.Repo
  alias RasDirectoryApi.Utils

  def all_teams(params) do
    Team
    |> Utils.query_params(params)
    |> Repo.all()
    |> Repo.preload(:memberships)
  end

  def get_user_teams(user_id, params \\ %{}) do
    query = from team in Team,
      join: membership in assoc(team, :memberships),
      where: membership.user_id == ^user_id,
      preload: :memberships

    query
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def get_user_lead_teams(user_id) do
    team_lead_role = role_from_name("team_lead")
    query = from team in Team,
      join: membership in assoc(team, :memberships),
      where: membership.user_id == ^user_id and membership.role == ^team_lead_role,
      preload: :memberships

    query
    |> Repo.all()
  end

  def create_team(attrs \\ %{}) do
    changeset = %Team{}
      |> team_changeset(attrs)

    with {:ok, team} <- Repo.insert(changeset) do
      {:ok, team |> Repo.preload(:memberships)}
    end
  end

  def update_team(team_id, attrs) do
    case get_team(team_id) do
      %Team{} = team ->
        with {:ok, team} <- Repo.update(team_changeset(team, attrs)) do
          {:ok, team |> Repo.preload(:memberships)}
        end
      nil ->
        {:error, :not_found}
    end
  end

  def get_team(team_id) do
    Team
    |> Repo.get(team_id)
    |> Repo.preload(:memberships)
  end

  def create_membership(attrs \\ %{}) do
    %Membership{}
    |> membership_changeset(attrs)
    |> Repo.insert()
  end

  def update_membership_role(%Membership{} = membership, role) do
    membership
    |> change(%{role: role})
    |> Repo.update()
  end

  def get_membership(team_id, user_id) do
    Repo.get_by(Membership, team_id: team_id, user_id: user_id)
  end

  def get_membership_from_team(%Team{} = team, user_id) do
    Enum.find(team.memberships, fn(membership) ->
      membership.user_id == user_id
    end)
  end

  def remove_user_from_team(user_id, team_id) do
    case get_membership(team_id, user_id) do
      %Membership{} = membership ->
        Repo.delete(membership)
      nil ->
        {:error, :not_found}
    end
  end

  def team_lead?(%User{id: user_id}) do
    teams = user_id |> get_user_lead_teams()
    teams != []
  end

  def is_team_lead(%Team{} = team, %User{} = user) do
    membership = case Ecto.assoc_loaded?(team.memberships) do
      true ->
        get_membership_from_team(team, user.id)
      false ->
        get_membership(team.id, user.id)
    end

    membership != nil and membership.role == role_from_name("team_lead")
  end

  def is_team_lead_of_member(target_user_id, member_user_id) do
    target_user = Account.get_user(target_user_id)

    member_teams = get_user_teams(member_user_id)
    common_team = Enum.find(member_teams, fn(team) ->
      is_team_lead(team, target_user)
    end)

    common_team != nil
  end

  def role_from_name(role_name) do
    Role.from_name(role_name)
  end

  def role_to_name(role) do
    Role.to_name(role)
  end

  defp team_changeset(model, params) do
    model
    |> cast(params, [:name])
    |> validate_required([:name])
    |> validate_length(:name, min: 3)
    |> unique_constraint(:name)
  end

  defp membership_changeset(model, params) do
    model
    |> cast(params, [:team_id, :user_id, :role])
    |> foreign_key_constraint(:team_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:user_id, name: :organization_memberships_team_id_user_id_index)
  end
end
