defmodule RasDirectoryApi.Account do
  import Ecto.{Query, Changeset}, warn: false
  alias RasDirectoryApi.Repo
  alias RasDirectoryApi.Account.User
  alias Comeonin.Bcrypt

  @fields [
    :username,
    :email,
    :password,
    :first_name,
    :last_name,
    :position,
    :role,
    :status
  ]

  @update_fields [
    :email,
    :password,
    :first_name,
    :last_name,
    :position,
  ]

  @active_user_status 1
  @banned_user_status 0

  def create_user(attrs \\ %{}) do
    %User{}
    |> user_changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> user_update_changeset(attrs)
    |> Repo.update()
  end

  def get_all_users do
    Repo.all(User)
  end

  def get_user(id) do
    Repo.get(User, id)
  end

  def get_by_username(username) do
    Repo.get_by(User, username: username)
  end

  def ban_user(%User{status: @active_user_status} = user) do
    user
    |> change(status: @banned_user_status)
    |> Repo.update
  end

  def ban_user(%User{status: @banned_user_status}) do
    {:error, :already_banned}
  end

  def update_user_role(user_id, role) do
    case get_user(user_id) do
      %User{} = user ->
        user
        |> change(role: role)
        |> Repo.update()
      nil ->
        {:error, :not_found}
    end
  end

  def update_status(user_id, status) do
    case get_user(user_id) do
      %User{} = user ->
        user
        |> change(status: status)
        |> Repo.update()
      nil ->
        {:error, :not_found}
    end
  end

  def is_active(user) do
    user.status == @active_user_status
  end

  def status(@active_user_status), do: "active"
  def status(_), do: "banned"

  def to_status("active"), do: @active_user_status
  def to_status("banned"), do: @banned_user_status

  defp user_changeset(model, params) do
    model
    |> cast(params, @fields)
    |> validate_required([:username, :email, :password])
    |> validate_length(:username, max: 32, min: 5)
    |> unique_constraint(:username)
    |> validate_format(:email, ~r/^.+@rasrecruitment\.com$/)
    |> unique_constraint(:email)
    |> validate_length(:password, max: 32, min: 8)
    |> force_change(:password, hash_password(params[:password]))
  end

  defp user_update_changeset(model, params) do
    model
    |> cast(params, @update_fields)
    |> validate_required([:username, :email, :password])
    |> validate_format(:email, ~r/^.+@rasrecruitment\.com$/)
    |> unique_constraint(:email)
    |> validate_length(:password, max: 32, min: 8)
    |> force_change(:password, hash_password(params[:password]))
  end

  defp hash_password(raw_password) do
    Bcrypt.hashpwsalt(raw_password)
  end
end
