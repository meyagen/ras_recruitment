defmodule RasDirectoryApi.Auth do
  import Ecto.{Query, Changeset}, warn: false

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Account.User
  alias RasDirectoryApi.Auth.Authentication
  alias RasDirectoryApi.Auth.Permission
  alias RasDirectoryApi.Auth.Role

  def sign_in(username, password) do
    with %User{} = user <- Authentication.find_and_confirm_password(username, password),
      true <- Account.is_active(user) do
      {:ok, user}
    else
      _ ->
        {:error, :invalid_user}
    end
  end

  def authorized?(user, action, opts \\ []) do
    Permission.authorized?(user, action, opts)
  end

  def is_admin(%User{} = user) do
    Role.is_admin(user)
  end

  def role_from_name(role_name) do
    Role.from_name(role_name)
  end

  def role_name(role) do
    Role.to_name(role)
  end
end
