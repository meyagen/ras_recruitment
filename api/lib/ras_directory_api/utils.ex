defmodule RasDirectoryApi.Utils do
  import Ecto.Query, warn: false

  @sort_by "id"
  @sort "desc"
  @count "50"

  def query_params(query, params) when params == %{} do
    query
  end

  def query_params(query, %{page: _} = params) do
    page = String.to_integer(params[:page])
    count = params[:count] || @count

    count = String.to_integer(count)
    offset_count = (page - 1) * count

    params = params
      |> Map.delete(:page)
      |> Map.put(:count, Integer.to_string(count))

    query
    |> query_params(params)
    |> offset(^offset_count)
    |> limit(^count)
  end

  def query_params(query, %{} = params) do
    sort_by = params[:sort_by] || @sort_by
    sort = params[:sort] || @sort

    query
    |> order_by([{^String.to_atom(sort), ^String.to_atom(sort_by)}])
    |> add_limit(params)
  end

  defp add_limit(query, %{count: count}) do
    query
    |> limit(^String.to_integer(count))
  end

  defp add_limit(query, %{}) do
    query
  end
end
