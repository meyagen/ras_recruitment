defmodule RasDirectoryApi.Account.User do
  use Ecto.Schema

  alias RasDirectoryApi.Organization.Membership

  schema "account_users" do
    field :username, :string, null: false
    field :password, :string, null: false
    field :email, :string, null: false
    field :first_name, :string
    field :last_name, :string
    field :position, :string
    field :role, :integer, default: 1
    field :status, :integer, default: 1

    has_many :memberships, Membership

    timestamps()
  end
end
