defmodule RasDirectoryApi.Messaging do
  import Ecto.{Query, Changeset}, warn: false

  alias RasDirectoryApi.Account
  alias RasDirectoryApi.Messaging.ApplicantMail
  alias RasDirectoryApi.Messaging.ApplicantNote
  alias RasDirectoryApi.Messaging.ClientMail
  alias RasDirectoryApi.Messaging.ClientNote
  alias RasDirectoryApi.Messaging.Mailer
  alias RasDirectoryApi.Outsourcing
  alias RasDirectoryApi.Recruitment
  alias RasDirectoryApi.Repo
  alias RasDirectoryApi.Utils

  @applicant_mail_fields [:subject, :content, :applicant_id, :created_by]
  @applicant_note_fields [:content, :applicant_id, :created_by]
  @client_mail_fields [:subject, :content, :client_id, :created_by]
  @client_note_fields [:content, :client_id, :created_by]

  def applicant_notes(applicant_id, params \\ %{}) do
    query = from applicant_note in ApplicantNote,
      where: applicant_note.applicant_id == ^applicant_id,
      preload: [:creator]

    query
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def client_notes(client_id, params \\ %{}) do
    query = from applicant_note in ClientNote,
      where: applicant_note.client_id == ^client_id,
      preload: [:creator]

    query
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def create_note(%{applicant_id: _} = attrs) do
    changeset = %ApplicantNote{}
      |> applicant_note_changeset(attrs)

    with {:ok, note} <- Repo.insert(changeset) do
      {:ok, _} = Recruitment.update_applicant_last_note_at(attrs[:applicant_id], note)
      {:ok, note |> Repo.preload([:creator])}
    end
  end

  def create_note(%{client_id: _} = attrs) do
    changeset = %ClientNote{}
      |> client_note_changeset(attrs)

    with {:ok, note} <- Repo.insert(changeset) do
      {:ok, _} = Outsourcing.update_client_last_note_at(attrs[:client_id], note)
      {:ok, note |> Repo.preload([:creator])}
    end
  end

  def get_applicant_note(id) do
    ApplicantNote
    |> Repo.get(id)
    |> Repo.preload([:creator])
  end

  def get_client_note(id) do
    ClientNote
    |> Repo.get(id)
    |> Repo.preload([:creator])
  end

  def delete_applicant_note(id) do
    case get_applicant_note(id) do
      %ApplicantNote{} = applicant_note ->
        Repo.delete(applicant_note)
      nil ->
        {:error, :not_found}
    end
  end

  def delete_client_note(id) do
    case get_client_note(id) do
      %ClientNote{} = client_note ->
        Repo.delete(client_note)
      nil ->
        {:error, :not_found}
    end
  end

  def applicant_mails(applicant_id, params \\ %{}) do
    query = from applicant_mail in ApplicantMail,
      where: applicant_mail.applicant_id == ^applicant_id,
      preload: [:creator]

    query
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def client_mails(client_id, params \\ %{}) do
    query = from client_mail in ClientMail,
      where: client_mail.client_id == ^client_id,
      preload: [:creator]

    query
    |> Utils.query_params(params)
    |> Repo.all()
  end

  def send_and_record_mail(%{applicant_id: applicant_id, created_by: user_id} = attrs) do
    sender_user = Account.get_user(user_id)
    receiver_applicant = Recruitment.get_applicant(applicant_id)

    with {:ok, _} <- send_mail(sender_user, receiver_applicant.email, attrs[:subject], attrs[:content]) do
      create_mail_record(attrs)
    end
  end

  def send_and_record_mail(%{client_id: client_id, created_by: user_id} = attrs) do
    sender_user = Account.get_user(user_id)
    receiver_client = Outsourcing.get_client(client_id)

    with {:ok, _} <- send_mail(sender_user, receiver_client.email, attrs[:subject], attrs[:content]) do
      create_mail_record(attrs)
    end
  end

  def send_mail(from_user, to, subject, content) do
    from = "#{from_user.first_name} #{from_user.last_name} <#{from_user.email}>"
    Mailer.basic_message(from, to, subject, content)
  end

  def create_mail_record(%{applicant_id: _} = attrs) do
    changeset = %ApplicantMail{}
      |> applicant_mail_changeset(attrs)

    with {:ok, mail} <- Repo.insert(changeset) do
      {:ok, mail |> Repo.preload([:creator])}
    end
  end

  def create_mail_record(%{client_id: _} = attrs) do
    changeset = %ClientMail{}
      |> client_mail_changeset(attrs)

    with {:ok, mail} <- Repo.insert(changeset) do
      {:ok, mail |> Repo.preload([:creator])}
    end
  end

  defp applicant_note_changeset(model, attrs) do
    model
    |> cast(attrs, @applicant_note_fields)
    |> validate_required(@applicant_note_fields)
    |> foreign_key_constraint(:applicant_id)
    |> foreign_key_constraint(:created_by)
  end

  defp applicant_mail_changeset(model, attrs) do
    model
    |> cast(attrs, @applicant_mail_fields)
    |> validate_required(@applicant_mail_fields)
    |> foreign_key_constraint(:applicant_id)
    |> foreign_key_constraint(:created_by)
  end

  defp client_note_changeset(model, attrs) do
    model
    |> cast(attrs, @client_note_fields)
    |> validate_required(@client_note_fields)
    |> foreign_key_constraint(:client_id)
    |> foreign_key_constraint(:created_by)
  end

  defp client_mail_changeset(model, attrs) do
    model
    |> cast(attrs, @client_mail_fields)
    |> validate_required(@client_mail_fields)
    |> foreign_key_constraint(:client_id)
    |> foreign_key_constraint(:created_by)
  end
end
