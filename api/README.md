# RasDirectoryApi
API for **R and S Directory**. Made using `Phoenix Framework`.

## Initial set-up
  * Install `elixir`
  * Install `phoenix`
  * Install `postgresql`
  * Create a `postgresql` user `ras_directory` with password `rasdirectoryapi`. This user should be able to create a database.
  * Add `api.ras-directory.com` to your hostfile and point it to `127.0.0.1`
  * Clone this repository

## Start the server
  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`api.ras-directory.com:4000`](http://api.ras-directory.com:4000) from your browser.
