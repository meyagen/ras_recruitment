use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ras_directory_api, RasDirectoryApiWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :ras_directory_api, RasDirectoryApi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "ras_directory",
  password: "rasdirectoryapi",
  database: "ras_directory_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# Configure Guardian
config :guardian, Guardian,
  issuer: "ras_directory_api",
  ttl: {1, :days},
  secret_key: "0/yn7cTUrwuI8/ePQ8UwpINzAR6qOVbe3AlQSO6G8KzOP1zTeS1HRk3m/c8wnJUM",
  serializer: RasDirectoryApi.Auth.Serializer

# Configure comeonin
config :bcrypt_elixir, :log_rounds, 4

# Configure Mailgun
config :ras_directory_api,
  mailgun_domain: "https://api.mailgun.net/v3/sandbox1b85015f1c9e4c07bfb9b255b6e38192.mailgun.org",
  mailgun_key: "key-b2c9d376c9c5af5692c27f91bc065453"
