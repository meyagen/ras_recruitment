# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ras_directory_api,
  ecto_repos: [RasDirectoryApi.Repo]

# Configures the endpoint
config :ras_directory_api, RasDirectoryApiWeb.Endpoint,
  render_errors: [view: RasDirectoryApiWeb.ErrorView, accepts: ~w(json-api)],
  pubsub: [name: RasDirectoryApi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures ja_serializer
config :phoenix, :format_encoders,
  "json-api": Poison

config :mime, :types, %{
  "application/vnd.api+json" => ["json-api"]
}

config :phoenix, RasDirectoryApi.Endpoint,
  render_errors: [view: RasDirectoryApi.ErrorView, accepts: ~w(json-api)]

# Configures Canary
config :canary, repo: RasDirectoryApi.Repo,
  unauthorized_handler: {RasDirectoryApiWeb.ErrorController, :unauthorized}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
