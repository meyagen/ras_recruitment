use Mix.Config

port = 4000

config :ras_directory_api, RasDirectoryApiWeb.Endpoint,
  load_from_system_env: true,
  url: [host: "localhost", port: port],
  http: [port: port],
  server: true

config :logger,
  backends: [:console, {LoggerFileBackend, :info}, {LoggerFileBackend, :error}]

config :logger, :info,
  level: :info,
  path: "/mnt/ras/log/ras_api_info.log"

config :logger, :error,
  level: :error,
  path: "/mnt/ras/log/ras_api_error.log"

import_config "prod.secret.exs"
