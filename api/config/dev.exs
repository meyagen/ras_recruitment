use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :ras_directory_api, RasDirectoryApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0NOSRBNTh6dYkV9NSmFXhnF2Rrh/v9HaKGTh1XaEw9wHfBM2rk/MSM2Z6bLeerJA",
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :ras_directory_api, RasDirectoryApi.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "ras_directory",
  password: "rasdirectoryapi",
  database: "ras_directory_dev",
  hostname: "localhost",
  pool_size: 10

# Configure Guardian
config :guardian, Guardian,
  issuer: "ras_directory_api",
  ttl: {7, :days},
  secret_key: "dfJb+vK6A0juT7vtz6p3Y9zDKHN0N+rye4pq8S45g5XcwHgKxtcqjx3hokfbF/SR",
  serializer: RasDirectoryApi.Auth.Serializer

# Configure Mailgun
config :ras_directory_api,
  mailgun_domain: "https://api.mailgun.net/v3/sandbox1b85015f1c9e4c07bfb9b255b6e38192.mailgun.org",
  mailgun_key: "key-b2c9d376c9c5af5692c27f91bc065453"
