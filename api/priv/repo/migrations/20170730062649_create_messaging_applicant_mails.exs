defmodule RasDirectoryApi.Repo.Migrations.CreateMessagingApplicantMails do
  use Ecto.Migration

  def change do
    create table(:messaging_applicant_mails) do
      add :subject, :string, null: false
      add :content, :text, null: false
      add :applicant_id, references(:recruitment_applicants)
      add :created_by, references(:account_users)

      timestamps()
    end
  end
end
