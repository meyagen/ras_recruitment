defmodule RasDirectoryApi.Repo.Migrations.CreateRecruitmentSkills do
  use Ecto.Migration

  def change do
    create table(:recruitment_skills) do
      add :name, :string, null: false
      timestamps()
    end

    create unique_index(:recruitment_skills, [:name], unique: true)
  end
end
