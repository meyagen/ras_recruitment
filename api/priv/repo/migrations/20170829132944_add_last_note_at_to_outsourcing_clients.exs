defmodule RasDirectoryApi.Repo.Migrations.AddLastNoteAtToOutsourcingClients do
  use Ecto.Migration

  def change do
    alter table(:outsourcing_clients) do
      add :last_note_at, :naive_datetime, null: false, default: fragment("now()")
    end
  end
end
