defmodule RasDirectoryApi.Repo.Migrations.AddCascadeToOutsourcingClientMails do
  use Ecto.Migration

  def up do
    execute "ALTER TABLE messaging_client_mails DROP CONSTRAINT messaging_client_mails_client_id_fkey"
    alter table(:messaging_client_mails) do
      modify :client_id, references(:outsourcing_clients, on_delete: :delete_all)
    end
  end

  def down do
    execute "ALTER TABLE messaging_client_mails DROP CONSTRAINT messaging_client_mails_client_id_fkey"
    alter table(:messaging_client_mails) do
      modify :client_id, references(:outsourcing_clients, on_delete: :nothing)
    end
  end
end
