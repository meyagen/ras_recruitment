defmodule RasDirectoryApi.Repo.Migrations.AddPositionToAccountUser do
  use Ecto.Migration

  def change do
    alter table(:account_users) do
      add :position, :string
    end
  end
end
