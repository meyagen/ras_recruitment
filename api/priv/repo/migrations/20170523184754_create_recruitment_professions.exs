defmodule RasDirectoryApi.Repo.Migrations.CreateRecruitmentProfessions do
  use Ecto.Migration

  def change do
    create table(:recruitment_professions) do
      add :name, :string, null: false
      timestamps()
    end

    create unique_index(:recruitment_professions, [:name], unique: true)
  end
end
