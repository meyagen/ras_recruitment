defmodule RasDirectoryApi.Repo.Migrations.AddStatusToRecruitmentApplicants do
  use Ecto.Migration

  def change do
    alter table(:recruitment_applicants) do
      add :status, :smallint, default: 1
    end
  end
end
