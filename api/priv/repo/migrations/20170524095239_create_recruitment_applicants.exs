defmodule RasDirectoryApi.Repo.Migrations.CreateRecruitmentApplicants do
  use Ecto.Migration

  def change do
    create table(:recruitment_applicants) do
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :landline_number, :string
      add :mobile_number, :string
      add :phone_number, :string
      add :email, :string

      add :country_id, references(:fact_countries)
      add :company_id, references(:recruitment_companies)
      add :profession_id, references(:recruitment_professions)
      add :created_by, references(:account_users)

      timestamps()
    end

    create index(:recruitment_applicants, [:country_id])
    create index(:recruitment_applicants, [:company_id])
    create index(:recruitment_applicants, [:profession_id])
    create index(:recruitment_applicants, [:created_by])
  end
end
