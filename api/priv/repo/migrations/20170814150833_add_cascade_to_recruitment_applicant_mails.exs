defmodule RasDirectoryApi.Repo.Migrations.AddCascadeToApplicantMails do
  use Ecto.Migration

  def up do
    execute "ALTER TABLE messaging_applicant_mails DROP CONSTRAINT messaging_applicant_mails_applicant_id_fkey"
    alter table(:messaging_applicant_mails) do
      modify :applicant_id, references(:recruitment_applicants, on_delete: :delete_all)
    end
  end

  def down do
    execute "ALTER TABLE messaging_applicant_mails DROP CONSTRAINT messaging_applicant_mails_applicant_id_fkey"
    alter table(:messaging_applicant_mails) do
      modify :applicant_id, references(:recruitment_applicants, on_delete: :nothing)
    end
  end
end
