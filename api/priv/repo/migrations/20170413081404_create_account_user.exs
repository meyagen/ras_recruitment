defmodule RasDirectoryApi.Repo.Migrations.CreateRasDirectoryApi.Account.User do
  use Ecto.Migration

  def change do
    create table(:account_users) do
      add :username, :string, null: false
      add :password, :string, null: false
      add :email, :string, null: false
      add :first_name, :string
      add :last_name, :string
      add :role, :integer, default: 1
      add :status, :integer, default: 1

      timestamps
    end

    create unique_index(:account_users, [:username])
    create unique_index(:account_users, [:email])
  end
end
