defmodule RasDirectoryApi.Repo.Migrations.CreateRecruitmentCompany do
  use Ecto.Migration

  def change do
    create table(:recruitment_companies) do
      add :name, :string, null: false
      add :phone_number, :string
      add :email, :string
      add :country_id, references(:fact_countries)
      add :created_by, references(:account_users)

      timestamps()
    end

    create unique_index(:recruitment_companies, [:name])
    create index(:recruitment_companies, [:country_id])
    create index(:recruitment_companies, [:created_by])
  end
end
