defmodule RasDirectoryApi.Repo.Migrations.AddOtherNumberToRecruitmentApplicants do
  use Ecto.Migration

  def change do
    alter table(:recruitment_applicants) do
      add :other_number, :string
    end
  end
end
