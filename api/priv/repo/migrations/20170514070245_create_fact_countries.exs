defmodule RasDirectoryApi.Repo.Migrations.CreateFactCountries do
  use Ecto.Migration

  def change do
    create table(:fact_countries) do
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:fact_countries, [:name])
  end
end
