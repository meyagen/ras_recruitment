defmodule RasDirectoryApi.Repo.Migrations.AddCascadeToRecruitmentApplicantSkills do
  use Ecto.Migration

  def up do
    execute "ALTER TABLE recruitment_applicant_skills DROP CONSTRAINT recruitment_applicant_skills_applicant_id_fkey"
    alter table(:recruitment_applicant_skills) do
      modify :applicant_id, references(:recruitment_applicants, on_delete: :delete_all)
    end
  end

  def down do
    execute "ALTER TABLE recruitment_applicant_skills DROP CONSTRAINT recruitment_applicant_skills_applicant_id_fkey"
    alter table(:recruitment_applicant_skills) do
      modify :applicant_id, references(:recruitment_applicants, on_delete: :nothing)
    end
  end
end
