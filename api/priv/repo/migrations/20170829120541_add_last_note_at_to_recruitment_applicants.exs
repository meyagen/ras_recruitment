defmodule RasDirectoryApi.Repo.Migrations.AddLastNoteAtToRecruitmentApplicants do
  use Ecto.Migration

  def change do
    alter table(:recruitment_applicants) do
      add :last_note_at, :naive_datetime, null: false, default: fragment("now()")
    end
  end
end
