defmodule RasDirectoryApi.Repo.Migrations.CreateMessagingClientMails do
  use Ecto.Migration

  def change do
    create table(:messaging_client_mails) do
      add :subject, :string, null: false
      add :content, :text, null: false
      add :client_id, references(:outsourcing_clients)
      add :created_by, references(:account_users)

      timestamps()
    end
  end
end
