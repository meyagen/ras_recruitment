defmodule RasDirectoryApi.Repo.Migrations.CreateTeamTable do
  use Ecto.Migration

  def change do
    create table(:organization_teams) do
      add :name, :string, null: false
      add :active?, :boolean, default: true

      timestamps()
    end

    create unique_index(:organization_teams, [:name])

    create table(:organization_memberships) do
      add :team_id, references(:organization_teams)
      add :user_id, references(:account_users)
      add :role, :integer, default: 1

      timestamps()
    end

    create unique_index(:organization_memberships, [:team_id, :user_id])
  end
end
