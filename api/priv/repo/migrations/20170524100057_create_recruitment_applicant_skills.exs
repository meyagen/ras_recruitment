defmodule RasDirectoryApi.Repo.Migrations.CreateRecruitmentApplicantSkills do
  use Ecto.Migration

  def change do
    create table(:recruitment_applicant_skills) do
      add :applicant_id, references(:recruitment_applicants)
      add :skill_id, references(:recruitment_skills)

      timestamps()
    end
  end
end
