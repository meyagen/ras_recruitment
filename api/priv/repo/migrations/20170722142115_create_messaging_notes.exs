defmodule RasDirectoryApi.Repo.Migrations.CreateMessagingNotes do
  use Ecto.Migration

  def change do
    create table(:messaging_applicant_notes) do
      add :content, :text, null: false
      add :applicant_id, references(:recruitment_applicants)
      add :created_by, references(:account_users)

      timestamps()
    end

    create table(:messaging_client_notes) do
      add :content, :text, null: false
      add :client_id, references(:outsourcing_clients)
      add :created_by, references(:account_users)

      timestamps()
    end
  end
end
