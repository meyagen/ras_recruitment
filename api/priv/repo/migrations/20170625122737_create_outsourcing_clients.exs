defmodule RasDirectoryApi.Repo.Migrations.CreateOutsourcingClients do
  use Ecto.Migration

  def change do
    create table(:outsourcing_clients) do
      add :first_name, :string, null: false
      add :last_name, :string, null: false
      add :position, :string
      add :mobile_number, :string
      add :primary_number, :string
      add :secondary_number, :string
      add :tertiary_number, :string
      add :email, :string

      add :company_id, references(:recruitment_companies), null: false
      add :country_id, references(:fact_countries), null: false
      add :created_by, references(:account_users), null: false

      timestamps()
    end

    create index(:outsourcing_clients, [:position])
    create index(:outsourcing_clients, [:company_id])
    create index(:outsourcing_clients, [:country_id])
    create index(:outsourcing_clients, [:created_by])
  end
end
